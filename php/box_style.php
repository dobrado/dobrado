<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['url', 'style', 'media'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) {
  $mysqli->close();
  exit;
}

// The posted style is used sql unsafe here.
$us_style = json_decode($_POST['style'], true);
$media = $mysqli->escape_string($_POST['media']);

foreach ($us_style as $us_selector => $us_rules) {
  if (!is_array($us_rules)) continue;
  $selector = $mysqli->escape_string($us_selector);
  // All box_style selectors must start with this string.
  if (strpos($selector, '#dobrado-') !== 0) continue;
  foreach ($us_rules as $us_property => $us_value) {
    $property = $mysqli->escape_string($us_property);
    // Ignore the default 'property' value.
    if ($property === 'property') continue;
    $value = $mysqli->escape_string($us_value);
    // If value is empty, the rule should be removed.
    if ($value === '') {
      $query = 'DELETE FROM box_style WHERE user = "'.$owner.'" AND '.
        'media = "'.$media.'" AND selector = "'.$selector.'" AND '.
        'property = "'.$property.'"';
      if (!$mysqli->query($query)) {
        log_db('box_style 1: '.$mysqli->error, $owner, $user->name, $page);
      }
      continue;
    }
    // If value contains 'javascript' or 'expression', ignore it.
    if (strpos($value, 'javascript') !== false ||
        strpos($value, 'expression') !== false) {
      continue;
    }
    // Otherwise work out if it's a new rule, or updating an existing one.
    $query = 'SELECT value FROM box_style WHERE user = "'.$owner.'" AND '.
      'media = "'.$media.'" AND selector = "'.$selector.'" AND '.
      'property = "'.$property.'"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows === 1) {
        $result->close();
        $query = 'UPDATE box_style SET value = "'.$value.'" WHERE '.
          'user = "'.$owner.'" AND media = "'.$media.'" AND '.
          'selector="'.$selector.'" AND property = "'.$property.'"';
        if (!$mysqli->query($query)) {
          log_db('box_style 2: '.$mysqli->error, $owner, $user->name, $page);
        }
      }
      else {
        $result->close();
        $query = 'INSERT INTO box_style VALUES ("'.$owner.'", "'.$media.'", '.
          '"'.$selector.'", "'.$property.'", "'.$value.'")';
        if (!$mysqli->query($query)) {
          log_db('box_style 3: '.$mysqli->error, $owner, $user->name, $page);
        }
      }
    }
    else {
      log_db('box_style 4: '.$mysqli->error, $owner, $user->name, $page);
    }
  }
}

$mysqli->close();

if ($owner === 'admin') write_box_style($owner, '../style.css');
else write_box_style($owner, '../'.$owner.'/style.css');

// Let the client know the action completed.
echo json_encode(['done' => true]);
