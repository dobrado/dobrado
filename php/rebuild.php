<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

if (!isset($_POST['url'])) {
  echo json_encode(['error' => 'url not provided']);
  exit;
}

include 'functions/db.php';
include 'functions/permission.php';
include 'functions/page_owner.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditSite) {
  echo json_encode(['error' => 'Permission denied rebuilding files.']);
  $mysqli->close();
  exit;
}

$date = date(DATE_COOKIE);

// Copy css files from the install directory, which are included with modules.
if ($handle = opendir('../install')) {
  while (($file = readdir($handle)) !== false) {
    if (preg_match('/.*\.css$/', $file)) {
      copy('../install/' . $file, '../css/' . $file);
    }
  }
  closedir($handle);
}
$handle = fopen('../3rdparty.css', 'w');
foreach (scandir('../css') as $file) {
  if (preg_match('/.*\.css$/', $file)) {
    $content = '/* File: ' . $file . "\n" .
      ' * Added by: ' . $user->name . "\n" .
      ' * Date: ' . $date . "\n" .
      " */\n";
    $content .= file_get_contents('../css/' . $file);
    $content .= "\n";
    fwrite($handle, $content);
  }
}
fclose($handle);

// Rebuild the dobrado.js and dobrado.pub.js files.
$content = "\n// File: core.pub.js\n// Added by: " . $user->name . "\n" .
  '// Date: ' . $date . "\n";
$content .= file_get_contents('../js/core.pub.js');
file_put_contents('../js/dobrado.pub.js', $content);
// core.pub.js is also included in dobrado.js, but first add core.js to content.
$content .= "\n// File: core.js\n// Added by: " . $user->name . "\n" .
  '// Date: ' . $date . "\n";
$content .= file_get_contents('../js/core.js');
file_put_contents('../js/dobrado.js', $content);

// Do the same as above for the source versions.
$content = "\n// File: core.pub.js\n// Added by: " . $user->name . "\n" .
  '// Date: ' . $date . "\n";
$content .= file_get_contents('../js/source/core.pub.js');
file_put_contents('../js/source/dobrado.pub.js', $content);
$content .= "\n// File: core.js\n// Added by: " . $user->name . "\n" .
  "// Date: " . $date . "\n";
$content .= file_get_contents('../js/source/core.js');
file_put_contents('../js/source/dobrado.js', $content);

// Then call each of the installed modules to update their own files.
if ($mysqli_result = $mysqli->query('SELECT label FROM installed_modules')) {
  while ($installed_modules = $mysqli_result->fetch_assoc()) {
    $module = new Module($user, $owner, $installed_modules['label']);
    $module->UpdateScript('../js');
  }
  $mysqli_result->close();
}
else {
  log_db('rebuild 1: ' . $mysqli->error);
}

if (!$mysqli->query('UPDATE script_version SET dobrado = dobrado + 1')) {
  log_db('rebuild 2: ' . $mysqli->error);
}
$mysqli->close();

// Let the client know the action completed.
echo json_encode(['done' => true]);
