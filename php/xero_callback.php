<?php
// Dobrado Content Management System
// Copyright (C) 2021 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

session_start();

if (!isset($_GET['code'])) {
  echo 'Authorisation code not found.';
  exit;
}

if ($_GET['state'] !== $_SESSION['oauth2state']) {
  echo 'State doesn\'t match.';
  exit;
}

include 'functions/db.php';

include 'config.php';
include 'module.php';
include 'user.php';

$user = new User();
$xero2 = new Module($user, $user->name, 'xero2');
if (!$xero2->IsInstalled()) {
  echo 'Xero2 module is not installed.';
  exit;
}

$xero2->Factory('StoreToken');
