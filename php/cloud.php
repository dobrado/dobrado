<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/db.php';
include 'config.php';
include 'module.php';
include 'user.php';

function challenge($register, $url, $challenge, $lease = false) {
  // Check if the register url already contains query parameters.
  $register .= strpos($register, '?') === false ? '?' : '&';
  if ($lease !== false) {
    $ch = curl_init($register . 'hub.mode=subscribe&hub.topic=' .
                    urlencode($url) . '&hub.challenge=' . $challenge .
                    '&hub.lease_seconds=' . $lease);
  }
  else {
    $ch = curl_init($register . 'url=' . urlencode($url) . '&challenge=' .
                    $challenge);
  }
  curl_setopt($ch, CURLOPT_HEADER, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  curl_setopt($ch, CURLOPT_ENCODING, '');
  log_db('challenge: curl ' . $register);
  $data = explode("\r\n", curl_exec($ch));
  $count = count($data);
  if ($count < 2) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: could not check notification URL.';
    return false;
  }
  // Check that the status code for the first header is in the 200's.
  if (!preg_match('/^HTTP\/1.1 2[0-9][0-9]/', $data[0])) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: incorrect status header.';
    return false;
  }
  // Then the last entry in the data array must be the challenge value.
  if ($challenge !== $data[$count - 1]) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: challenge does not match. ' .
      $challenge . ' !== ' . $data[$count - 1];
    return false;
  }
  return true;
}

$user = new User();
$reader = new Module($user, '', 'reader');
if (!$reader->IsInstalled()) {
  header('HTTP/1.1 404 Not Found');
  echo 'Update notifications are not available.';
  exit;
}

if (isset($_POST['hub_mode']) && $_POST['hub_mode'] === 'subscribe') {
  $url = $_POST['hub_topic'];
  $register = $_POST['hub_callback'];
  if (challenge($register, $url,
      bin2hex(openssl_random_pseudo_bytes(8)), 86400)) {
    $reader->Factory('Register', [$url, $register, 1]);
    header('HTTP/1.1 202 Accepted');
    echo 'Registration successful.';
  }
  exit;
}

if (isset($_POST['protocol']) && $_POST['protocol'] !== 'http-post') {
  header('HTTP/1.1 404 Not Found');
  echo 'Sorry only http-post protocol is supported.';
  exit;
}

$port = isset($_POST['port']) ? $_POST['port'] : '80';
$path = isset($_POST['path']) ? $_POST['path'] : '/';
$domain = '';
$register = '';
$verify = false;
$server = $user->config->ServerName();

if (isset($_POST['domain'])) {
  $domain = $_POST['domain'];
  $verify = true;
}
else {
  $domain = $_SERVER['REMOTE_ADDR'];
}
// Use ports 80 and 443 to specify whether http or https should be
// used for updating, otherwise explicitly add the port number.
if ($port === '80') {
  $register = 'http://' . $domain;
}
else if ($port === '443') {
  $register = 'https://' . $domain;
}
else {
  $register = 'http://' . $domain . ':' . $port;
}
$register .= $path;

$i = 1;
while (isset($_POST['url' . $i])) {
  $url = $_POST['url' . $i++];
  // Make sure each of the provided urls are for this domain.
  if (strpos($url, $server) === false) {
    header('HTTP/1.1 404 Not Found');
    echo 'Error: cannot register for ' . $url;
    continue;
  }
  if ($verify &&
      !challenge($register, $url, bin2hex(openssl_random_pseudo_bytes(8)))) {
    continue;
  }
  $reader->Factory('Register', [$url, $register, 0]);
  echo 'Registration for ' . $url . ' successful.';
}
