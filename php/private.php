<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

session_start();

if (!isset($_SESSION['user'])) exit;

$regex = '/[a-zA-Z0-9_-]{1,200}\.[a-z0-9]{1,10}$/';
$file = isset($_GET['file']) ? $_GET['file'] : '';
if (!preg_match($regex, $file)) exit;

include 'functions/db.php';
include 'functions/permission.php';
include 'config.php';
include 'module.php';
include 'user.php';
$user = new User();

$server = $user->config->ServerName();
$path = $user->config->PrivatePath().'/'.$server.'/user/'.$user->name.'/'.$file;

if (file_exists($path)) {
  header('Cache-Control: private');
  header('Content-Type: application/octet');
  header('Content-Disposition: attachment; filename='.basename($path));
  readfile($path);
  unlink($path);
}
else {
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  $url = $user->config->FancyUrl() ? '/' : '/index.php?page=';
  header('Location: '.$scheme.$server.$url.$user->config->Unavailable());
}
