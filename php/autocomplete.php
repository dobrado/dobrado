<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/db.php';

include 'config.php';
include 'module.php';
include 'user.php';

session_start();

$user = new User();
if (!$user->loggedIn) exit;

$result = [];
$us_type = isset($_GET['type']) ? $_GET['type'] : '';
$us_match = isset($_GET['match']) ? $_GET['match'] : '';

if ($us_type === 'search') {
  // TODO: Need to create a Search module and add it to the page set in the
  // 'search-page' template. Also only one result is currently returned here,
  // ie the search term the user provided. Need the Post module to add search
  // terms found in SetContent to a search_tags table that can be checked here.
  $path = substitute('search-page', $user->group);
  if ($path !== '') {
    $result[] = ['id' => 0, 'name' => $us_match, 'url' => $path . $us_match];
  }
}
else if ($us_type === 'nickname') {
  $id = 0;
  $limit = 5;
  $mysqli = connect_db();
  $match = $mysqli->escape_string($us_match);
  $query = 'SELECT name, url, cache, nickname FROM nickname WHERE ' .
    'nickname LIKE "' . $match . '%" ORDER BY nickname LIMIT ' . $limit;
  if ($mysqli_result = $mysqli->query($query)) {
    while ($nickname = $mysqli_result->fetch_assoc()) {
      $result[] = ['id' => $id++, 'name' => $nickname['nickname'],
                   'photo' => $nickname['cache'],
                   'fullname' => $nickname['name'], 'url' => $nickname['url']];
    }
    $mysqli_result->close();
  }
  else {
    log_db('autocomplete.php 1: ' . $mysqli->error);
  }
  $count = count($result);
  // If no matches for nickname try full name or url. (Note that this can't
  // be done in one query because empty nicknames get ordered to the top.)
  if ($count < $limit) {
    $limit -= $count;
    $query = 'SELECT name, url, cache FROM nickname WHERE ' .
      'name LIKE "' . $match . '%" OR url LIKE "http://' . $match . '%" OR ' .
      'url LIKE "https://' . $match . '%" ORDER BY name LIMIT ' . $limit;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($nickname = $mysqli_result->fetch_assoc()) {
        $result[] = ['id' => $id++, 'name' => $nickname['name'],
                     'photo' => $nickname['cache'], 'fullname' => '',
                     'url' => $nickname['url']];
      }
      $mysqli_result->close();
    }
    else {
      log_db('autocomplete.php 2: ' . $mysqli->error);
    }
  }
  $mysqli->close();
}

echo json_encode($result);
