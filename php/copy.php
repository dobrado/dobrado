<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['page', 'owner', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$new_page = $mysqli->escape_string($_POST['page']);
$new_owner = $mysqli->escape_string($_POST['owner']);
$url = $mysqli->escape_string($_POST['url']);
list($current_page, $current_owner) = page_owner($url);

$user = new User();
$user->SetPermission($current_page, $current_owner);
if (!$user->canCopyPage) {
  $error = 'Could not copy the current page: Permission denied.';
  echo json_encode(['error' => $error]);
  $mysqli->close();
  exit;
}

// If owner is empty, default to the current user, unless that is 'admin' in
// which case need to also check the new page name as it might conflict with a
// username if fancy url is true. (can still update but admin needs permission)
if ($new_owner === '') {
  $new_owner = $user->name;
  if ($user->name === 'admin' && $user->config->FancyUrl()) {
    $query = 'SELECT user FROM users WHERE user = "'.$new_page.'"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows !== 0) {
        $new_owner = $new_page;
        $new_page = "index";
      }
    }
    else {
      log_db('copy 1: '.$mysqli->error, $current_owner, $user->name,
             $current_page);
    }
  }
}

if (!can_edit_page($new_owner.'/'.$new_page)) {
  $error = 'Could not copy to page: <b>'.$new_owner.'/'.$new_page.
    '</b>.<br/>Please make sure you have permission to edit that page.';
  echo json_encode(['error' => $error]);
  $mysqli->close();
  exit;
}

// Make sure the destination page is empty.
$error = '';
$query = 'SELECT box_id FROM modules where user = "'.$new_owner.'" AND '.
  'page= "'.$new_page.'" AND deleted = 0';
if ($result = $mysqli->query($query)) {
  if ($result->num_rows !== 0) {
    $error = 'The new page must be empty.';
  }
  $result->close();
}
else {
  log_db('copy 2: '.$mysqli->error, $current_owner, $user->name, $current_page);
}
$mysqli->close();

if ($error !== '') {
  echo json_encode(['error' => $error]);
  exit;
}

$name = copy_page($current_page, $current_owner, $new_page, $new_owner);

// Set the reload-page session variable so that the browser forces a page
// reload to get updated css files.
$_SESSION['reload-page'] = true;
echo json_encode(['name' => $name]);
