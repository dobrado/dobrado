<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Note: This file is included by the Autoupdate module, but it should be copied
// and renamed using the version number of the core update you are building.
function core_update() {
  $mysqli = connect_db();
  // Need to update version in installed_modules for Autoupdate module because
  // current update was removed but this is preventing a new update from being
  // installed (which includes this fix for next time).
  $query = 'UPDATE installed_modules SET version = "" WHERE ' .
    'label = "autoupdate"';
  if (!$mysqli->query($query)) {
    error_log('core_update: ' . $mysqli->error);
  }
  $mysqli->close();
}