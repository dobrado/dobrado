<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function category_markup($category, $action = '', $name = '') {
  $category = trim($category);
  if ($category === '') return '';

  // Check if the category looks like a url.
  if (stripos($category, 'http') === 0) {
    if ($name !== '') {
      if ($action !== '') $action = ' u-' . $action;
      return '<span class="ui-icon ui-icon-person"></span>' .
        '<a href="' . $category . '" ' .
          'class="h-card u-category' . $action . '">' . $name . '</a> ';
    }

    if (preg_match('/^https?:\/\/(.+)$/', $category, $match)) {
      $display = $match[1];
      if (strlen($display) > 60) $display = substr($display, 0, 50) . '...';
      // Repost markup is handled by Post->Content so just need to specify
      // u-url here. Add u-category if action is not set.
      if ($action === 'repost-of') $action = 'url';
      else if ($action === '') $action = 'category';
      return '<a href="' . $category . '" class="u-' . $action . '">' .
        $display . '</a> ';
    }
  }
  // SimplePie adds markup for person tags, convert to jQuery UI icon.
  if (strpos($category, 'class="person-tag"')) {
    $category = preg_replace('/class="person-tag"/',
                             'class="ui-icon ui-icon-person"', $category);
  }
  else if (strpos($category, 'reposted by ') === 0) {
    $category = 'Reposted from';
  }
  return '<span class="p-category">' . $category . '</span> ';
}

function content_markup($content) {
  // SimplePie adds markup for like-of, repost-of, quotation-of and in-reply-to,
  // convert to jQuery UI icons.
  if (strpos($content, 'class="in-reply-to"')) {
    $replacement = 'class="ui-icon ui-icon-arrowreturnthick-1-e" ' .
                   'title="In reply to"';
    $content = preg_replace('/class="in-reply-to"/', $replacement, $content);
  }
  if (strpos($content, 'class="like-of"')) {
    $replacement = 'class="ui-icon ui-icon-star" title="Like of"';
    $content = preg_replace('/class="like-of"/', $replacement, $content);
  }
  if (strpos($content, 'class="repost-of"')) {
    $replacement = 'class="ui-icon ui-icon-refresh" title="Repost of"';
    $content = preg_replace('/class="repost-of"/', $replacement, $content);
  }
  if (strpos($content, 'class="quotation-of"')) {
    $replacement = 'class="ui-icon ui-icon-comment" title="Quotation of"';
    $content = preg_replace('/class="quotation-of"/', $replacement, $content);
  }
  return $content;
}

function style_rule($property = 'property', $value = 'value') {
  $readonly = $property === 'property' && $value === 'value' ? '' :
    ' readonly="true"';
  return '<div class="style-rule">' .
           '<button class="remove-style-rule">remove</button>' .
           '<span class="ui-widget">' .
           '<input class="style-property" type="text" ' .
             'value="' . htmlspecialchars($property) . '"' . $readonly . '>' .
           '</span>' .
           '<span class="ui-widget">' .
             '<input class="style-value" type="text" ' .
               'value="' . htmlspecialchars($value) . '">' .
           '</span>' .
         '</div>';
}

function style_groups($style, $mode) {
  // Make sure the 'General' style group is always created when displaying
  // the style editor for individual boxes. Note the General group is only
  // used for box styles.
  $general_style_group_written = false;
  if ($mode !== 'box') $general_style_group_written = true;
  $content = '';
  foreach ($style as $selector => $rules) {
    // If the selector is just the id, rules are put in the General group.
    if (preg_match('/^#dobrado-[0-9]+$/', $selector) && $mode === 'box') {
      $content .= '<h3>General</h3>';
      $general_style_group_written = true;
    }
    else {
      // Otherwise just the custom part of the selector is used when
      // displaying individual boxes.
      $custom = '';
      if (preg_match('/^(?:#dobrado-[0-9]+ )?(.*)$/', $selector, $match)) {
        $custom = htmlspecialchars($match[1]);
      }
      $content .= '<h3>' . $custom . '</h3>';
    }
    $content .= '<div class="style-group">';
    foreach ($rules as $property => $value) {
      $content .= style_rule($property, $value);
    }
    $content .= '<button class="new-style-rule">Add a new style rule</button>' .
      '</div>';
  }
  if (!$general_style_group_written) {
    $default_general_group = '<h3>General</h3>' .
      '<div class="style-group">' . style_rule() .
        '<button class="new-style-rule">Add a new style rule</button>' .
      '</div>';
    $content = $default_general_group . $content;
  }
  return $content;
}
