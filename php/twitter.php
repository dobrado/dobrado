<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (!isset($_GET['oauth_token'])) exit;

include 'functions/db.php';
include 'config.php';
include 'module.php';
include 'user.php';

$user = new User();
$twitter = new Module($user, '', 'twitter');
if (!$twitter->IsInstalled()) {
  header('HTTP/1.1 404 Not Found');
  echo 'Twitter module not installed.';
  exit;
}

// Check if generating a feed for a username.
if (isset($_GET['username'])) {
  echo $twitter->Factory('Feed');
  exit;
}

// Twitter callback provides oauth_verifier parameter to upgrade a request
// token to an access token.
if (isset($_GET['oauth_verifier'])) {
  $twitter->Factory('AccessToken',
                    [$_GET['oauth_token'], $_GET['oauth_verifier']]);
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  $config = new Config();
  $page = $config->FancyUrl() ? '/' : '/index.php?page=';
  $page .= substitute('twitter-callback');
  header('Location: ' . $scheme . $_SERVER['SERVER_NAME'] . $page);
}
