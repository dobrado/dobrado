<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

include 'functions/db.php';

$user = $_SESSION['user'];
$pages = [];

$mysqli = connect_db();
$term = isset($_POST['term']) ? $mysqli->escape_string($_POST['term']) : '';
// Set the pages array to a list of names to autocomplete for this user.
// First get the user's own pages that match the given term:
$query = 'SELECT DISTINCT page FROM modules WHERE user = "' . $user . '" ' .
  'AND page LIKE "%' . $term . '%" AND deleted = 0 ORDER BY page LIMIT 10';
if ($result = $mysqli->query($query)) {
  while ($modules = $result->fetch_assoc()) {
    $pages[] = $modules['page'];
  }
  $result->close();
}
else {
  log_db('search 1: ' . $mysqli->error);
}
// Then get pages the user has permission for:
$query = 'SELECT DISTINCT user, page FROM user_permission WHERE ' .
  'user != "' . $user . '" AND (edit = 1 OR copy = 1 OR view = 1) AND ' .
  '(visitor = "' . $user . '" OR visitor = "") AND CONCAT(user, "/", page) ' .
  'LIKE "%' . $term . '%" ORDER BY user, page LIMIT 10';
if ($result = $mysqli->query($query)) {
  while ($user_permission = $result->fetch_assoc()) {
    $pages[] = $user_permission['user'] . '/' . $user_permission['page'];
  }
  $result->close();
}
else {
  log_db('search 2: ' . $mysqli->error);
}
// Then get the pages where the user is in a group that has permission.
$query = 'SELECT group_permission.user, page FROM group_permission LEFT JOIN ' .
  'group_names ON group_names.name = group_permission.name AND ' .
  'group_names.user = group_permission.user WHERE ' .
  'group_permission.user != "' . $user . '" AND ' .
  'group_names.visitor = "' . $user . '" AND ' .
  '(edit = 1 OR copy = 1 OR view = 1) AND ' .
  'CONCAT(group_permission.user, "/", page) LIKE ' .
  '"%' . $term . '%" ORDER BY group_permission.user, page LIMIT 10';
if ($result = $mysqli->query($query)) {
  while ($group_permission = $result->fetch_assoc()) {
    // Make sure there are no duplicates from user_permission table.
    $group_page = $group_permission['user'] . '/' . $group_permission['page'];
    if (!in_array($group_page, $pages)) $pages[] = $group_page;
  }
  $result->close();
}
else {
  log_db('search 3: ' . $mysqli->error);
}

$mysqli->close();

echo json_encode($pages);