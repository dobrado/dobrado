<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function header_value($headers, $name) {
  foreach ($headers as $key => $value) {
    if (strtolower($key) === strtolower($name)) return $value;
  }
  return '';
}

$us_token = '';
$headers = apache_request_headers();
$authorization = header_value($headers, 'Authorization');
if ($authorization !== '') {
  // Remove the prefix 'Bearer ' from the Authorization header.
  $us_token = substr($authorization, 7);
}
else if (isset($_POST['access_token'])) {
  $us_token = urldecode($_POST['access_token']);
}
if ($us_token === '') {
  header('HTTP/1.1 401 Unauthorised');
  exit;
}

include 'functions/db.php';

$me = '';
$mysqli = connect_db();
$token = $mysqli->escape_string($us_token);
$query = 'SELECT me FROM access_tokens WHERE token = "' . $token . '"';
if ($result = $mysqli->query($query)) {
  if ($access_tokens = $result->fetch_assoc()) {
    $me = $access_tokens['me'];
  }
  $result->close();
}
else {
  log_db('notify_endpoint.php 1: ' . $mysqli->error);
}
$mysqli->close();

if (!preg_match('/^https?:\/\/' . $_SERVER['SERVER_NAME'] . '/', $me)) {
  log_db('notify_endpoint.php 2: Couldn\'t match ' . $_SERVER['SERVER_NAME'] .
         ' in: ' . $me);
  header('HTTP/1.1 403 Forbidden');
  exit;
}

include 'functions/page_owner.php';

list($page, $owner) = page_owner($me);

$content_type = header_value($headers, 'Content-Type');
$data = $content_type === 'application/json' ?
  json_decode(file_get_contents('php://input'), true) : [];
if ($content_type === 'application/json') {
  if (!isset($data['action']) || $data['action'] !== 'notify') {
    log_db('notify_endpoint.php 3: JSON data action not found.');
    header('HTTP/1.1 400 Bad Request');
    exit;
  }
}
else if (!isset($_POST['action']) || $_POST['action'] !== 'notify') {
  log_db('notify_endpoint.php 4: POST data action not found.');
  header('HTTP/1.1 400 Bad Request');
  exit;
}

$mysqli = connect_db();
// Need to know what page the reader is on. This means updates are limited to
// one page at the moment.
$page = '';
$id = 0;
$query = 'SELECT page, box_id FROM modules WHERE user = "' . $owner . '" ' .
  'AND label = "reader" AND deleted = 0';
if ($mysqli_result = $mysqli->query($query)) {
  if ($modules = $mysqli_result->fetch_assoc()) {
    $page = $modules['page'];
    $id = $modules['box_id'];
  }
  $mysqli_result->close();
}
else {
  log_db('notify_endpoint.php 5: ' . $mysqli->error);
}
if ($page !== '' && $id !== 0) {
  $timestamp = time();
  $query = 'INSERT INTO notify VALUES ("' . $owner . '", "' . $page . '", ' .
    $id . ', "reader", "feed", ' . $timestamp . ', 0) ON DUPLICATE KEY ' .
    'UPDATE timestamp = ' . $timestamp;
  if (!$mysqli->query($query)) {
    log_db('notify_endpoint.php 6: ' . $mysqli->error);
  }
}
$mysqli->close();
header('HTTP/1.1 204 No Content');
