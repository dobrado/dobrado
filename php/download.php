<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

session_start();

// TODO: At the moment only the public directory is supported.
// Next step is to support uploads to the user's private directory in the
// Browser module, and then can use user->config->PrivatePath() here.

$regex = '/(public\/[a-zA-Z0-9_-]{1,200}\.[a-z0-9]{1,10})$/';
$file = $_GET['file'];
if (!preg_match($regex, $file, $matches)) exit;

// Remove the protocol and domain from the file if present.
$path = $matches[1];

// Make sure this file is in the cart.
if (!$_SESSION['cart']) exit;
$in_cart = false;
for ($i = 0; $i < count($_SESSION['cart']); $i++) {
  if ($_SESSION['cart'][$i] == $file) {
    $in_cart = true;
    break;
  }
}
if (!$in_cart) exit;

if ($_SESSION['cart-owner'] != 'admin') {
  $path = $_SESSION['cart-owner'].'/'.$path;
}
$path = '../'.$path;

if (file_exists($path)) {
  header('Cache-Control: private');
  header('Content-Type: application/octet');
  header('Content-Disposition: attachment; filename='.basename($path));
  readfile($path);
}
