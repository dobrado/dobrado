<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function header_value($headers, $name) {
  foreach ($headers as $key => $value) {
    if (strtolower($key) === strtolower($name)) return $value;
  }
  return '';
}

$us_token = '';
$headers = apache_request_headers();
$authorization = header_value($headers, 'Authorization');
if ($authorization !== '') {
  // Remove the prefix 'Bearer ' from the Authorization header.
  $us_token = substr($authorization, 7);
}
else if (isset($_POST['access_token'])) {
  $us_token = urldecode($_POST['access_token']);
}
if ($us_token === '') {
  header('HTTP/1.1 401 Unauthorised');
  exit;
}

include 'functions/db.php';

// Look up me value for local users in access_tokens.
$me = '';
$mysqli = connect_db();
$token = $mysqli->escape_string($us_token);
$query = 'SELECT me FROM access_tokens WHERE token = "' . $token . '"';
if ($mysqli_result = $mysqli->query($query)) {
  if ($access_tokens = $mysqli_result->fetch_assoc()) {
    $me = $access_tokens['me'];
  }
  $mysqli_result->close();
}
else {
  log_db('microsub.php 1: ' . $mysqli->error);
}
$mysqli->close();

// External users can also use the Microsub server if they have previously
// logged in to the site.
$indieauth = false;
if ($me === '') {
  if (!isset($_GET['me']) || $_GET['me'] === '') {
    log_db('microsub.php 2: \'me\' parameter not provided in Microsub request');
    header('HTTP/1.1 403 Forbidden');
    exit;
  }

  // Allow the client to set the me parameter without a scheme.
  $us_me = $_GET['me'];
  if (preg_match('/^https?:\/\/(.+)$/', $us_me, $match)) {
    $us_me = trim($match[1], ' /');
  }
  $us_me = preg_replace('/\//', '_', $us_me);

  $mysqli = connect_db();
  $me = $mysqli->escape_string($us_me);
  // Check if this token has already been confirmed with their token endpoint.
  $query = 'SELECT value FROM settings WHERE user = "' . $me . '" AND ' .
    'label = "microsub" AND name = "token"';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($settings = $mysqli_result->fetch_assoc()) {
      $indieauth = $settings['value'] === $us_token;
    }
    $mysqli_result->close();
  }
  else {
    log_db('microsub.php 3: ' . $mysqli->error);
  }
  if (!$indieauth) {
    $token_endpoint = '';
    // A token endpoint is stored in the settings table in auth.php when the
    // user logs in.
    $query = 'SELECT value FROM settings WHERE user = "' . $me . '" AND ' .
      'label = "token" AND name = "endpoint"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($settings = $mysqli_result->fetch_assoc()) {
        $token_endpoint = $settings['value'];
      }
      $mysqli_result->close();
    }
    else {
      log_db('microsub.php 4: ' . $mysqli->error);
    }
    // Confirm the new token with the user's token endpoint.
    if ($token_endpoint !== '') {
      $curl_headers = ['Authorization: Bearer ' . $us_token,
                       'Accept: application/json'];
      $ch = curl_init($token_endpoint);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, 20);
      curl_setopt($ch, CURLOPT_ENCODING, '');
      curl_setopt($ch, CURLOPT_HEADER, false);

      log_db('microsub.php 5: curl ' . $token_endpoint);
      $body = curl_exec($ch);
      $response = [];
      if (curl_errno($ch) === 0) {
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code === 200) {
          $response = json_decode($body, true);
        }
        else {
          log_db('microsub.php 6: Error getting ' . $token_endpoint .
                 "\nHTTP code: " . $http_code . "\nBody: " . $body);
        }
      }
      else {
        log_db('microsub.php 7: Error connecting to ' . $token_endpoint .
               "\nCurl error: " . curl_error($ch));
      }
      curl_close($ch);

      if (isset($response['me'])) {
        $check_me = '';
        if (preg_match('/^https?:\/\/(.+)$/', $response['me'], $match)) {
          $check_me = trim($match[1], ' /');
        }
        $check_me = preg_replace('/\//', '_', $check_me);
        if ($me === $check_me) {
          $indieauth = true;
          $query = 'INSERT INTO settings VALUES ("' . $me . '", "microsub", ' .
            '"token", "' . $token . '") ON DUPLICATE KEY UPDATE ' .
            'value = "' . $token . '"';
          if (!$mysqli->query($query)) {
            log_db('microsub.php 8: ' . $mysqli->error);
          }
        }
      }
    }
  }
  $mysqli->close();
}

if (!$indieauth &&
    !preg_match('/^https?:\/\/' . $_SERVER['SERVER_NAME'] . '/', $me)) {
  log_db('microsub.php 9: Couldn\'t find an account on ' .
         $_SERVER['SERVER_NAME'] . ' using the access token provided.');
  header('HTTP/1.1 403 Forbidden');
  exit;
}

if (!isset($_GET['action']) && !isset($_POST['action'])) {
  log_db('microsub.php 10: action not set.');
  header('HTTP/1.1 400 Bad Request');
  exit;
}

include 'functions/copy_page.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

// This provides the owner and their home page, because url is from rel=me,
// indieauth accounts are set to a specific admin page for the Reader module.
list($page, $owner) = $indieauth ?
  [substitute('indieauth-page'), 'admin'] : page_owner($me);
$username = $indieauth ? $me : $owner;
$user = new User($username);
$user->SetPermission($page, $owner);
$reader = new Module($user, $owner, 'reader');
if (!$reader->IsInstalled()) {
  header('HTTP/1.1 500 Internal Server Error');
  log_db('microsub.php 11: Reader module is not installed.');
  exit;
}

header('Content-Type: application/json');
echo json_encode($reader->Factory('Microsub'));
