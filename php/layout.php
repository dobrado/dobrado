<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['id', 'box_order', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/update_layout.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) {
  $mysqli->close();
  return;
}

// Modules can be grouped together, so a layout change would affect the
// whole group. In this case the following values are comma separated.
// If there is only one module, explode still puts the full string in an array.
$id_array = explode(',', $mysqli->escape_string($_POST['id']));
$box_order_array = explode(',', $mysqli->escape_string($_POST['box_order']));
$mysqli->close();
$total = count($id_array);
// When groups are moved down the page, the modules below them are moved up,
// so need to find an offset for the current group to move by. This is the
// first box_order given, plus the group size, minus the module being moved.
$offset = 0;

for ($i = 0; $i < $total; $i++) {
  // Remove the '#dobrado-' prefix from the id.
  $id = (int)substr($id_array[$i], 9);
  $order = (int)$box_order_array[$i];
  if ($i === 0) {
    $offset = $order + $total - 1;
  }
  update_layout($id, $order, $offset, $page, $owner);
}

// Let the client know the action completed.
echo json_encode(['done' => true]);
