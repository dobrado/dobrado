<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Only run this script from the command line.
if (php_sapi_name() !== 'cli') exit;

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/install_module.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/new_user.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

// Note: The frequency with which this script is called by the system cron
// should be set in Config's Cron method. Also be careful with admin's
// system_group if the empty system_group is being used, because the User
// class will set the group when it's not provided.

$owner = 'admin';
$user = new User($owner);

// cron.php is called from the command line, so must set $_SERVER values.
$_SERVER['SCRIPT_FILENAME'] = 'php/cron.php';
$_SERVER['SERVER_NAME'] = $user->config->ServerName();
$_SERVER['HTTPS'] = $user->config->Secure() ? '1' : '';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

$mysqli = connect_db();
if ($mysqli_result = $mysqli->query('SELECT label FROM installed_modules')) {
  while ($modules = $mysqli_result->fetch_assoc()) {
    $module = new Module($user, $owner, $modules['label']);
    $module->Cron();
  }
  $mysqli_result->close();
}
else {
  log_db('cron.php: ' . $mysqli->error);
}
$mysqli->close();
