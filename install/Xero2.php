<?php
// Dobrado Content Management System
// Copyright (C) 2021 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Xero2 extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view xero2.'];
    }

    $action = $_POST['action'];
    if ($action === 'submit') return $this->Submit();
  }

  public function CanAdd($page) {
    if (!$this->user->canEditSite) return false;
    return !$this->AlreadyOnPage('xero2', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    include 'php/vendor/autoload.php';
    $config = $this->Config();
    $token = $config['token'];
    if ($config['enabled'] && $config['expires'] < time()) {
      $provider = $this->GetProvider();
      $newAccessToken = $provider->getAccessToken('refresh_token', [
        'refresh_token' => $config['refresh_token']]);
      
      $mysqli = connect_db();
      $token = $mysqli->escape_string($newAccessToken->getToken());
      $expires = (int)$newAccessToken->getExpires();
      $us_refresh_token = $newAccessToken->getRefreshToken();
      $refresh_token = $mysqli->escape_string($us_refresh_token);
      $us_id_token = $newAccessToken->getValues()['id_token'];
      $id_token = $mysqli->escape_string($us_id_token);
      $query = 'UPDATE xero_tokens SET token = "' . $token . '", ' .
        'expires = ' . $expires . ', refresh_token = ' .
        '"' . $refresh_token . '", id_token = "' . $id_token . '" ' .
        'WHERE system_group = "' . $this->user->group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Xero2->Content: ' . $mysqli->error);
      }
      $mysqli->close();
    }

    $config = $this->Config();
    $checked = $config['enabled'] ? 'checked="checked" ' : '';
    return '<form id="xero2-form">' .
      '<div class="form-spacing">' .
        '<label for="xero2-enabled-input">Send all invoices to Xero:</label>' .
        '<input id="xero2-enabled-input" ' . $checked . 'type="checkbox">' .
      '</div>' .
      '<button id="xero2-submit">submit</button>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'Enabled') return $this->Config()['enabled'];
    if ($fn === 'SendInvoice') return $this->SendInvoice($p);
    if ($fn === 'StoreToken') return $this->StoreToken();
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }
  
  public function Install($path) {
    // Append dobrado.xero2.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.xero2.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS xero_settings (' .
      'client_id TEXT,' .
      'client_secret TEXT' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Xero2->Install 1: ' . $mysqli->error);
    }
      
    $query = 'CREATE TABLE IF NOT EXISTS xero_tokens (' .
      'token TEXT,' .
      'expires INT(10) UNSIGNED,' .
      'tenant_id VARCHAR(100),' .
      'refresh_token TEXT,' .
      'id_token TEXT,' .
      'enabled TINYINT(1),' .
      'system_group VARCHAR(50),' .
      'PRIMARY KEY(system_group)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Xero2->Install 2: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS xero_contacts (' .
      'account_name TEXT,' .
      'contact_id TEXT,' .
      'system_group VARCHAR(50)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Xero2->Install 3: ' . $mysqli->error);
    }
    $mysqli->close();

    $site_style = ['"","label[for=xero2-enabled-input]","float","none"',
                   '"","#xero2-form label","width","12em"',
                   '"","#xero2-form","margin","20px"',
                   '"","#xero2-submit","margin-left","11.3em"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.xero2.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function Config() {
    $config = ['token' => '', 'expires' => 0, 'tenant_id' => '',
      'refresh_token' => '', 'id_token' => '', 'enabled' => false];
    $mysqli = connect_db();
    $query = 'SELECT token, expires, tenant_id, refresh_token, id_token, ' .
      'enabled FROM xero_tokens WHERE system_group = ' .
      '"' . $this->user->group . '"';
    if ($result = $mysqli->query($query)) {
      if ($xero_tokens = $result->fetch_assoc()) {
        $config['token'] = $xero_tokens['token'];
        $config['expires'] = (int)$xero_tokens['expires'];
        $config['tenant_id'] = $xero_tokens['tenant_id'];
        $config['refresh_token'] = $xero_tokens['refresh_token'];
        $config['id_token'] = $xero_tokens['id_token'];
        $config['enabled'] = (bool)$xero_tokens['enabled'];
      }
      $result->close();
    }
    else {
      $this->Log('Xero2->Config: ' . $mysqli->error);
    }
    $mysqli->close();
    return $config;
  }

  private function GetProvider() {
    $settings = $this->Settings();
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    $redirect_uri = $scheme . $_SERVER['SERVER_NAME'] .
      '/php/xero_callback.php';
    $urlAuthorize = 'https://login.xero.com/identity/connect/authorize';
    $urlAccessToken = 'https://identity.xero.com/connect/token';
    $urlResourceOwnerDetails = 'https://api.xero.com/api.xro/2.0/Organisation';
    return new \League\OAuth2\Client\Provider\GenericProvider([
      'clientId'                => $settings['client_id'],
      'clientSecret'            => $settings['client_secret'],
      'redirectUri'             => $redirect_uri,
      'urlAuthorize'            => $urlAuthorize,
      'urlAccessToken'          => $urlAccessToken,
      'urlResourceOwnerDetails' => $urlResourceOwnerDetails]);
  }

  private function Settings() {
    $settings = ['client_id' => '', 'client_secret' => ''];
    $mysqli = connect_db();
    $query = 'SELECT client_id, client_secret FROM xero_settings';
    if ($result = $mysqli->query($query)) {
      if ($xero_settings = $result->fetch_assoc()) {
        $settings = $xero_settings;
      }
      $result->close();
    }
    else {
      $this->Log('Xero2->Settings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $settings;
  }

  private function SendInvoice($data) {
    $config = $this->Config();
    if (!$config['enabled']) return;

    if ($config['tenant_id'] === '') {
      $this->Log('Xero2->SendInvoice 1: tenant_id not set for ' .
                 $this->user->group);
      return;
    }

    include_once 'vendor/autoload.php';
    $token = $config['token'];
    if ($config['expires'] < time()) {
      $provider = $this->GetProvider();
      $newAccessToken = $provider->getAccessToken('refresh_token', [
        'refresh_token' => $config['refresh_token']]);
      
      $mysqli = connect_db();
      $token = $mysqli->escape_string($newAccessToken->getToken());
      $expires = (int)$newAccessToken->getExpires();
      $us_refresh_token = $newAccessToken->getRefreshToken();
      $refresh_token = $mysqli->escape_string($us_refresh_token);
      $us_id_token = $newAccessToken->getValues()['id_token'];
      $id_token = $mysqli->escape_string($us_id_token);
      $query = 'UPDATE xero_tokens SET token = "' . $token . '", ' .
        'expires = ' . $expires . ', refresh_token = ' .
        '"' . $refresh_token . '", id_token = "' . $id_token . '" ' .
        'WHERE system_group = "' . $this->user->group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Xero2->SendInvoice 2: ' . $mysqli->error);
      }
      $mysqli->close();
    }

    $xero_config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->
      setAccessToken($token);
    $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
      new GuzzleHttp\Client(), $xero_config);

    $contact_id = $this->GetContactID($data['accountName']);
    if ($contact_id === '') {
      $page = 1;
      $contact_list = $apiInstance->getContacts($config['tenant_id'], '', '',
                                                '', [], $page, true, true);
      while (count($contact_list) === 100) {
        foreach($contact_list as $contact) {
          $this->SetContactID($contact->getName(), $contact->getContactId());
        }
        $page++;
        $contact_list = $apiInstance->getContacts($config['tenant_id'], '', '',
                                                  '', [], $page, true, true);
      }
      foreach($contact_list as $contact) {
        $this->SetContactID($contact->getName(), $contact->getContactId());
      }
      $contact_id = $this->GetContactID($data['accountName']);
      if ($contact_id === '') {
        // Create a new contact in Xero
        $contact = new XeroAPI\XeroPHP\Models\Accounting\Contact;
        $contact->setName($data['accountName']);
        $arr_contacts = [];
        array_push($arr_contacts, $contact);
        $contacts = new XeroAPI\XeroPHP\Models\Accounting\Contacts;
        $contacts->setContacts($arr_contacts);
        $apiResponse = $apiInstance->createContacts($config['tenant_id'],
                                                    $contacts);
        $contact_id = $apiResponse->getContacts()[0]->getContactId();
        if ($contact_id === '') {
          $this->Log('Xero2->SendInvoice 3: contact id not found for ' .
                     $data['accountName']);
          return;
        }
      }
    }

    $contact = new XeroAPI\XeroPHP\Models\Accounting\Contact;
    $contact->setContactID($contact_id);

    $lineItems = [];
    foreach($data['lineItems'] as $item) {
      $lineItem = new XeroAPI\XeroPHP\Models\Accounting\LineItem;
      $lineItem->setDescription($item['description']);
      $lineItem->setQuantity($item['quantity']);
      $lineItem->setUnitAmount($item['unitAmount']);
      $lineItem->setAccountCode($item['accountCode']);
      if ($item['taxable'] === 1) $lineItem->setTaxType('OUTPUT');
      else $lineItem->setTaxType('EXEMPTOUTPUT');
      array_push($lineItems, $lineItem);
    }

    $invoice = new XeroAPI\XeroPHP\Models\Accounting\Invoice;
    $invoice->setType(XeroAPI\XeroPHP\Models\Accounting\Invoice::TYPE_ACCREC);
    $invoice->setContact($contact);
    $invoice->setDate($data['date']);
    $invoice->setDueDate($data['dueDate']);
    $invoice->setInvoiceNumber($data['invoiceNumber']);
    $invoice->setTotalTax($data['totalTax']);
    $invoice->setTotal($data['total']);
    $invoice->setLineAmountTypes('Inclusive');
    $invoice->setLineItems($lineItems);

    $invoices = new XeroAPI\XeroPHP\Models\Accounting\Invoices;
    $arr_invoices = [];
    array_push($arr_invoices, $invoice);
    $invoices->setInvoices($arr_invoices);
    $summarizeErrors = false;
    $unitdp = 2;
    try {
      $result = $apiInstance->createInvoices($config['tenant_id'], $invoices,
                                             $summarizeErrors, $unitdp);
    } catch (Exception $e) {
      $this->Log('Xero2->SendInvoice 4: ' . $e->getMessage());
    }
  }

  private function GetContactID($account_name) {
    $contact_id = '';
    $mysqli = connect_db();
    $query = 'SELECT contact_id FROM xero_contacts WHERE account_name = "' .
      $account_name . '" AND system_group = "' . $this->user->group . '"';
    if ($result = $mysqli->query($query)) {
      if ($xero_contacts = $result->fetch_assoc()) {
        $contact_id = $xero_contacts['contact_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Xero2->GetContactID: ' . $mysqli->error);
    }
    $mysqli->close();
    return $contact_id;
  }

  private function SetContactID($account_name, $contact_id) {
    if ($this->GetContactID($account_name) !== '') return;

    $mysqli = connect_db();
    $query = 'INSERT INTO xero_contacts VALUES ("' . $account_name . '", ' .
      '"' . $contact_id . '", "' . $this->user->group . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Xero2->SetContactID: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function Submit() {
    $mysqli = connect_db();
    $enabled = (int)$_POST['enabled'];
    $query = 'UPDATE xero_tokens SET enabled = ' . $enabled .
      ' WHERE system_group = "' . $this->user->group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Xero2->Submit: ' . $mysqli->error);
    }
    $mysqli->close();

    $config = $this->Config();
    if ($enabled === 0 || $config['refresh_token'] != '') {
      return ['done' => true];
    }

    include 'vendor/autoload.php';
    $options = ['scope' => ['openid email profile offline_access assets ' .
      'projects accounting.settings accounting.transactions ' .
      'accounting.contacts accounting.journals.read accounting.reports.read ' .
      'accounting.attachments']];
    $provider = $this->GetProvider();
    $authorizationUrl = $provider->getAuthorizationUrl($options);
    $_SESSION['oauth2state'] = $provider->getState();
    return ['location' => $authorizationUrl];
  }

  private function StoreToken() {
    include 'vendor/autoload.php';
    $provider = $this->GetProvider();
    try {
      // Try to get an access token using the authorization code grant.
      $accessToken = $provider->getAccessToken('authorization_code', [
        'code' => $_GET['code']]);

      $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->
        setAccessToken( (string)$accessToken->getToken() );
      $identityInstance = new XeroAPI\XeroPHP\Api\IdentityApi(
        new GuzzleHttp\Client(),
        $config);

      $result = $identityInstance->getConnections();

      $mysqli = connect_db();
      $token = $mysqli->escape_string($accessToken->getToken());
      $expires = (int)$accessToken->getExpires();
      $tenant_id = $mysqli->escape_string($result[0]->getTenantId());
      $refresh_token = $mysqli->escape_string($accessToken->getRefreshToken());
      $id_token = $mysqli->escape_string($accessToken->getValues()['id_token']);
      $query = 'INSERT INTO xero_tokens VALUES ("' . $token . '", ' .
        $expires . ', "' . $tenant_id . '", "' . $refresh_token . '", ' .
        '"' . $id_token . '", 1, "' . $this->user->group . '") ' .
        'ON DUPLICATE KEY UPDATE token = "' . $token . '", ' .
        'expires = ' . $expires . ', tenant_id = "' . $tenant_id . '", ' .
        'refresh_token = "' . $refresh_token . '", ' .
        'id_token = "' . $id_token . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Xero->StoreToken: ' . $mysqli->error);
      }
      $mysqli->close();

      echo 'Token saved.';
    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
      echo 'Callback failed';
      exit;
    }
  }
}
