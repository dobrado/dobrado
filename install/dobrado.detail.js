// @source: /js/source/dobrado.detail.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.detail){dobrado.detail={};}
(function(){'use strict';$(function(){if($('.detail').length===0){return;}
$('#detail-form').dialog({show:true,autoOpen:false,width:500,position:{my:'top',at:'top+50',of:window},title:'Edit your details',create:dobrado.fixedDialog});$('.detail .edit a').click(edit);$('.detail-thumb-browse').button().click(browser);$('#detail-form .submit').button().click(submit);});function edit(){$('#detail-form').dialog('open');}
function browser(){dobrado.createModule('browser','browser','detail');return false;}
function submit(){dobrado.log('Saving user details','info');var thumb=$('#detail-thumb-input').val();$.post('/php/request.php',{request:'detail',first:$('#detail-first-input').val(),last:$('#detail-last-input').val(),thumb:thumb,follow:$('#detail-follow-input').val(),action:$('#detail-action-input').val(),phone:$('#detail-phone-input').val(),address:$('#detail-address-textarea').val(),description:$('#detail-description-textarea').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'detail request')){return;}
location.reload();});return false;}
dobrado.detail.select=function(filename){$('#detail-thumb-input').val(filename);};}());