/*global dobrado: true, Slick: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.roster) {
  dobrado.roster = {};
}
(function() {

  'use strict';

  // This is a representation of the roster in json.
  var roster = {};
  // Store the role descriptions to pre-fill when editing existing roles.
  var description = {};
  // This is an instance of slick grid, if available on the page.
  var grid = null;
  var gridId = '';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.roster').length === 0) {
      return;
    }
    $('.roster-role-dialog').dialog({
      show: true,
      autoOpen: false,
      width: 450,
      height: 400,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Edit Roles',
      create: dobrado.fixedDialog });
    $('.roster-edit-roles').button().click(editRoles);
    $('#roster-form .remove').button().click(remove);
    $('#roster-form .submit').button().click(submit);
    $('.roster-search-submit').button().click(search);
    $('#roster-show-previous').button({
      icon: 'ui-icon-caret-1-w', showLabel: false }).click(previousDate);
    $('#roster-show-next').button({
      icon: 'ui-icon-caret-1-e', showLabel: false }).click(nextDate);
    $('#roster-date-input').val('').datepicker({
      dateFormat: dobrado.dateFormat }).change(showRoster);
    $('#roster-start-input').val('').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#roster-end-input').val('').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#roster-role-select').change(showRoleDescription);

    dobrado.log('Loading roster...', 'info');
    $.post('/php/request.php',
           { request: 'roster', action: 'list',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster list')) {
          return;
        }
        roster = JSON.parse(response);
        updateNames();
        var timestamp = $('.roster-next-date').html();
        $('#roster-date-input').datepicker('setDate', timestamp);
        // Set the roster role description to the first in the list.
        $.each(roster.roles, function(role, description) {
          $('#roster-role-description').html(description);
          // Make sure the select doesn't get out of sync with the description.
          $('#roster-role-select').val(dobrado.decode(role));
          return false;
        });

        if ($('.grid').length !== 0) {
          gridSetup();
        }
      });
  });

  function gridSetup() {
    gridId = '#' + $('.grid').attr('id');
    var columns = [{ id : 'date', name: 'Date', field: 'date', width: 140,
                     formatter: Slick.Formatters.Timestamp },
                   { id : 'username', name: 'Username', field: 'username',
                     width: 120 }];
    if ($('.dobrado-mobile').is(':hidden')) {
      columns.push({ id : 'fullname', name: 'Full Name', field: 'first',
                     width: 220, formatter: Slick.Formatters.Fullname });
    }
    columns.push({ id : 'role', name: 'Role', field: 'role', width: 200 });
    columns.push({ id : 'phone', name: 'Phone', field: 'phone', width: 120 });
    var options = { autoHeight: true, forceFitColumns: true };
    grid = dobrado.grid.instance(gridId, [], columns, options);
    grid.setSelectionModel(new Slick.RowSelectionModel());
    grid.onClick.subscribe(function(e, item) {
      showUser(item.row);
    });
    // The grid is only shown to users with permission.
    $(gridId).hide();
  }

  function previousDate() {
    var timestamp = $('.roster-next-date').html();
    dobrado.log('Showing previous roster...', 'info');
    $.post('/php/request.php',
           { request: 'roster', action: 'previousDate', timestamp: timestamp,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster previousDate')) {
          return;
        }
        var show = JSON.parse(response);
        $('.roster-next-date').html(show.date);
        $('.roster-show-date').html(show.content);
        $('#roster-date-input').datepicker('setDate', show.date);
      });
  }

  function nextDate() {
    var timestamp = $('.roster-next-date').html();
    dobrado.log('Showing next roster...', 'info');
    $.post('/php/request.php',
           { request: 'roster', action: 'nextDate', timestamp: timestamp,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster nextDate')) {
          return;
        }
        var show = JSON.parse(response);
        $('.roster-next-date').html(show.date);
        $('.roster-show-date').html(show.content);
        $('#roster-date-input').datepicker('setDate', show.date);
      });
  }

  function showRoster() {
    // Convert the date input into a timestamp.
    var timestamp = $.datepicker.formatDate('@',
                      $('#roster-date-input').datepicker('getDate'));
    if (timestamp === '') {
      return;
    }
    
    dobrado.log('Showing roster...', 'info');
    $.post('/php/request.php',
           { request: 'roster', action: 'showRoster', timestamp: timestamp,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster showRoster')) {
          return;
        }
        var show = JSON.parse(response);
        $('.roster-next-date').html(show.date);
        $('.roster-show-date').html(show.content);
      });
  }

  function showUser(row) {
    var user = roster.content[row];
    $('#roster-username-input').val(user.username);
    $('#roster-firstname-input').val(dobrado.decode(user.first));
    $('#roster-lastname-input').val(dobrado.decode(user.last));
    $('#roster-phone-input').val(dobrado.decode(user.phone));
    $('#roster-date-input').val(dobrado.formatDate(user.date));
    $('#roster-role-select').val(dobrado.decode(user.role));
    if (roster.roles) {
      $('#roster-role-description').html(roster.roles[user.role]);
    }
  }

  function showRoleDescription() {
    if (roster.roles) {
      $('#roster-role-description').html(roster.roles[$(this).val()]);
    }
  }

  function usernameUpdate(event, ui) {
    var user = $('#roster-username-input').val();
    $('#roster-firstname-input').val('');
    $('#roster-lastname-input').val('');
    $('#roster-phone-input').val('');

    // ui is set for autocomplete select event, otherwise function called
    // from change event.
    if (ui) {
      user = ui.item.value;
    }
    $.each(roster.details, function(key, value) {
      if (key === user) {
        if (value.first && value.last && value.phone) {
          $('#roster-firstname-input').val(dobrado.decode(value.first));
          $('#roster-lastname-input').val(dobrado.decode(value.last));
          $('#roster-phone-input').val(dobrado.decode(value.phone));
        }
        return false;
      }
    });
  }

  function updateNames() {
    // Create an autocomplete list of usernames.
    var usernames = [];
    $.each(roster.users, function(index, item) {
      usernames.push(item);
    });
    $('#roster-username-input').autocomplete({ minLength: 1,
                                               search: dobrado.fixAutoCompleteMemoryLeak,
                                               source: usernames,
                                               select: usernameUpdate });
    $('#roster-username-input').change(usernameUpdate);
  }

  function remove() {
    update('remove');
    return false;
  }

  function submit() {
    update('submit');
    return false;
  }

  function update(action) {
    // Convert the date input into a timestamp.
    var timestamp = $.datepicker.formatDate('@',
                      $('#roster-date-input').datepicker('getDate'));
    if (timestamp === '') {
      return;
    }
    
    if (action === 'submit') {
      dobrado.log('Saving user...', 'info');
    }
    else if (action === 'remove') {
      dobrado.log('Removing user...', 'info');
    }

    $.post('/php/request.php',
           { request: 'roster', action: action,
             username: $('#roster-username-input').val(),
             first: $('#roster-firstname-input').val(),
             last: $('#roster-lastname-input').val(),
             phone: $('#roster-phone-input').val(),
             role: $('#roster-role-select').val(),
             timestamp: timestamp, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster remove')) {
          return;
        }
        var showDate = JSON.parse(response);
        $('.roster-show-date').html(showDate.content);
        $('#roster-username-input').val('');
        $('#roster-firstname-input').val('');
        $('#roster-lastname-input').val('');
        $('#roster-phone-input').val('');
        $('#roster-date-input').val('');
      });
  }

  function editRoles() {

    function addRole() {
      var role = $('#roster-add-role').val();
      if (role === '') {
        return false;
      }
      var newRole = true;
      // Update the stored value of the description and number required.
      $.each(description, function(key, value) {
        if (key === role) {
          value.description = $('#roster-description').val();
          value.requires = $('#roster-requires').val();
          newRole = false;
          return false;
        }
      });
      if (newRole) {
        dobrado.log('Adding role...', 'info');
        // Add the new role to the stored descriptions, and to autocomplete.
        description[role] = { description: $('#roster-description').val(),
                              requires: $('#roster-requires').val(),
                              display: $('#roster-display:checked').length };
        var source = $('#roster-add-role').autocomplete('option', 'source');
        source.push(role);
        $('#roster-add-role').autocomplete('option', 'source', source);
      }
      else {
        dobrado.log('Updating role...', 'info');
      }
      $.post('/php/request.php',
             { request: 'roster', action: 'add-role', role: role,
               requires: $('#roster-requires').val(),
               description: $('#roster-description').val(),
               display: $('#roster-display:checked').length,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'roster add-role')) {
            return;
          }
          var roles = JSON.parse(response);
          $('#roster-add-role').val('');
          $('#roster-description').val('');
          $('#roster-requires').val('');
          $('#roster-display').prop('checked', true);
          $('#roster-role-select').html(roles.options);
          $('.roster-list-roles').html(roles.content);
          $('.roster-remove-role').button({
            icon: 'ui-icon-closethick', showLabel: false }).click(removeRole);
        });
      return false;
    }

    function removeRole() {
      var role = $(this).siblings('.roster-role').html();
      dobrado.log('Removing role...', 'info');
      $.post('/php/request.php',
             { request: 'roster', action: 'remove-role', role: role,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'roster remove-role')) {
            return;
          }
          var roles = JSON.parse(response);
          $('#roster-add-role').val('');
          $('#roster-requires').val('');
          $('#roster-description').val('');
          $('#roster-display').prop('checked', true);
          $('#roster-role-select').html(roles.options);
          $('.roster-list-roles').html(roles.content);
          $('.roster-remove-role').button({
            icon: 'ui-icon-closethick', showLabel: false }).click(removeRole);
        });
      return false;
    }

    function roleUpdate(event, ui) {
      var role = $('#roster-add-role').val();
      if (ui) {
        role = ui.item.value;
      }
      $.each(description, function(key, value) {
        if (key === role) {
          $('#roster-description').val(dobrado.decode(value.description));
          $('#roster-requires').val(value.requires);
          $('#roster-display').prop('checked', value.display === 1);
          return false;
        }
      });
    }

    dobrado.log('Loading roles...', 'info');
    $.post('/php/request.php',
           { request: 'roster', action: 'edit-roles',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster edit-roles')) {
          return;
        }
        var roles = JSON.parse(response);
        $('.roster-role-dialog').dialog('open');
        $('.roster-role-dialog').html(roles.content);
        $('.roster-add-role-button').button().click(addRole);
        $('.roster-remove-role').button({
          icon: 'ui-icon-closethick', showLabel: false }).click(removeRole);
        description = roles.description;
        // Autocomplete the current roles.
        var roleNames = [];
        $.each(description, function(key, value) {
          roleNames.push(dobrado.decode(key));
        });
        $('#roster-add-role').autocomplete({ minLength: 1,
                                             search: dobrado.fixAutoCompleteMemoryLeak,
                                             source: roleNames,
                                             select: roleUpdate });
        $('#roster-add-role').change(roleUpdate);
      });
    return false;
  }

  function search() {
    var start = parseInt($.datepicker.formatDate('@',
      $('#roster-start-input').datepicker('getDate')), 10);
    if (!start) {
      start = '';
    }
    var end = parseInt($.datepicker.formatDate('@',
      $('#roster-end-input').datepicker('getDate')), 10);
    if (!end) {
      end = '';
    }
    var exportData = $('#roster-export:checked').length;

    dobrado.log('Searching...', 'info');
    $.post('/php/request.php',
           { request: 'roster', action: 'search', start: start, end: end,
             exportData: exportData, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'roster search')) {
          return;
        }
        var data = JSON.parse(response);
        roster.content = data.content;
        if (roster.content.length === 0) {
          $('.roster-search-info').html('No data found for search.');
          $(gridId).hide();
          return;
        }
        if (grid) {
          $(gridId).show();
          grid.setData(roster.content);
          grid.updateRowCount();
          grid.render();
        }
        if (exportData) {
          location.href = '/php/private.php?file=' + data.filename;
        }
      });
    return false;
  }

}());
