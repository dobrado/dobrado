/*global dobrado: true, Slick: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.summary) {
  dobrado.summary = {};
}
(function() {

  'use strict';

  // This is an instance of slick grid, if available on the page.
  var purchaseGrid = null;
  var purchaseGridId = "";
  // If there is another grid module, it is used to display payments.
  var paymentGrid = null;
  var paymentGridId = "";
  var oneWeekAgo = new Date().getTime() - (86400000 * 7);

  $(function() {
    // Don't run if the module isn't on the page.
    if ($(".summary").length === 0) {
      return;
    }

    $("#summary-available-orders").change(showOrder);
    $(".view-previous a").click(function() { $("#summary-form").toggle(); });
    $("#summary-start-input").val(dobrado.formatDate(oneWeekAgo)).datepicker({
      dateFormat: dobrado.dateFormat });
    $("#summary-end-input").val(dobrado.formatDate()).datepicker({
      dateFormat: dobrado.dateFormat });
    $("#summary-form .submit").button().click(view);
    // If a grid module is on the page initialise columns for purchase data.
    if ($(".grid").length !== 0) {
      gridSetup();
    }
  });

  function gridSetup() {
    // Get the id's for all the grid modules on the page.
    $('.grid').each(function(index) {
      if (index === 0) {
        purchaseGridId = '#' + $(this).attr('id');
      }
      if (index === 1) {
        paymentGridId = '#' + $(this).attr('id');
      }
    });
    var mobile = $('.dobrado-mobile').is(':visible');
    var columns = [{ id : 'product', name: 'Product', field: 'name',
                     width: 230 }];
    if (!mobile) {
      columns.push({ id : 'supplier', name: 'Supplier', field: 'supplier',
                     width: 100 });
    }
    columns.push({ id : 'date', name: 'Date', field: 'date', width: 110,
                   formatter: Slick.Formatters.Timestamp });
    if (!mobile) {
      columns.push({ id : 'quantity', name: 'Quantity', field: 'quantity',
                     width: 90 });
      columns.push({ id : 'price', name: 'Price', field: 'price', width: 80,
                     formatter: Slick.Formatters.Dollar });
    }
    columns.push({ id : 'total', name: 'Total', field: 'total', width: 80,
                   formatter: Slick.Formatters.Dollar });
    var options = { autoHeight: true, forceFitColumns: true };
    purchaseGrid = dobrado.grid.instance(purchaseGridId, [], columns, options);
    // If a second grid module is on the page initialise columns for payments.
    if ($('.grid').length === 2) {
      columns = [{ id : 'date', name: 'Date', field: 'date', width: 110,
                   formatter: Slick.Formatters.Timestamp },
                 { id : 'comment', name: 'Comment', field: 'comment',
                   width: 300 },
                 { id : 'amount', name: 'Amount', field: 'amount', width: 80,
                   formatter: Slick.Formatters.Dollar }];
      paymentGrid = dobrado.grid.instance(paymentGridId, [], columns, options);
    }
    // Hide grid modules after column widths have been computed.
    $('.grid').hide();
  }

  function showOrder() {
    var group = $(this).val();
    $(".summary-order").hide();
    $(".summary-group-" + group).show();
    $.post("/php/request.php", { request: "summary",
                                 action: "change",
                                 group: group,
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "summary change")) {
          return;
        }
      });
  }

  function view() {
    dobrado.log("Updating view...", "info");
    $(".grid").hide();
    $(".summary .total").html("");

    var select = $("#summary-view-select").val();
    var start = parseInt($.datepicker.formatDate("@",
      $("#summary-start-input").datepicker("getDate")), 10);
    var end = parseInt($.datepicker.formatDate("@",
      $("#summary-end-input").datepicker("getDate")), 10);
    $.post("/php/request.php", { request: "summary",
                                 action: "view",
                                 select: select,
                                 start: start,
                                 end: end,
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "summary view")) {
          return;
        }
        var summary = JSON.parse(response);
        var total = 0;
        if (summary.data.length === 0) {
          $(".summary .total").html("No data for the dates given.");
          return;
        }
        var startText = $("#summary-start-input").val();
        var endText = $("#summary-end-input").val();
        if (select === "purchase" && purchaseGrid) {
          $(purchaseGridId).show();
          purchaseGrid.setData(summary.data);
          purchaseGrid.updateRowCount();
          purchaseGrid.render();
          $.each(summary.data, function(i, item) {
            total += parseFloat(item.total);
          });
          var surcharge = "";
          if (summary.surcharge !== 0) {
            surcharge = " (plus surcharge: $" + summary.surcharge.toFixed(2) +
              ")";
          }
          $(".summary .total").html("Total purchases for " + startText +
                                    " to " + endText + ": $" +
                                    total.toFixed(2) + surcharge);
        }
        else if (select === "sold" && purchaseGrid) {
          $(purchaseGridId).show();
          purchaseGrid.setData(summary.data);
          purchaseGrid.updateRowCount();
          purchaseGrid.render();
          $.each(summary.data, function(i, item) {
            total += parseFloat(item.total);
          });
          $(".summary .total").html("Total sold for " + startText + " to " +
                                    endText + ": $" + total.toFixed(2));
        }
        else if (select === "payment" && paymentGrid) {
          $(paymentGridId).show();
          paymentGrid.setData(summary.data);
          paymentGrid.updateRowCount();
          paymentGrid.render();
          $.each(summary.data, function(i, item) {
            total += parseFloat(item.amount);
          });
          $(".summary .total").html("Total payments for " + startText + " to " +
                                    endText + ": $" + total.toFixed(2));
        }
      });
    return false;
  }

}());
