<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Invite extends Base {

  public function Add($id) {
    // Members of the 'invite-notifications' group need to be able to view
    // this page.
    $mysqli = connect_db();
    $query = 'INSERT INTO group_permission VALUES ("admin", ' .
      '"' . $this->user->page . '", "invite-notifications", 0, 0, 1)';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->Add: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Callback() {
    $mysqli = connect_db();
    $action = isset($_POST['action']) ?
      $mysqli->escape_string($_POST['action']) : '';
    $group = isset($_POST['group']) ?
      $group = $mysqli->escape_string($_POST['group']) : '';

    if ($action === 'changeGroup') {
      $mysqli->close();
      if ($group === $this->user->group || in_array($group, $this->Created())) {
        $_SESSION['purchase-group'] = $group;
        $_SESSION['purchase-group-changed'] = true;
        return ['done' => true];
      }
      return ['error' => 'Group not found.'];
    }

    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    // Check if this user is an admin for the group, they need to be in the
    // same default group as the creator of the current group, and be in the
    // 'invite-notifications' admin group.
    $admin = false;
    $group_matches = false;
    $query = 'SELECT users.system_group FROM invite LEFT JOIN users ON ' .
      'invite.user = users.user WHERE name = "' . $this->user->group . '" ' .
      'AND invite.system_group = ""';
    if ($result = $mysqli->query($query)) {
      if ($invite = $result->fetch_assoc()) {
        if ($default_group === $invite['system_group']) {
          $group_matches = true;
        }
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Callback 1: ' . $mysqli->error);
    }
    if ($group_matches) {
      $query = 'SELECT visitor FROM group_names WHERE user = "admin" AND ' .
        'name = "invite-notifications" AND visitor = "' . $this->user->name.'"';
      if ($result = $mysqli->query($query)) {
        if ($result->num_rows === 1) {
          $admin = true;
        }
      }
      else {
        $this->Log('Invite->Callback 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    $result = [];
    if ($admin && $action === 'invite') {
      $result = $this->InviteGroup($group);
    }
    else if ($admin && $action === 'remove') {
      $result = $this->RemoveGroup($group);
    }
    else {
      // Otherwise the user is responding to an invite.
      $result = $this->Response($action, $default_group);
    }
    $this->user->group = $default_group;
    return $result;
  }

  public function CanAdd($page) {
    // Need admin privileges to add the invite module.
    if (!$this->user->canEditSite) return false;
    // Can only have one invite module on a page.
    return !$this->AlreadyOnPage('invite', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $default_group = $this->user->group;
    $content = '';
    $creator = '';
    $system_group = '';
    $invite_list = [];
    $all_notifications = [];
    $group_notifications = [];
    $not_notified = [];
    $mysqli = connect_db();
    // The invite in the url overrides the 'purchase-group' session variable,
    // which is also set so that callbacks work.
    if (isset($_GET['invite'])) {
      $this->user->group = $mysqli->escape_string($_GET['invite']);
      $_SESSION['purchase-group'] = $this->user->group;
      $_SESSION['purchase-group-changed'] = true;
    }
    else if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    // Display a group select if this user has created invite groups.
    $created = $this->Created();
    if (count($created) > 0) {
      $content .= '<p class="invite-display-group">Displaying invites for ' .
        '<select id="invite-group-select">' .
          '<option value="' . $default_group . '">' .
            $this->Substitute('group-name', '', '', $default_group) .
          '</option>';
      for ($i = 0; $i < count($created); $i++) {
        $group = $created[$i];
        if ($group === $this->user->group) {
          $content .= '<option selected="selected" value="' . $group . '">' .
            $this->Substitute('group-name') . '</option>';
        }
        else {
          $content .= '<option value="' . $group . '">' .
            $this->Substitute('group-name', '', '', $group) . '</option>';
        }
      }
      $content .= '</select></p>';
    }
    // Find the user who created this invite, which is done via the
    // group wizard module entering an empty system group in the table. 
    $query = 'SELECT invite.user, users.system_group FROM invite LEFT JOIN ' .
      'users ON invite.user = users.user WHERE ' .
      'name = "' . $this->user->group . '" AND invite.system_group = ""';
    if ($result = $mysqli->query($query)) {
      if ($invite = $result->fetch_assoc()) {
        $creator = $invite['user'];
        $system_group = $invite['system_group'];
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Content 1: ' . $mysqli->error);
    }
    // If this group hasn't been set up to invite others, just return now.
    if ($creator === '') {
      $content .= 'The group: <b>' . $this->Substitute('group-name') . '</b> ' .
        'cannot send out invites.<br><br>Please switch groups or use the ' .
        '<b>Group Settings</b> dialog to create a new buying group.';
      $mysqli->close();
      $this->user->group = $default_group;
      unset($_SESSION['purchase-group']);
      $_SESSION['purchase-group-changed'] = true;
      return $content;
    }

    // Get the list of groups receiving notifications for this buying group.
    $query = 'SELECT DISTINCT system_group FROM group_names LEFT JOIN users ' .
      'ON group_names.visitor = users.user WHERE group_names.user = "admin" ' .
      'AND name = "invite-notifications-' . $this->user->group . '"';
    if ($result = $mysqli->query($query)) {
      while ($group_names = $result->fetch_assoc()) {
        $group_notifications[] = $group_names['system_group'];
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Content 2: ' . $mysqli->error);
    }
    // Get the list of groups interested in receiving invite notifications for
    // new buying groups. Also want to know who hasn't been notified about the
    // current group from this list.
    $query = 'SELECT visitor, system_group FROM group_names LEFT JOIN users ' .
      'ON group_names.visitor = users.user WHERE group_names.user = "admin" ' .
      'AND name = "invite-notifications"';
    if ($result = $mysqli->query($query)) {
      while ($group_names = $result->fetch_assoc()) {
        $all_notifications[] = $group_names['visitor'];
        if (!in_array($group_names['system_group'], $group_notifications)) {
          $not_notified[] = $group_names['system_group'];
        }
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Content 3: ' . $mysqli->error);
    }
    // Get the list of invites.
    $query = 'SELECT system_group, visitor, joined FROM invite WHERE ' .
      'name = "' . $this->user->group . '" AND system_group != ""';
    if ($result = $mysqli->query($query)) {
      while ($invite = $result->fetch_assoc()) {
        $invite_list[$invite['system_group']] =
          ['visitor' => $invite['visitor'], 'joined' => $invite['joined']];
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Content 4: ' . $mysqli->error);
    }
    // To be able to modify invites for the group, the current user must be in
    // the same system group as the user who created the invite and also be a
    // member of the 'invite-notifications' admin group.
    if ($default_group === $system_group &&
        in_array($this->user->name, $all_notifications)) {
      $accepted_count = 0;
      $declined_count = 0;
      $waiting_count = 0;
      $accepted_content = '';
      $declined_content = '';
      $waiting_content = '';
      foreach ($invite_list as $group => $details) {
        $joined = $details['joined'];
        if ($joined === 'accepted') {
          $accepted_count++;
          $accepted_content .= '<div class="form-spacing">' .
            '<button class="invite-remove">remove</button>' .
            '<span class="invite-remove-group">' . $group . '</span></div>';
        }
        else if ($joined === 'declined') {
          $declined_count++;
          $declined_content .= '<div class="form-spacing">' .
            '<button class="invite-remove">remove</button>' .
            '<span class="invite-remove-group">' . $group . '</span></div>';
        }
        else if ($joined === 'waiting') {
          $waiting_count++;
          $waiting_content .= '<div class="form-spacing">' .
            '<button class="invite-remove">remove</button>' .
            '<span class="invite-remove-group">' . $group . '</span></div>';
        }
      }
      $content .= '<p>Invite settings for <b>' .
        $this->Substitute('group-name') . '</b></p>';
      if ($accepted_count === 1) {
        $content .= '<p><b>1</b> group has accepted the invitation:<br>' .
          $accepted_content . '</p>';
      }
      else if ($accepted_count !== 0) {
        $content .= '<p><b>' . $accepted_count . '</b> groups have accepted ' .
          'the invitation:<br>' . $accepted_content . '</p>';
      }
      if ($declined_count === 1) {
        $content .= '<p><b>1</b> group has declined the invitation:<br>' .
          $declined_content . '</p>';
      }
      else if ($declined_count !== 0) {
        $content .= '<p><b>' . $declined_count . '</b> groups have declined ' .
          'the invitation:<br>' . $declined_content . '</p>';
      }
      if ($waiting_count === 1) {
        $content .= '<p><b>1</b> group has not replied to the invitation:<br>' .
          $waiting_content . '</p>';
      }
      else if ($waiting_count !== 0) {
        $content .= '<p><b>' . $waiting_count . '</b> groups have not ' .
          'replied to the invitation:<br>' . $waiting_content . '</p>';
      }
      // Offer to invite other groups that haven't been invited yet.
      if (count($not_notified) !== 0) {
        $content .= '<p>You can invite other groups to join.<br>' .
          'These groups haven\'t been invited yet: ' .
          '<select id="invite-select">';
        $group_query = '';
        for ($i = 0; $i < count($not_notified); $i++) {
          if ($group_query !== '') {
            $group_query .= ' OR ';
          }
          $group_query .= 'system_group = "' . $not_notified[$i] . '"';
        }
        $query = 'SELECT DISTINCT system_group FROM users WHERE ' .$group_query;
        if ($result = $mysqli->query($query)) {
          while ($users = $result->fetch_assoc()) {
            $group = $users['system_group'];
            $content .= '<option value="' . $group . '">' .
              $this->Substitute('group-name', '', '', $group) . '</option>';
          }
          $result->close();
        }
        else {
          $this->Log('Invite->Content 5: ' . $mysqli->error);
        }
        $content .= '</select><button id="invite-add">invite</button></p>';
      }
      else {
        $content .= '<p>There are no other groups to invite.<br>' .
          'If you would like to invite someone to join this buying group, ' .
          'please ask them to join <b>invite-notifications</b>.';
      }
    }
    if (in_array($default_group, $group_notifications)) {
      if (isset($invite_list[$default_group])) {
        $visitor = $invite_list[$default_group]['visitor'];
        $joined = $invite_list[$default_group]['joined'];
        $content .= '<p class="invite-status">';
        if ($joined !== 'waiting') {
          if ($this->user->name === $visitor) {
            $content .= 'You have ' . $joined . ' the invitation to join ' .
              '<b>' . $this->Substitute('group-name') . '</b>.</p>';
          }
          else {
            $content .= 'Another member <b>' . $visitor . '</b>, has ' .
              $joined . ' the invitation to join <b>' .
              $this->Substitute('group-name') . '</b>.</p>';
          }
          $content .= '<p class="invite-description">' .
              $this->Substitute('group-description') . '</p>' .
            'You can change the status: ';
        }
        else {
          $content .= 'You have been invited to join the group <b>' .
            $this->Substitute('group-name') . '</b>.</p>' .
            '<p class="invite-description">' .
              $this->Substitute('group-description') . '</p>';
        }
        $detail = new Detail($this->user, $this->owner);
        $user = $detail->User($creator);
        $content .= '<button id="invite-accept">accept</button>' .
          '<button id="invite-decline">decline</button>' .
          '<p class="invite-contact">' .
            'Contact details for this group:<br>Contact ' . $user['first'] .
            ' ' . $user['last'] . '<br>Username ' . $creator . '<br>Email ' .
            $user['email'] . '</p>';
      }
      else {
        $content .= 'You cannot join the group <b>' .
          $this->Substitute('group-name') . '</b>.';
        unset($_SESSION['purchase-group']);
        $_SESSION['purchase-group-changed'] = true;
      }
    }
    else {
      $content .= 'You must receive invite notifications to make changes here.';
      unset($_SESSION['purchase-group']);
      $_SESSION['purchase-group-changed'] = true;
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'Exists') {
      return $this->Exists($p);
    }
    if ($fn === 'Created') {
      return $this->Created();
    }
    if ($fn === 'Joined') {
      return $this->Joined();
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.invite.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.invite.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS invite (' .
      'user VARCHAR(50) NOT NULL,' .
      'name VARCHAR(50) NOT NULL,' .
      'system_group VARCHAR(50) NOT NULL,' .
      'visitor VARCHAR(50),' .
      'joined ENUM("accepted", "declined", "waiting") NOT NULL,' .
      'PRIMARY KEY(user, name, system_group)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    $site_style = ['"",".invite-description","background-color","#eeeeee"',
                   '"",".invite-description","border","1px solid #aaaaaa"',
                   '"",".invite-description","border-radius","2px"',
                   '"",".invite-description","padding","5px"',
                   '"",".invite-contact","background-color","#eeeeee"',
                   '"",".invite-contact","border","1px solid #aaaaaa"',
                   '"",".invite-contact","border-radius","2px"',
                   '"",".invite-contact","padding","5px"',
                   '"","#invite-decline","margin-left","10px"'];
    $this->AddSiteStyle($site_style);

    return $this->Dependencies(['detail', 'notification', 'purchase']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.invite.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function AllBuyingGroups() {
    $groups = [];
    $mysqli = connect_db();
    $query = 'SELECT name FROM invite WHERE ' .
      'system_group = "' . $this->user->group . '" AND joined = "accepted"';
    if ($result = $mysqli->query($query)) {
      while ($invite = $result->fetch_assoc()) {
        $groups[] = $invite['name'];
      }
    }
    else {
      $this->Log('Invite->AllBuyingGroups: ' . $mysqli->error);
    }
    $mysqli->close();
    return $groups;
  }

  public function OpenBuyingGroups() {
    $purchase = new Purchase($this->user, $this->owner);
    $default_group = $this->user->group;
    $groups = [];
    $mysqli = connect_db();
    $query = 'SELECT name FROM invite WHERE ' .
      'system_group = "' . $this->user->group . '" AND joined = "accepted"';
    if ($result = $mysqli->query($query)) {
      while ($invite = $result->fetch_assoc()) {
        $this->user->group = $invite['name'];
        if ($purchase->PurchasingAvailable() ||
            ($this->Substitute('pre-order') === 'true' &&
            $purchase->OrderingAvailable())) {
          $groups[] = $this->user->group;
        }
      }
    }
    else {
      $this->Log('Invite->OpenBuyingGroups: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $groups;
  }

  public function Created() {
    $created = [];
    $mysqli = connect_db();
    $query = 'SELECT name FROM invite WHERE user = "' . $this->user->name .'" '.
      'AND system_group = ""';
    if ($result = $mysqli->query($query)) {
      while ($invite = $result->fetch_assoc()) {
        $created[] = $invite['name'];
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Created: ' . $mysqli->error);
    }
    $mysqli->close();
    return $created;
  }

  public function Host() {
    $host = false;
    $mysqli = connect_db();
    $query = 'SELECT name FROM invite WHERE name = "' . $this->user->group.'" '.
      'AND system_group = ""';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows === 1) {
        $host = true;
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Host: ' . $mysqli->error);
    }
    $mysqli->close();
    return $host;
  }

  public function Joined() {
    $joined = [];
    $mysqli = connect_db();
    $query = 'SELECT system_group FROM invite WHERE ' .
      'name = "' . $this->user->group . '" AND system_group != "" AND ' .
      'joined = "accepted"';
    if ($result = $mysqli->query($query)) {
      while ($invite = $result->fetch_assoc()) {
        $joined[] = $invite['system_group'];
      }
    }
    else {
      $this->Log('Invite->Joined: ' . $mysqli->error);
    }
    $mysqli->close();
    return $joined;
  }

  public function Open($default_group) {
    $exists = false;
    $mysqli = connect_db();
    $query = 'SELECT name FROM invite WHERE name = "' . $this->user->group .'"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows !== 0) {
        $exists = true;
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Open 1: ' . $mysqli->error);
    }
    // Also check there are no existing users with this system_group.
    $query = 'SELECT DISTINCT system_group FROM users WHERE ' .
      'system_group = "' . $this->user->group . '"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows !== 0) {
        $exists = true;
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Open 2: ' . $mysqli->error);
    }
    if ($exists) {
      $mysqli->close();
      return false;
    }

    // Inserting an empty system_group signifies that this user is the creator. 
    $query = 'INSERT INTO invite VALUES ("' . $this->user->name . '", ' .
      '"' . $this->user->group . '", "", "", "accepted")';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->Open 2: ' . $mysqli->error);
    }
    // Also add the user's default group, otherwise it won't show up on the
    // summary page, which means the session variable can't be set to add
    // any other groups on the invite page.
    $query = 'INSERT INTO invite VALUES ("' . $this->user->name . '", ' .
      '"' . $this->user->group . '", "' . $default_group . '", ' .
      '"' . $this->user->name . '", "accepted")';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->Open 3: ' . $mysqli->error);
    }
    $query = 'INSERT INTO group_names VALUES ("admin", "invite-notifications-' .
      $this->user->group . '", "' . $this->user->name . '", 1)';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->Open 4: ' . $mysqli->error);
    }
    // Add default group-name here as it's a required template.
    $query = 'INSERT INTO template VALUES ("group-name", ' .
      '"' . $this->user->group . '", "' . $this->user->group . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->Open 5: ' . $mysqli->error);
    }
    $mysqli->close();
    return true;
  }

  public function Exists($group) {
    $exists = false;
    $mysqli = connect_db();
    $query = 'SELECT name FROM invite WHERE name = "' . $group . '"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows !== 0) {
        $exists = true;
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Exists: ' . $mysqli->error);
    }
    $mysqli->close();
    return $exists;
  }

  // Private functions below here ////////////////////////////////////////////

  private function InviteGroup($group) {
    $notify = [];
    $mysqli = connect_db();
    // First look for members of this group who want to receive notifications.
    $query = 'SELECT visitor FROM group_names LEFT JOIN users ON ' .
      'group_names.visitor = users.user WHERE group_names.user = "admin" AND ' .
      'name = "invite-notifications" AND system_group = "' . $group . '"';
    if ($result = $mysqli->query($query)) {
      while ($invite = $result->fetch_assoc()) {
        $notify[] = $invite['visitor'];
      }
      $result->close();
    }
    else {
      $this->Log('Invite->InviteGroup 1: ' . $mysqli->error);
    }
    if (count($notify) === 0) {
      $mysqli->close();
      // This shouldn't happen because invites are only shown for groups that
      // have been added to 'invite-notifications'. 
      return ['error' => 'This group doesn\'t have a contact person.'];
    }

    $query = 'INSERT INTO invite VALUES ("' . $this->user->name . '", ' .
      '"' . $this->user->group . '", "' . $group . '", "", "waiting") ' .
      'ON DUPLICATE KEY UPDATE user = "' . $this->user->name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->InviteGroup 2: ' . $mysqli->error);
    }
    $content = ['author' => $this->user->name,
                'description' => $this->Substitute('group-name'),
                'category' => 'invite',
                'permalink' => $this->Url('invite=' . $this->user->group),
                'feed' => '', 'public' => 0];
    // Add the members of the group who want to receive notifications to the
    // specific notifications for this buying group. Ignore duplicates.
    for ($i = 0; $i < count($notify); $i++) {
      $visitor = $notify[$i];
      $query = 'INSERT INTO group_names VALUES ' .
        '("admin", "invite-notifications-' . $this->user->group . '", ' .
        '"' . $visitor . '", 0) ON DUPLICATE KEY UPDATE edit_group = 0';
      if (!$mysqli->query($query)) {
        $this->Log('Invite->InviteGroup 3: ' . $mysqli->error);
      }
      // Also send them a notification when they are added.
      $notification = new Notification($this->user, $visitor);
      $id = new_module($this->user, $visitor, 'notification', '',
                       $notification->Group(), 'outside');
      $notification->SetContent($id, $content);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function RemoveGroup($group) {
    $mysqli = connect_db();
    $query = 'DELETE FROM invite WHERE name = "' . $this->user->group . '" ' .
      'AND system_group = "' . $group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->RemoveGroup 1: ' . $mysqli->error);
    }
    $query = 'DELETE group_names FROM group_names LEFT JOIN users ON ' .
      'group_names.visitor = users.user WHERE group_names.user = "admin" AND ' .
      'name = "invite-notifications-' . $this->user->group . '" AND ' .
      'system_group = "' . $group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Invite->RemoveGroup 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function Response($action, $default_group) {
    $invited = false;
    $mysqli = connect_db();
    $query = 'SELECT visitor FROM group_names WHERE user = "admin" AND ' .
      'name = "invite-notifications-' . $this->user->group . '" AND ' .
      'visitor = "' . $this->user->name . '"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows === 1) {
        $invited = true;
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Response 1: ' . $mysqli->error);
    }
    if (!$invited) {
      $mysqli->close();
      return ['error' => 'Permission denied.'];
    }

    // Get the current status of the response to the invite.
    $joined = '';
    $query = 'SELECT joined FROM invite WHERE ' .
      'name = "' . $this->user->group . '" AND ' .
      'system_group = "' . $default_group . '"';
    if ($result = $mysqli->query($query)) {
      if ($invite = $result->fetch_assoc()) {
        $joined = $invite['joined'];
      }
      $result->close();
    }
    else {
      $this->Log('Invite->Response 2: ' . $mysqli->error);
    }
    $result = [];
    if ($joined === '') {
      $result['error'] = 'Your group has not been invited to join this group.';
    }
    else if ($action === $joined) {
      $result['error'] = 'Invite status hasn\'t changed.';
    }
    else if ($action === 'accepted' || $action === 'declined') {
      $query = 'UPDATE invite SET joined = "' . $action . '", ' .
        'visitor = "' . $this->user->name . '" WHERE ' .
        'name = "' . $this->user->group . '" AND ' .
        'system_group = "' . $default_group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Invite->Response 3: ' . $mysqli->error);
      }
      $result['done'] = true;
    }
    else {
      $result['error'] = 'Unknown action.';
    }
    $mysqli->close();

    // This session variable is set in Content() but don't want to leave it
    // set once an invited user has provided their response.
    unset($_SESSION['purchase-group']);
    $_SESSION['purchase-group-changed'] = true;
    return $result;
  }

}
