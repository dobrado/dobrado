<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Graph extends Base {

  public function Add($id) {
    $mysqli = connect_db();
    $feed_id = $this->RequestFeedId($id, 'graph');
    // Initialise all graph labels to empty strings, except for the first
    // entry which is reserved for 'time'.
    $query = 'INSERT INTO graph_labels VALUES ("'.$this->owner.'", '.$feed_id.
      ',"time","","","","","","","","","","","","","","","","","","","")';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->Add: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Callback() {
    $us_mode = isset($_POST['mode']) ? $_POST['mode'] : '';
    if ($us_mode !== '') {
      if ($us_mode === 'init') {
        return $this->Init();
      }
      if (!$this->user->canEditPage) {
        return ['error' => 'You don\'t have permission to edit graphs.'];
      }
      if ($us_mode === 'removeLabel') {
        return $this->RemoveLabel();
      }
      if ($us_mode === 'newLabel') {
        return $this->NewLabel();
      }
      if ($us_mode === 'newTimeGraph') {
        return $this->NewGraph('time');
      }
      if ($us_mode === 'newCustomGraph') {
        return $this->NewGraph('custom');
      }
      if ($us_mode === 'removeGraph') {
        return $this->RemoveGraph();
      }
      if ($us_mode === 'updateTitle') {
        return $this->UpdateTitle();
      }
      if ($us_mode === 'removeSeries') {
        return $this->RemoveSeries();
      }
      if ($us_mode === 'addSeries') {
        return $this->AddSeries();
      }
      if ($us_mode === 'connectModule') {
        return $this->ConnectModule();
      }
      if ($us_mode === 'disconnectModule') {
        return $this->DisconnectModule();
      }
      return ['error' => 'Unknown graph edit mode.'];
    }
    // If mode is not given, expecting feed updates here.
    $this->AddData($_POST);
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $feed_id = $this->RequestFeedId($id);
    $content = '';
    if ($this->user->canEditPage) {
      $connections = $this->ModuleConnections($feed_id);
      $class = $connections === '' ? 'hidden' : '';
      $connections = '<div class="graph-disconnect '.$class.'">'.
        '<label for="graph-disconnect-module-select">disconnect:</label>'.
        '<select id="graph-disconnect-module-select">'.$connections.
        '</select> <button id="graph-disconnect-module-button">ok</button>'.
        '</div>';
      $labels = $this->Labels($feed_id);
      $class = $labels === '' ? 'hidden' : '';
      $labels = '<div class="graph-remove-label '.$class.'">'.
        'remove label:<select id="graph-remove-label-select">'.$labels.
        '</select> <button id="graph-remove-label-button">ok</button>'.
        '</div>';
      $content .= '<button class="manage-feed-button">Manage feed</button>'.
        '<div class="manage-feed hidden">'.
          '<label for="graph-feed-input">Feed ID:</label>'.
          '<input id="graph-feed-input" type="text" '.
            'value="'.$feed_id.'" size="10" readonly="readonly"> '.
          '<label for="graph-key-input">Api Key:</label>'.
          '<input id="graph-key-input" size="10" '.
            'value="'.$this->RequestApiKey($feed_id).'" readonly="readonly">'.
          '<br>Connect a module to this Feed ID:<br>'.
          '<label for="graph-connect-module-input">module name:</label>'.
          '<input id="graph-connect-module-input" size="15"> '.
          '<button id="graph-connect-module-button">ok</button><br>'.
          $connections.
          '<div class="manage-labels">Manage labels:<br>'.
            '<label for="graph-new-label-input">new label:</label>'.
            '<input id="graph-new-label-input" size="15"> '.
            '<button id="graph-new-label-button">ok</button><br>'.
            $labels.
          '</div>'.
          'Graph types:<br>'.
          '<a href="#" id="new-time-graph">Add a time based graph</a><br>'.
          '<a href="#" id="new-custom-graph">Add a custom graph</a>'.
        '</div>'.
        '<div class="graph-format-help-contents hidden">'.
          '<h2>Graph format codes for tick labels</h2>'.
          '<p>The following is taken from documentation for jqplot and '.
            'sprintf.js. Note that the date rendering codes are only used for '.
            'the x-axis on time based graphs, otherwise the sprintf codes are '.
            'used.</p>'.
          '<h4>sprintf codes</h4>'.
          '<p>%%: A literal percent sign<br>'.
            '%b: A binary number<br>'.
            '%c: An ASCII character represented by the given value<br>'.
            '%d: A signed decimal number<br>'.
            '%f: A floating point value<br>'.
            '%o: An octal number<br>'.
            '%s: A string<br>'.
            '%x: A hexadecimal number (lowercase characters)<br>'.
            '%X: A hexadecimal number (uppercase characters)<br><br>'.
            'Precision is specified by a decimal after the %, followed by a '.
            'number. The field width is specified by a number after the %.'.
          '</p>'.
          '<h4>date rendering codes</h4>'.
          '<p>%Y: Four digit year<br>'.
            '%y: Two digit year<br>'.
            '%m: Two digit month<br>'.
            '%#m: One or two digit month<br>'.
            '%B: Full month name<br>'.
            '%b: Abbreviated month name<br>'.
            '%d: Two digit day of month<br>'.
            '%#d: One or two digit day of month<br>'.
            '%e: One or two digit day of month<br>'.
            '%A: Full name of the day of the week<br>'.
            '%a: Abbreviated name of the day of the week<br>'.
            '%w: Number of the day of the week (0 = Sunday, 6 = Saturday)<br>'.
            '%o: The ordinal suffix string following the day of the month<br>'.
            '%H: Hours in 24-hour format (two digits)<br>'.
            '%#H: Hours in 24-hour integer format (one or two digits)<br>'.
            '%I: Hours in 12-hour format (two digits)<br>'.
            '%#I: Hours in 12-hour integer format (one or two digits)<br>'.
            '%p: AM or PM<br>'.
            '%M: Minutes (two digits)<br>'.
            '%#M: Minutes (one or two digits)<br>'.
            '%S: Seconds (two digits)<br>'.
            '%#S: Seconds (one or two digits)<br>'.
            '%s: Unix timestamp<br>'.
            '%N: Milliseconds (three digits)<br>'.
            '%#N: Milliseconds (one to three digits)<br>'.
          '</p>'.
          '<button class="graph-format-close">close</button>'.
        '</div>';
    }
    $mysqli = connect_db();
    $query = 'SELECT graph_id FROM graph_instance WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' ORDER BY graph_id';
    if ($result = $mysqli->query($query)) {
      while ($graph_instance = $result->fetch_assoc()) {
        $content .= $this->Display($id, $graph_instance['graph_id']);
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Content: '.$mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      if ($fn === 'AddData' && count($p) === 2) {
        $data = $p[0];
        $module = $p[1];
        $this->AddData($data, $module);
      }
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS graph_data (' .
      'user VARCHAR(50) NOT NULL,' .
      'feed_id INT UNSIGNED NOT NULL,' .
      'sample_id INT UNSIGNED NOT NULL AUTO_INCREMENT,' .
      $this->LabelQuery(' DOUBLE, ', true) .
      'PRIMARY KEY(user, feed_id, sample_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS graph_labels (' .
      'user VARCHAR(50) NOT NULL,' .
      'feed_id INT UNSIGNED NOT NULL,' .
      $this->LabelQuery(' VARCHAR(100), ', true) .
      'PRIMARY KEY(user, feed_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->Install 2: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS graph_instance (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'graph_id INT UNSIGNED NOT NULL,' .
      'graph_type ENUM("time", "custom") NOT NULL,' .
      'display_count INT UNSIGNED NOT NULL,' .
      'x_axis_format VARCHAR(100),' .
      'y_axis_format VARCHAR(100),' .
      'title VARCHAR(200),' .
      'PRIMARY KEY(user, box_id, graph_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->Install 3: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS graph_pairs (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'graph_id INT UNSIGNED NOT NULL,' .
      'x_label VARCHAR(100),' .
      'y_label VARCHAR(100),' .
      'PRIMARY KEY(user, box_id, graph_id, x_label(50), y_label(50))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->Install 4: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS graph_connect (' .
      'user VARCHAR(50) NOT NULL,' .
      'label VARCHAR(50) NOT NULL,' .
      'feed_id INT UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, label)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->Install 5: ' . $mysqli->error);
    }

    $mysqli->close();

    // Append dobrado.graph.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.graph.js', true);
    // Add style rules to the site_style table.
    $site_style = ['"",".graph-area","height","300px"',
                   '"",".graph-area","width","400px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM api WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 1: '.$mysqli->error);
      }
      $query = 'DELETE FROM graph_instance WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 2: '.$mysqli->error);
      }
      $query = 'DELETE FROM graph_pairs WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 3: '.$mysqli->error);
      }
    }
    else {
      // If a specific id is not set remove all graph data for the user.
      $query = 'DELETE FROM graph_data WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 4: '.$mysqli->error);
      }
      $query = 'DELETE FROM graph_labels WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 5: '.$mysqli->error);
      }
      $query = 'DELETE FROM graph_instance WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 6: '.$mysqli->error);
      }
      $query = 'DELETE FROM graph_pairs WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Graph->Remove 7: '.$mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.graph.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.graph.js', true);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Display($id, $graph_id) {
    $content = '<div class="graph-instance" id="graph-'.$graph_id.'">';
    if (!$this->user->canEditPage) {
      $content .= '<div class="graph-area" id="graph-area-'.$graph_id.'">'.
        '</div></div>';
      return $content;
    }

    $graph_type = '';
    $display_count = 0;
    $mysqli = connect_db();
    $query = 'SELECT graph_type, display_count, x_axis_format, y_axis_format, '.
      'title FROM graph_instance WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND graph_id = '.$graph_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_instance = $result->fetch_assoc()) {
        $graph_type = $graph_instance['graph_type'];
        $content .= '<button class="edit">edit</button>'.
          '<button class="remove">remove</button>'.
          '<div class="graph-info hidden" id="graph-info-'.$graph_id.'">'.
            '<form class="title-form" id="title-form-'.$graph_id.'">'.
              '<label for="title-input-'.$graph_id.'">Title:</label>'.
              '<input class="title-input" id="title-input-'.$graph_id.
                '" value="'.$graph_instance['title'].'" size="30"><br>'.
              '<label for="display-count-'.$graph_id.'">Points per series:'.
              '</label>'.
              '<input class="display-count" id="display-count-'.$graph_id.
                '" value="'.$graph_instance['display_count'].'" size="5"><br>'.
              '<label for="x-axis-format-'.$graph_id.'">x-axis format:'.
              '</label>'.
              '<input class="x-axis-format" id="x-axis-format-'.$graph_id.
                '" value="'.$graph_instance['x_axis_format'].'" size="10"><br>'.
              '<label for="y-axis-format-'.$graph_id.'">y-axis format:'.
              '</label>'.
              '<input class="y-axis-format" id="y-axis-format-'.$graph_id.
                '" value="'.$graph_instance['y_axis_format'].'" size="10"><br>'.
              '<a href="#" class="graph-format-help">formatting help</a><br>'.
              '<button class="submit-title">submit</button>'.
            '</form>';
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Display 1: '.$mysqli->error);
    }

    $query = 'SELECT x_label, y_label FROM graph_pairs WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'graph_id = '.$graph_id;
    if ($result = $mysqli->query($query)) {
      while ($graph_pairs = $result->fetch_assoc()) {
        if ($graph_type === 'custom') {
          $content .= '<div class="data-series">'.
              '<button class="remove-series">remove</button>'.
              'x-axis: <span class="x-axis">'.$graph_pairs['x_label'].
              '</span>, '.
              'y-axis: <span class="y-axis">'.$graph_pairs['y_label'].'</span>'.
            '</div>';
        }
        else if ($graph_type === 'time') {
          $content .= '<div class="data-series">'.
              '<button class="remove-series">remove</button>'.
              'series: <span class="y-axis">'.$graph_pairs['y_label'].'</span>'.
            '</div>';
        }
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Display 2: '.$mysqli->error);
    }
    $mysqli->close();

    $labels = $this->Labels($this->RequestFeedId($id));
    $form = '<form class="new-series-form hidden" id="new-series-form-'.
      $graph_id.'">';
    if ($graph_type === 'custom') {
      $form .= 'x-axis: <select class="new-x-axis-select">'.$labels.
        '</select><br>y-axis: <select class="new-y-axis-select">'.$labels.
        '</select>';
    }
    else if ($graph_type === 'time') {
      $form .= 'series: <select class="new-y-axis-select">'.$labels.
        '</select>';
    }
    $form .= ' <button class="submit-series">submit</button></form>';
    $content .=
      '<a href="#" class="new-data-series">Add a new data series</a><br>'.
      $form.'</div>'.
      '<div class="graph-area" id="graph-area-'.$graph_id.'"></div></div>';

    return $content;
  }

  private function Init() {
    $object = [];
    $id = (int)$_POST['id'];
    $mysqli = connect_db();
    $query = 'SELECT graph_id FROM graph_instance WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      while ($graph_instance = $result->fetch_assoc()) {
        $graph_id = $graph_instance['graph_id'];
        $object[$graph_id] = $this->Data($id, $graph_id);
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Init: '.$mysqli->error);
    }
    $mysqli->close();
    return $object;
  }

  private function ModuleConnections($feed_id) {
    $connections = '';
    $mysqli = connect_db();
    $query = 'SELECT label FROM graph_connect WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
    if ($result = $mysqli->query($query)) {
      while ($graph_connect = $result->fetch_assoc()) {
        $connections .= '<option value="'.$graph_connect['label'].'">'.
          $graph_connect['label'].'</option>';
      }
      $result->close();
    }
    else {
      $this->Log('Graph->ModuleConnections: '.$mysqli->error);
    }
    $mysqli->close();
    return $connections;
  }

  private function Labels($feed_id) {
    $labels = '';
    $mysqli = connect_db();
    $query = 'SELECT '.$this->LabelQuery().' FROM graph_labels WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_labels = $result->fetch_row()) {
        for ($i = 0; $i < 20; $i++) {
          $option = $graph_labels[$i];
          // 'time' isn't selectable except as a graph type.
          if ($option !== '' && $option !== 'time') {
            $labels .= '<option value="'.$option.'">'.$option.'</option>';
          }
        }
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Labels: '.$mysqli->error);
    }
    $mysqli->close();
    return $labels;
  }

  private function RemoveLabel() {
    $object = [];
    $id = (int)$_POST['id'];
    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $feed_id = $this->RequestFeedId($id);
    $query = 'SELECT '.$this->LabelQuery().' FROM graph_labels WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_labels = $result->fetch_assoc()) {
        $result->close();
        if ($column = $this->Key($graph_labels, $label)) {
          $query = 'UPDATE graph_labels SET '.$column.' = "" WHERE '.
            'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
          if (!$mysqli->query($query)) {
            $this->Log('Graph->RemoveLabel 1: '.$mysqli->error);
          }
          $object['done'] = true;
        }
        else {
          $object['error'] = 'Label does not exist';
        }
      }
      else {
        $result->close();
      }
    }
    else {
      $this->Log('Graph->RemoveLabel 2: '.$mysqli->error);
    }
    $mysqli->close();
    return $object;
  }

  private function NewLabel() {
    $object = [];
    $mysqli = connect_db();
    $id = (int)$_POST['id'];
    $label = $mysqli->escape_string($_POST['label']);
    $feed_id = $this->RequestFeedId($id);
    $query = 'SELECT '.$this->LabelQuery().' FROM graph_labels WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_labels = $result->fetch_assoc()) {
        $result->close();
        // Check if the label already exists.
        if ($column = $this->Key($graph_labels, $label)) {
          $object['error'] = 'Label already exists';
        }
        else {
          // The label doesn't exist, but check if there is a column available.
          if ($available = $this->Key($graph_labels, '')) {
            $query = 'UPDATE graph_labels SET '.$available.' = "'.$label.'" '.
              'WHERE user = "'.$this->owner.'" AND feed_id = '.$feed_id;
            if (!$mysqli->query($query)) {
              $this->Log('Graph->NewLabel 1: '.$mysqli->error);
            }
            $object['data'] = $this->Labels($feed_id);
          }
          else {
            $object['error'] = 'Labels full: Please remove an existing label';
          }
        }
      }
      else {
        $result->close();
      }
    }
    else {
      $this->Log('Graph->NewLabel 2: '.$mysqli->error);
    }
    $mysqli->close();
    return $object;
  }

  private function NewGraph($type) {
    $graph_id = 0;
    $id = (int)$_POST['id'];
    $mysqli = connect_db();
    $query = 'SELECT MAX(graph_id) AS graph_id FROM graph_instance '.
      'WHERE user = "'.$this->owner.'" AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($graph_instance = $result->fetch_assoc()) {
        $graph_id = (int)$graph_instance['graph_id'] + 1;
      }
      $result->close();
    }
    else {
      $this->Log('Graph->NewGraph 1: '.$mysqli->error);
    }

    $query = 'INSERT INTO graph_instance VALUES ("'.$this->owner.'", '.
      $id.', '.$graph_id.', "'.$type.'", 5, "%e %b %Y", "$%d", "")';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->NewGraph 2: '.$mysqli->error);
    }
    $mysqli->close();
    return ['area' => 'graph-area-'.$graph_id,
            'content' => $this->Display($id, $graph_id)];
  }

  private function RemoveGraph() {
    $id = (int)$_POST['id'];
    $graph_id = (int)$_POST['graphId'];
    $mysqli = connect_db();
    $query = 'DELETE FROM graph_instance WHERE user = "'.$this->owner.'" '.
      'AND box_id = '.$id.' AND graph_id = '.$graph_id;
    if (!$mysqli->query($query)) {
      $this->Log('Graph->RemoveGraph: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function UpdateTitle() {
    $id = (int)$_POST['id'];
    $graph_id = (int)$_POST['graphId'];
    $display_count = (int)$_POST['displayCount'];
    $mysqli = connect_db();
    $title = $mysqli->escape_string(htmlspecialchars($_POST['title']));
    $x_axis_format =
      $mysqli->escape_string(htmlspecialchars($_POST['xAxisFormat']));
    $y_axis_format =
      $mysqli->escape_string(htmlspecialchars($_POST['yAxisFormat']));
    $update_query = 'SET title = "'.$title.'"';
    if ($display_count !== '') {
      $update_query .= ', display_count = '.$display_count;
    }
    if ($x_axis_format !== '') {
      $update_query .= ', x_axis_format = "'.$x_axis_format.'"';
    }
    if ($y_axis_format !== '') {
      $update_query .= ', y_axis_format = "'.$y_axis_format.'"';
    }
    $query = 'UPDATE graph_instance '.$update_query.' WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'graph_id = '.$graph_id;
    if (!$mysqli->query($query)) {
      $this->Log('Graph->UpdateTitle: '.$mysqli->error);
    }
    $mysqli->close();
    return $this->Data($id, $graph_id);
  }

  private function Data($id, $graph_id) {
    $mysqli = connect_db();
    // Get the names of the data pairs that we want to graph.
    $labels = [];
    $pairs = [];
    $query = 'SELECT x_label, y_label FROM graph_pairs WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'graph_id = '.$graph_id;
    if ($result = $mysqli->query($query)) {
      while ($graph_pairs = $result->fetch_assoc()) {
        $x_label = $graph_pairs['x_label'];
        $y_label = $graph_pairs['y_label'];
        if (!in_array($x_label, $labels)) {
          $labels[] = $x_label;
        }
        if (!in_array($y_label, $labels)) {
          $labels[] = $y_label;
        }
        $pairs[] = ['x' => $x_label, 'y' => $y_label];
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Data 1: '.$mysqli->error);
    }

    // Find the column positions for those labels to match in the data table.
    $columns = [];
    // Also create data_labels to reverse match label values.
    $data_labels = [];
    $feed_id = $this->RequestFeedId($id);
    $query = 'SELECT '.$this->LabelQuery().' FROM graph_labels WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_labels = $result->fetch_assoc()) {
        foreach ($graph_labels as $key => $value) {
          if (in_array($value, $labels)) {
            $columns[] = $key;
            $data_labels[$value] = $key;
          }
        }
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Data 2: '.$mysqli->error);
    }

    // Get the settings to display for this graph.
    $graph_type = '';
    $display_count = 0;
    $x_axis_format = '';
    $y_axis_format = '';
    $title = '';
    $query = 'SELECT graph_type, display_count, x_axis_format, y_axis_format, '.
      'title FROM graph_instance WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND graph_id = '.$graph_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_instance = $result->fetch_assoc()) {
        $graph_type = $graph_instance['graph_type'];
        $display_count = $graph_instance['display_count'];
        $x_axis_format = $graph_instance['x_axis_format'];
        $y_axis_format = $graph_instance['y_axis_format'];
        $title = $graph_instance['title'];
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Data 3: '.$mysqli->error);
    }

    $graph = [];
    if (count($columns) === 0) {
      // If nothing to show just return here, however jqPlot
      // does not like to render empty arrays, so set to zero.
      $graph[] = [0 => 0];
      return ['type' => $graph_type, 'x_axis' => $x_axis_format,
              'y_axis' => $y_axis_format, 'title' => $title, 'data' => $graph];
    }

    // Get values from the graph_data table using the label columns and
    // limit to display_count. Also want to exclude rows with null values
    // for this column query.
    $column_query = '';
    for ($i = 0; $i < count($columns); $i++) {
      $column_query .= 'AND '.$columns[$i].' IS NOT NULL ';
    }
    $data = [];
    $query = 'SELECT '.join(',', $columns).' FROM graph_data WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id.' '.$column_query.
      'ORDER BY sample_id DESC LIMIT '.$display_count;
    if ($result = $mysqli->query($query)) {
      foreach ($columns as $key) {
        $data[$key] = [];
      }
      while ($graph_data = $result->fetch_assoc()) {
        foreach ($columns as $key) {
          if (!is_null($graph_data[$key])) {
            $data[$key][] = $graph_data[$key];
          }
        }
      }
      $result->close();
    }
    else {
      $this->Log('Graph->Data 4: '.$mysqli->error);
    }
    $mysqli->close();

    for ($i = 0; $i < count($pairs); $i++) {
      $x_label = $pairs[$i]['x'];
      $y_label = $pairs[$i]['y'];
      $x_column = $data_labels[$x_label];
      $y_column = $data_labels[$y_label];
      $x_data = $data[$x_column];
      $y_data = $data[$y_column];
      $x_length = count($x_data);
      $y_length = count($y_data);
      // If the lengths of the two arrays are not equal, assume that the
      // ends of the array match up. (ie, one set of values started logging
      // later than the other.)
      if ($x_length > $y_length) {
        $offset = $x_length - $y_length;
        $x_data = array_slice($x_data, $offset);
      }
      else if ($y_length > $x_length) {
        $offset = $y_length - $x_length;
        $y_data = array_slice($y_data, $offset);
      }
      if ($combined = array_combine($x_data, $y_data)) {
        $next = [];
        foreach ($combined as $key => $value) {
          // jqPlot requires each point in its own array.
          $next[] = [(float)$key, (float)$value];
        }
        $graph[] = $next;
      }
    }
    // If there is no data make sure something is returned.
    if (count($graph) == 0) $graph[] = [0 => 0];
    return ['type' => $graph_type, 'x_axis' => $x_axis_format,
            'y_axis' => $y_axis_format, 'title' => $title, 'data' => $graph];
  }

  private function AddData($us_data, $module = '') {
    $mysqli = connect_db();
    $feed_id = -1;

    if (isset($_POST['id'])) {
      $feed_id = (int)$_POST['id'];
    }
    // When id isn't set, another module is providing data and expects
    // the graph module to know what to do with it.
    else {
      $query = 'SELECT feed_id FROM graph_connect WHERE label = "'.$module.'" '.
        'AND user = "'.$this->owner.'"';
      if ($result = $mysqli->query($query)) {
        if ($graph_connect = $result->fetch_assoc()) {
          $feed_id = (int)$graph_connect['feed_id'];
        }
        $result->close();
      }
      else {
        $this->Log('Graph->AddData 1: '.$mysqli->error);
      }
    }
    // Return if a label isn't connected to a feed_id, or if one isn't provided.
    if ($feed_id === -1) return false;

    $query = 'SELECT '.$this->LabelQuery().' FROM graph_labels WHERE '.
      'user = "'.$this->owner.'" AND feed_id = '.$feed_id;
    if ($result = $mysqli->query($query)) {
      $column_names = '';
      $values = '';
      // Find the generic table labels for each value posted.
      if ($graph_labels = $result->fetch_assoc()) {
        foreach ($us_data as $us_key => $us_value) {
          if ($us_column = $this->Key($graph_labels, $us_key)) {
            if ($column_names !== '') {
              $column_names .= ',';
            }
            $column_names .= $mysqli->escape_string($us_column);
            if ($values !== '') {
              $values .= ',';
            }
            $values .= '"'.$mysqli->escape_string($us_value).'"';
          }
        }
      }
      $result->close();
      if ($column_names !== '' && $values !== '') {
        $query = 'INSERT INTO graph_data (user, feed_id, '.$column_names.') '.
          'VALUES ("'.$this->owner.'", '.$feed_id.', '.$values.')';
        if (!$mysqli->query($query)) {
          $this->Log('Graph->AddData 2: '.$mysqli->error);
        }
      }
    }
    else {
      $this->Log('Graph->AddData 3: '.$mysqli->error);
    }
    $mysqli->close();
  }

  private function RemoveSeries() {
    $id = (int)$_POST['id'];
    $graph_id = (int)$_POST['graphId'];
    $mysqli = connect_db();
    $x_label = 'time';

    if ($graph_id === 0) {
      $mysqli->close();
      return ['error' => 'graphId not given'];
    }

    // Check the graph_type so that the correct pair is removed.
    $query = 'SELECT graph_type FROM graph_instance WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'graph_id = '.$graph_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_instance = $result->fetch_assoc()) {
        if ($graph_instance['graph_type'] === 'custom') {
          $x_label = $mysqli->escape_string(htmlspecialchars($_POST['xLabel']));
        }
      }
      $result->close();
    }
    else {
      $this->Log('Graph->RemoveSeries 1: '.$mysqli->error);
    }

    $y_label = $mysqli->escape_string(htmlspecialchars($_POST['yLabel']));
    $query = 'DELETE FROM graph_pairs WHERE user = "'.$this->owner.'"'.
      ' AND box_id = '.$id.' AND graph_id = '.$graph_id.
      ' AND x_label = "'.$x_label.'" AND y_label = "'.$y_label.'" LIMIT 1';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->RemoveSeries 2: '.$mysqli->error);
    }
    $mysqli->close();
    return $this->Data($id, $graph_id);
  }

  private function AddSeries() {
    $id = (int)$_POST['id'];
    $graph_id = (int)$_POST['graphId'];
    $mysqli = connect_db();
    $x_label = 'time';
    if ($graph_id === 0) {
      $mysqli->close();
      return ['error' => 'graphId not given'];
    }

    $query = 'SELECT graph_type FROM graph_instance WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'graph_id = '.$graph_id;
    if ($result = $mysqli->query($query)) {
      if ($graph_instance = $result->fetch_assoc()) {
        if ($graph_instance['graph_type'] === 'custom') {
          $x_label = $mysqli->escape_string($_POST['xLabel']);
        }
      }
      $result->close();
    }
    else {
      $this->Log('Graph->AddSeries 1: '.$mysqli->error);
    }

    $y_label = $mysqli->escape_string(htmlspecialchars($_POST['yLabel']));
    $query = 'INSERT INTO graph_pairs VALUES ("'.$this->owner.'", '.$id.', '.
      $graph_id.', "'.$x_label.'", "'.$y_label.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Graph->AddSeries 2: '.$mysqli->error);
    }
    $mysqli->close();
    return $this->Data($id, $graph_id);
  }

  private function ConnectModule() {
    $feed_id = (int)$_POST['feedID'];
    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $query = 'INSERT INTO graph_connect VALUES ("'.$this->owner.'", '.
      '"'.$label.'", '.$feed_id.') ON DUPLICATE KEY UPDATE feed_id = '.$feed_id;
    if (!$mysqli->query($query)) {
      $this->Log('Graph->ConnectModule: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function DisconnectModule() {
    $feed_id = (int)$_POST['feedID'];
    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $query = 'DELETE FROM graph_connect WHERE user = "'.$this->owner.'" '.
      'AND label = "'.$label.'" AND feed_id = '.$feed_id;
    if (!$mysqli->query($query)) {
      $this->Log('Graph->DisonnectModule: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function Key($array, $value) {
    $keys = array_keys($array, $value);
    if (count($keys) === 0) return false;
    return $keys[0];
  }

  private function LabelQuery($join = ', ', $append = false) {
    $labels = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight',
               'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
               'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
               'twenty'];
    return $append ? join($join, $labels).$join : join($join, $labels);
  }

}
