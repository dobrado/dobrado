/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
//
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
//
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.subdomaincheck) {
  dobrado.subdomaincheck = {};
}
(function() {

  'use strict';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.subdomaincheck').length === 0) {
      return;
    }

    $('#subdomaincheck-button').button().click(check);
    $('#subdomaincheck-email-submit').button().click(submit);
  });

  function check() {
    var subdomainInput = $('#subdomaincheck-input').val();
    if (subdomainInput === '') {
      return false;
    }

    $('#subdomaincheck-info').html('checking...');
    $.post('/php/request.php',
           { request: 'subdomaincheck', action: 'lookup',
             subdomain: subdomainInput, url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'check')) {
          return;
        }
        let check = JSON.parse(response);
        $('#subdomaincheck-info').html(check.info);
        $('#subdomaincheck-email-form').hide();
        if (check.email) {
          $('#subdomaincheck-email-form').show();
        }
      });
    return false;
  }

  function submit() {
    var email = $('#subdomaincheck-email').val();
    if (email === '') {
      return false;
    }

    $.post('/php/request.php',
           { request: 'subdomaincheck', action: 'submit', email: email,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'submit')) {
          return;
        }
        let check = JSON.parse(response);
        $('#subdomaincheck-email-info').html(check.info);
      });
    return false;
  }

}());
