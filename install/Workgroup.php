<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Workgroup extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $detail = new Detail($this->user, $this->owner);
    $all_users = $detail->AllUsers();
    $roster = new Roster($this->user, $this->owner);
    $all_roles = $roster->AllRoles();

    $content = $this->Substitute('workgroup-start-text');
    foreach ($all_roles as $role => $details) {
      $content .= '<div class="workgroup-role"><h4>' . $role . '</h4>' .
        '<div class="workgroup-description">' . $details['description'] .
        '</div>';
      for ($i = 0; $i < count($details['members']); $i++) {
        $user = $details['members'][$i];
        if ($all_users[$user]['active'] && $all_users[$user]['display']) {
          $content .= '<div class="workgroup-profile">' .
              $all_users[$user]['thumbnail'] .
              '<div class="workgroup-fullname">' .
                $all_users[$user]['first'] . ' ' . $all_users[$user]['last'] .
              '</div>' .
              '<div class="workgroup-username">Username: ' . $user . '</div>' .
              '<div class="workgroup-phone">Phone: ' .
                $all_users[$user]['phone'] . '</div>' .
              '<div class="workgroup-phone">email: ' .
                $all_users[$user]['email'] . '</div>' .
              '<div class="workgroup-description">' .
                $all_users[$user]['description'] . '</div>' .
            '</div>';
          $active[] = $user;
        }
      }
      $content .= '<div class="clear"></div></div>';
    }
    // Add all users not found in roles to the end.
    $content .= '<div class="workgroup-non-active">' .
      $this->Substitute('workgroup-non-active-text') . '</div>';
    foreach ($all_users as $user => $details) {
      if ($details['active'] &&
          $details['display'] && !in_array($user, $active)) {
        $content .= '<div class="workgroup-profile">' .
            $all_users[$user]['thumbnail'] .
            '<div class="workgroup-fullname">' .
              $all_users[$user]['first'] . ' ' . $all_users[$user]['last'] .
            '</div>' .
            '<div class="workgroup-username">Username: ' . $user . '</div>' .
            '<div class="workgroup-phone">Phone: ' .
              $all_users[$user]['phone'] . '</div>' .
            '<div class="workgroup-phone">email: ' .
              $all_users[$user]['email'] . '</div>' .
            '<div class="workgroup-description">' .
              nl2br($all_users[$user]['description'], false) . '</div>' .
          '</div>';
      }
    }
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $site_style = ['"",".workgroup-profile .thumb","float","left"',
                   '"",".workgroup-profile .thumb","padding","5px"',
                   '"",".workgroup-profile","clear","both"',
                   '"",".workgroup-fullname","font-size","1.4em"',
                   '"",".workgroup-role","clear","both"',
                   '"",".workgroup-role","padding","10px"',
                   '"",".workgroup-role","margin","10px"',
                   '"",".workgroup-role","background-color","#eeeeee"',
                   '"",".workgroup-role","border","1px solid #aaaaaa"',
                   '"",".workgroup-role","border-radius","2px"',
                   '"",".workgroup-role .clear","clear","both"',
                   '"",".workgroup-description","margin","10px"',
                   '"",".workgroup-non-active","clear","both"',
                   '"",".workgroup-non-active","margin","10px"'];
    $this->AddSiteStyle($site_style);

    $template = ['"workgroup-start-text", "", "Members are listed below ' .
                   'in their working groups or based on recent roster ' .
                   'information.<br>To get involved in a working group, ' .
                   'please see the contact information for members in ' .
                   'each group."',
                 '"workgroup-non-active-text", "", "If your name is ' .
                   'listed below, we would love you to get involved in ' .
                   'running the co-op op. You can start by adding your ' .
                   'name to the roster."'];
    $this->AddTemplate($template);
    $description = ['workgroup-start-text' => 'A description to display at ' .
                      'the start of the Workgroup module content.',
                    'workgroup-non-active-text' => 'A description to display ' .
                      'at the start of the non-active user group.'];
    $this->AddTemplateDescription($description);

    // Check module dependenices.
    return $this->Dependencies(['detail', 'roster']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript.
  }

}
