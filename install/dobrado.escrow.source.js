/*global dobrado: true, CKEDITOR: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.escrow) {
  dobrado.escrow = {};
}
(function() {

  'use strict';

  // Store the recipient's details after address lookup.
  var details = null;
  // Store the reply url after checking so that it can be used by submit.
  var reply = '';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.escrow').length === 0) {
      return;
    }

    $('#escrow-start').button().click(start);
    // Also allow pressing enter check the address.
    $('#escrow-web-address').keypress(function(event) {
      if (event.which === 13 ) {
        event.preventDefault();
        start();
      }
    });
    $('#escrow-payment-description').val('').click(function() {
      // Extended editor isn't modal so don't allow editor changes when open.
      if (!$('.extended').dialog('isOpen')) {
        dobrado.escrow.showEditor();
      }
    });

    $('#escrow-payment-submit').button().click(submit);
    if ($('.escrow-reply-context').html() !== '') {
      checkReply();
    }
  });

  function checkReply() {
    reply = $('.escrow-reply-context').html();
    if (reply === '') return;

    dobrado.log('Checking reply...', 'info');
    $.post('/php/request.php',
           { request: 'escrow', action: 'reply', reply: reply,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'escrow reply')) {
          return;
        }
        var recipient = JSON.parse(response);
        if (recipient.fail) {
          $('.escrow-start-info').append(recipient.fail);
          $('.escrow-reply-context').html('').hide();
          reply = '';
        }
        else {
          details = recipient.details;
          $('.escrow-start-info').html('');
          $('.escrow-recipient-info').html(recipient.success);
          $('.escrow-reply-context').
            prepend('<b>This payment is in reply to:</b> ').show();
          $('#escrow-form').show();
          $('label[for=escrow-payment-from]').html('Accept Payment From');
          if (recipient.payment === 'debit') {
            $('#escrow-payment-to').prop('checked', true);
          }
          else {
            $('#escrow-payment-from').prop('checked', true);
          }
          $('#escrow-payment-amount').val(recipient.amount);
          $('.escrow-transaction').html(recipient.history);
          if ($(dobrado.current).data('label') !== 'escrow') {
            dobrado.escrow.showEditor();
          }
        }
      });
  }

  function start() {
    var address = $('#escrow-web-address').val();
    if (address === '') return false;

    $('.escrow-start-info').html('');
    $('.escrow-submit-info').html('');
    $('.escrow-transaction').html('');
    dobrado.log('Checking web address...', 'info');
    $.post('/php/request.php',
           { request: 'escrow', action: 'start', address: address,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'escrow start')) {
          return;
        }
        $('#escrow-web-address').val('');
        var recipient = JSON.parse(response);
        if (recipient.success) {
          details = recipient.details;
          $('.escrow-recipient-info').html(recipient.success);
          $('#escrow-form').show();
          $('.escrow-transaction').html(recipient.history);
          if ($(dobrado.current).data('label') !== 'escrow') {
            dobrado.escrow.showEditor();
          }
        }
        else if (recipient.fail) {
          $('.escrow-recipient-info').html('');
          $('#escrow-form').hide();
          $('.escrow-start-info').html(recipient.fail);
        }
      });
  }

  function submit() {
    if (!details || details.url === '') {
      $('.escrow-submit-info').html('Recipient\'s details not found.');
      return false;
    }

    dobrado.log('Creating payment record...', 'info');
    var description = '';
    if (dobrado.editor) {
      description = dobrado.editor.getData();
    }
    else {
      description = $('#escrow-payment-description').val();
    }
    $.post('/php/request.php',
           { request: 'escrow', action: 'submit',
             recipient: details.url, name: details.name,
             type: $('input[name=escrow-payment-type]:checked').val(),
             amount: $('#escrow-payment-amount').val(), reply: reply,
             description: description, url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'escrow submit')) {
          return;
        }
        var post = JSON.parse(response);
        if (post.success) {
          // This request is used to check micropub-status.
          $.post('/php/request.php',
                 { request: 'post', action: 'updated',
                   url: location.href, token: dobrado.token },
            function(response) {
              if (dobrado.checkResponseError(response, 'escrow updated')) {
                return;
              }
              var updated = JSON.parse(response);
              $('.escrow-submit-info').html(updated.status);
            });
        }
        else if (post.fail) {
          $('.escrow-submit-info').html(post.fail);
          $('#escrow-post').val(post.content);
          $('.escrow-no-micropub').show();
        }
        // Reset the details in the payment form.
        reply = '';
        $('.escrow-reply-context').html('').hide();
        $('label[for=escrow-payment-from]').html('Request Payment From');
        $('#escrow-payment-amount').val('');
        $('.escrow-transaction').html('');
        if (dobrado.editor) {
          dobrado.editor.setData('');
        }
        else {
          $('#escrow-payment-description').val('');
        }
      });
    return false;
  }

  dobrado.escrow.showEditor = function() {
    if (dobrado.editMode) {
      dobrado.closeEditor();
    }
    $('#escrow-payment-description').parents('div').each(function() {
      if (/^dobrado-/.test($(this).attr('id'))) {
        dobrado.current = '#' + $(this).attr('id');
        return false;
      }
    });
    $(dobrado.current).data('label', 'escrow');
    CKEDITOR.addCss('.cke_editable { margin: 10px; }');
    dobrado.editor = CKEDITOR.replace('escrow-payment-description',
      { allowedContent: true,
        autoGrow_minHeight: $('#escrow-payment-description').height(),
        autoGrow_onStartup: true,
        disableNativeSpellChecker: false,
        enterMode: CKEDITOR.ENTER_BR,
        extraPlugins: 'extended',
        filebrowserBrowseUrl: '/php/browse.php',
        removePlugins:
          'elementspath,tableselection,tabletools,contextmenu,liststyle',
        resize_enabled: false,
        toolbar: [[ 'Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'Link',
                    'Unlink', '-', 'EmojiPanel', 'Image', 'Extended' ]]
      });
  };

}());
