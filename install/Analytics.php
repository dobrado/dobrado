<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Analytics extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    $mysqli = connect_db();
    $referer = '';
    if (isset($_SERVER['HTTP_REFERER'])) {
      $us_referer = $_SERVER['HTTP_REFERER'];
      $referer = $mysqli->escape_string(substr($us_referer, 0, 300));
    }
    // The current time is quantized to a timestamp on the hour.
    $timestamp = strtotime(date('F j Y H:00:00') . ' UTC');
    $query = 'INSERT INTO analytics VALUES ("' . $this->owner . '", ' .
      '"' . $this->user->page . '", ' . $timestamp . ', "' . $referer . '", ' .
      '1) ON DUPLICATE KEY UPDATE counter = counter + 1';
    if (!$mysqli->query($query)) {
      $this->Log('Analytics->Content: ' . $mysqli->error);
    }
    $mysqli->close();
    return false;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS analytics (' .
      'user VARCHAR(50) NOT NULL,' .
      'page VARCHAR(200) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'referer VARCHAR(300) NOT NULL,' .
      'counter INT UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, page(50), timestamp, referer(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Analytics->Install 1: ' . $mysqli->error);
    }
    // Install Analytics for all existing users. (New users will copy the
    // module from the admin user.)
    $query = 'INSERT INTO modules (user, page, label, class, box_order, ' .
      'placement, deleted) SELECT user, "", "analytics", "", 0, "footer", 0 ' .
      'FROM users';
    if (!$mysqli->query($query)) {
      $this->Log('Analytics->Install 2: ' . $mysqli->error);
    }
    $query = 'INSERT INTO modules_history (user, page, label, class, ' .
      'box_order, placement, action, modified_by, timestamp) SELECT user, ' .
      '"", "analytics", "", 0, "footer", "add", "admin", ' . time() .
      ' FROM users';
    if (!$mysqli->query($query)) {
      $this->Log('Analytics->Install 3: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Placement() {
    return 'footer';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

  // Public functions that aren't part of interface here /////////////////////

  public function Referers($start, $end) {
    $data = [];
    $mysqli = connect_db();
    $query = 'SELECT user, page, referer, SUM(counter) AS counter FROM ' .
      'analytics WHERE referer != "" AND timestamp >= ' . $start . ' AND ' .
      'timestamp <= ' . $end . ' GROUP BY user, page, referer ORDER BY ' .
      'counter DESC';
    if ($result = $mysqli->query($query)) {
      while ($analytics = $result->fetch_assoc()) {
        $data[] = ['user' => $analytics['user'],
                   'page' => $analytics['page'],
                   'referer' => $analytics['referer'],
                   'counter' => (int)$analytics['counter']];
      }
      $result->close();
    }
    else {
      $this->Log('Analytics->Referers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $data;
  }

  public function Total($start, $end) {
    $total = 0;
    $mysqli = connect_db();
    $query = 'SELECT SUM(counter) AS counter FROM analytics WHERE ' .
      'timestamp >= ' . $start . ' AND timestamp <= ' . $end;
    if ($result = $mysqli->query($query)) {
      if ($analytics = $result->fetch_assoc()) {
        $total = (int)$analytics['counter'];
      }
      $result->close();
    }
    else {
      $this->Log('Analytics->Total: ' . $mysqli->error);
    }
    $mysqli->close();
    return $total;
  }

  // Private functions below here ////////////////////////////////////////////

}
