<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Payment extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view payments'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'list') {
      return $this->AllData();
    }
    if ($us_action === 'search') {
      $mysqli = connect_db();
      $username = $mysqli->escape_string($_POST['username']);
      $mysqli->close();
      $timestamp = (int)$_POST['timestamp'] / 1000;
      $start = (int)$_POST['start'] / 1000;
      $end = (int)$_POST['end'] / 1000;
      $group = (bool)$_POST['group'];
      $exportData = (bool)$_POST['exportData'];
      return $this->Search($username, $timestamp, $start, $end, $group,
                           $exportData);
    }
    if ($us_action === 'details') {
      $banking = new Banking($this->user, $this->owner);
      $detail = new Detail($this->user, $this->owner);
      return ['bank' => $banking->AllSettings(),
              'contact' => $detail->AllUsers(true)];
    }
    if ($us_action === 'edit') {
      return $this->EditPayment();
    }
    if ($us_action === 'remove') {
      return $this->RemovePayment();
    }
    if ($us_action === 'editBanking') {
      return $this->EditBanking();
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    // Need admin privileges to add the payment module.
    if (!$this->user->canEditSite) return false;
    // Can only have one payment module on a page.
    return !$this->AlreadyOnPage('payment', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<form id="payment-form" autocomplete="off">' .
        '<button class="default-action hidden">default</button>' .
        '<div class="import">Use the form below to enter individual ' .
          'transactions, or <a href="" id="payment-toggle-import">' .
            'click here to import bank records.</a>' .
          '<div class="import-wrapper hidden">' .
            '<div class="form-spacing">' .
              '<label for="payment-import-input">Import File:</label>' .
              '<input id="payment-import-input" type="file">' .
            '</div>' .
            '<div class="info"></div>' .
          '</div>' .
        '</div><hr>' .
        '<div class="form-spacing">' .
          '<label for="payment-username-input">Username:</label>' .
          '<input id="payment-username-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-fullname-input">Full Name:</label>' .
          '<input id="payment-fullname-input" type="text" maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-date-input">Date:</label>' .
          '<input id="payment-date-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-reference-input">Reference:</label>' .
          '<input id="payment-reference-input" type="text" maxlength="20">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-comment">Comment:</label>' .
          '<textarea id="payment-comment"></textarea>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-amount-input">Amount:</label>' .
          '<input id="payment-amount-input" type="text" maxlength="50">' .
        '</div>' .
        '<hr>' .
        '<a class="toggle-search-options" href="#">' .
          'Search between start and end dates</a><br>' .
        '<div class="search-options hidden">' .
          '<div class="form-spacing">' .
            '<label for="payment-start-date-input">Start:</label>' .
            '<input id="payment-start-date-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="payment-end-date-input">End:</label>' .
            '<input id="payment-end-date-input" type="text" maxlength="50">' .
          '</div>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="payment-group-input" type="checkbox"> ' .
          'Group results by date' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="payment-export-data" type="checkbox"> ' .
          'Download search results' .
        '</div>' .
        '<div class="bank-details"></div>' .
        '<div class="contact-details"></div>' .
        '<button class="submit">submit</button>' .
        '<button class="back hidden">go back</button>' .
        '<button class="remove">remove</button>' .
        '<button class="search">search</button>' .
      '</form>' .
      '<form id="payment-details-form" class="hidden" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="payment-details-reference-input">Reference:</label>' .
          '<input id="payment-details-reference-input" type="text" ' .
            'maxlength="20">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-name-input">Account Name:</label>' .
          '<input id="payment-details-name-input" type="text" ' .
            'maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-number-input">Account Number:</label>' .
          '<input id="payment-details-number-input" type="text" ' .
            'maxlength="20">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-bsb-input">BSB:</label>' .
          '<input id="payment-details-bsb-input" type="text" maxlength="7">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-credit-input">Receive Credit:</label>' .
          '<input id="payment-details-credit-input" type="checkbox">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-surcharge-input">Add Surcharge:' .
          '</label>' .
          '<input id="payment-details-surcharge-input" type="checkbox">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-deposit-input">Paid Deposit:</label>' .
          '<input id="payment-details-deposit-input" type="checkbox">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="payment-details-buyer-group-select">' .
            'Buyer Group:</label>' .
          '<select id="payment-details-buyer-group-select">' .
            '<option value="">base rate</option>' .
            '<option value="wholesale">wholesale</option>' .
            '<option value="retail">retail</option>' .
          '</select>' .
        '</div>' .
        '<div id="payment-details-info"></div>' .
        '<button class="submit">submit</button>' .
      '</form>' .
      '<div class="paging hidden">' .
        'Displaying transactions: <span class="start">1</span> - ' .
        '<span class="end">50</span>' .
        '<button class="previous">Previous</button>' .
        '<button class="next">Next</button>' .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.payment.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.payment.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS payments (' .
      'user VARCHAR(50) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'reference VARCHAR(100),' .
      'amount DECIMAL(8,2) NOT NULL,' .
      'comment TEXT,' .
      'volunteer VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS payment_totals (' .
      'user VARCHAR(50) NOT NULL,' .
      'amount DECIMAL(20,10) NOT NULL,' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 2: ' . $mysqli->error);
    }
    // Create triggers to update user's total when the payments table changes.
    // Note that payment_totals.amount is only updated here, it expects that
    // the user has already been inserted into the table. (When their account
    // is created).
    $query = 'CREATE TRIGGER insert_payment_totals AFTER INSERT ON payments ' .
      'FOR EACH ROW UPDATE payment_totals SET amount = amount + NEW.amount ' .
      'WHERE user = NEW.user';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 3: ' . $mysqli->error);
    }
    // Note that the user for an existing payment cannot be modified as this
    // rule would not be triggered.
    $query = 'CREATE TRIGGER update_payment_totals AFTER UPDATE ON payments ' .
      'FOR EACH ROW UPDATE payment_totals SET ' .
      'amount = amount + NEW.amount - OLD.amount WHERE user = NEW.user';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 4: ' . $mysqli->error);
    }
    $query = 'CREATE TRIGGER delete_payment_totals AFTER DELETE ON payments ' .
      'FOR EACH ROW UPDATE payment_totals SET amount = amount - OLD.amount ' .
      'WHERE user = OLD.user';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 5: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS payment_settings (' .
      'system_group VARCHAR(50) NOT NULL,' .
      'surcharge DECIMAL(8,2),' .
      'min_surcharge DECIMAL(8,2),' .
      'max_surcharge DECIMAL(8,2),' .
      'deposit DECIMAL(8,2),' .
      'info DECIMAL(8,2),' .
      'warning DECIMAL(8,2),' .
      'PRIMARY KEY(system_group)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 6: ' . $mysqli->error);
    }
    $query = 'INSERT INTO payment_settings VALUES ("", 0, 0, 0, 0, 50, 100)';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Install 7: ' . $mysqli->error);
    }
    $mysqli->close();

    $description = ['payment-surcharge-type' => 'The name of the custom ' .
                      'surcharge type used, options are: ' .
                      '\'sliding-scale-capped\', \'zero-one-two-fifty\'. ' .
                      'If left blank the surcharge value is taken from the ' .
                      'payment settings. See Payment module Surcharge ' .
                      'function for details on custom types.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#payment-form","background-color","#eeeeee"',
                   '"","#payment-form","border","1px solid #aaaaaa"',
                   '"","#payment-form","border-radius","2px"',
                   '"","#payment-form","padding","5px"',
                   '"","#payment-form label","width","6em"',
                   '"","#payment-comment","max-width","98%"',
                   '"","#payment-comment","width","400px"',
                   '"","#payment-comment","height","80px"',
                   '"","#payment-settings-form label","width","8em"',
                   '"","#payment-settings-form input","width","100px"',
                   '"","#payment-settings-form button","margin-left","8.3em"',
                   '"","#payment-settings-form .info","margin-bottom","20px"',
                   '"","#payment-details-form label","width","8em"',
                   '"","#payment-details-form .submit","margin-left","8.3em"',
                   '"",".payment .edit","text-align","right"',
                   '"",".payment a","color","#222222"',
                   '"",".payment a.toggle-search-options","font-size","0.8em"',
                   '"",".payment a","text-decoration","none"',
                   '"",".payment a:hover","color","#aaaaaa"',
                   '"",".payment .submit","float","right"',
                   '"",".payment .search","float","right"',
                   '"",".payment .search","margin-right","10px"',
                   '"",".payment .paging","margin-left","20px"',
                   '"",".payment .start","font-weight","bold"',
                   '"",".payment .end","font-weight","bold"',
                   '"",".payment .previous","margin-left","20px"',
                   '"",".payment .next","margin-left","20px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    // Return if Remove was called for a specific module,
    // only want to remove payments when deleting an account.
    if (isset($id)) return;

    // Note that deleting accounts with existing payments must be done
    // very carefully because it can effect record keeping.
    $mysqli = connect_db();
    $query = 'DELETE FROM payments WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Remove 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM payment_totals WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Remove 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.payment.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function AllSettings($group = '') {
    if ($group === '') $group = $this->user->group;
    $settings = ['surcharge' => '', 'min_surcharge' => '',
                 'max_surcharge' => '', 'deposit' => '', 'info' => '',
                 'warning' => ''];
    $mysqli = connect_db();
    $query = 'SELECT surcharge, min_surcharge, max_surcharge, deposit, ' .
      'info, warning FROM payment_settings WHERE ' .
      'system_group = "' . $group . '" OR system_group = "" ' .
      'ORDER BY system_group DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payment_settings = $mysqli_result->fetch_assoc()) {
        $settings = $payment_settings;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->AllSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $settings;
  }

  public function AllTotals($organisation = false) {
    $totals = [];
    $mysqli = connect_db();

    $query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT payment_totals.user, amount FROM payment_totals ' .
        'LEFT JOIN users ON payment_totals.user = users.user WHERE ' .
        'users.user NOT LIKE "buyer\_%" AND ' . $organiser->GroupQuery();
    }
    else {
      $query = 'SELECT payment_totals.user, amount FROM payment_totals ' .
        'LEFT JOIN users ON payment_totals.user = users.user WHERE ' .
        'users.user NOT LIKE "buyer\_%" AND ' .
        'users.system_group = "' . $this->user->group . '"';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($payment_totals = $mysqli_result->fetch_assoc()) {
        $totals[$payment_totals['user']] = (float)$payment_totals['amount'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->AllTotals: ' . $mysqli->error);
    }
    $mysqli->close();
    return $totals;
  }

  public function Data($start, $end, $user = '', $incoming_only = false) {
    $data = [];
    if ($user === '') $user = $this->user->name;
    $mysqli = connect_db();
    $query = 'SELECT timestamp, amount, comment FROM payments WHERE ' .
      'timestamp >= ' . $start . ' AND timestamp <= ' . $end . ' AND ' .
      'user = "' . $user . '" ORDER BY timestamp';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($payments = $mysqli_result->fetch_assoc()) {
        $amount = (float)$payments['amount'];
        if ($amount < 0 && $incoming_only) continue;

        $amount = number_format($amount, 2, '.', '');
        $timestamp = (int)$payments['timestamp'];
        // This flag defaults to false for Javascript, which uses milliseconds.
        if (!$incoming_only) $timestamp *= 1000;
        $data[] = ['date' => $timestamp, 'comment' => $payments['comment'],
                   'amount' => $amount];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->Data: ' . $mysqli->error);
    }
    $mysqli->close();
    return $data;
  }

  public function LevelSettings() {
    $info = '';
    $warning = '';
    $mysqli = connect_db();
    $query = 'SELECT info, warning FROM payment_settings WHERE ' .
      'system_group = "' . $this->user->group . '" OR system_group = "" ' .
      'ORDER BY system_group DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payment_settings = $mysqli_result->fetch_assoc()) {
        $info = $payment_settings['info'];
        $warning = $payment_settings['warning'];
        // Set some sensible defaults if payment_settings are empty.
        if ($info === '') $info = '50';
        if ($warning === '') $warning = '100';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->LevelSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    return [(float)$info, (float)$warning];
  }

  public function ListRecent($user) {
    if ($this->Substitute('payment-list-recent') !== 'true') return '';

    $start = strtotime($this->Substitute('payment-list-start'));
    if (!$start) return '';

    $data = $this->Data($start, time(), $user, true);
    if (count($data) === 0) {
      return '<p>No payment received in the past week.</p>';
    }

    $recent = 'Thanks for your recent payment.<br>';
    foreach ($data as $payment) {
      $recent .= '  <b>$' . $payment['amount'] . '</b> was received on ' .
        date('jS F Y', $payment['date']) . '<br>';
    }
    return '<p>' . $recent . '</p>';
  }

  public function MostRecent() {
    $timestamp = 0;
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    // Want the most recent payment that was manually entered, so ignore
    // automatic deductions that get added when new accounts are created.
    $query = 'SELECT MAX(timestamp) AS timestamp FROM payments LEFT JOIN ' .
      'users ON payments.user = users.user WHERE ' . $organiser->GroupQuery() .
      ' AND comment != "Automatic deposit deduction for new user."';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payments = $mysqli_result->fetch_assoc()) {
        $timestamp = (int)$payments['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->MostRecent: ' . $mysqli->error);
    }
    $mysqli->close();
    return $timestamp;
  }

  public function NewUser($user = '', $deposit = true) {
    if ($user === '') {
      $user = $this->user->name;
    }
    $mysqli = connect_db();
    $query = 'INSERT INTO payment_totals VALUES ("' . $user . '", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->NewUser 1: ' . $mysqli->error);
    }
    if (isset($_SESSION['new-supplier']) && $_SESSION['new-supplier']) {
      $deposit = false;
    }
    if ($deposit) {
      // Add the deposit amount as a negative value for the new user.
      $deposit_amount = 0;
      $query = 'SELECT deposit FROM payment_settings WHERE ' .
        'system_group = "' . $this->user->group . '" OR system_group = "" ' .
        'ORDER BY system_group DESC';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($payment_settings = $mysqli_result->fetch_assoc()) {
          $deposit_amount = (float)$payment_settings['deposit'] * -1;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Payment->NewUser 2: ' . $mysqli->error);
      }
      if ($deposit_amount > 0.001 || $deposit_amount < -0.001) {
        $query = 'INSERT INTO payments VALUES (' .
          '"' . $this->user->name . ' ", ' . time() . ', "", ' .
          $deposit_amount . ', "Automatic deposit deduction for new user.", ' .
          '"")';
        if (!$mysqli->query($query)) {
          $this->Log('Payment->NewUser 3: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();
  }

  public function Save($user, $time, $total, $method = 'cash') {
    $mysqli = connect_db();
    $total = number_format($total, 2, '.', '');
    $query = 'INSERT INTO payments VALUES ("' . $user. '", ' . $time . ', ' .
      '"", ' . $total . ', "' . $method . '", "' . $this->user->name . '") ' .
      'ON DUPLICATE KEY UPDATE amount = ' . $total . ', ' .
      'comment = "' . $method . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Payment->Save: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Search($username, $timestamp = 0, $start = 0,
                         $end = 0, $group = true, $exportData = false) {
    $mysqli = connect_db();
    $data = ['search' => []];
    $search = '';
    // Need to specify table for user query due to join.
    if ($username !== '') {
      $search .= 'payments.user = "' . $username . '"';
    }
    if ($timestamp !== 0) {
      if ($search !== '') {
        $search .= ' AND ';
      }
      $search .= 'timestamp >= ' .
        strtotime(date('F j Y 00:00:00', (int)$timestamp)) . ' AND ' .
        'timestamp <= ' . strtotime(date('F j Y 23:59:59', (int)$timestamp));
    }
    if ($start !== 0) {
      if ($search !== '') {
        $search .= ' AND ';
      }
      $search .= 'timestamp >= ' .
        strtotime(date('F j Y 00:00:00', (int)$start));
    }
    if ($end !== 0) {
      if ($search !== '') {
        $search .= ' AND ';
      }
      $search .= 'timestamp <= ' . strtotime(date('F j Y 23:59:59', (int)$end));
    }
    if ($search === '') {
      return $data;
    }

    $day_total = 0;
    $prev_day = -1;
    $prev_user = '';
    $prev_timestamp = 0;
    $organiser = new Organiser($this->user, $this->owner);
    $query = 'SELECT payments.user, timestamp, reference, amount, comment ' .
      'FROM payments LEFT JOIN users ON payments.user = users.user WHERE ' .
      $organiser->GroupQuery() . ' AND ' . $search . ' ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($payments = $mysqli_result->fetch_assoc()) {
        $amount = (float)$payments['amount'];
        $timestamp = (int)$payments['timestamp'] * 1000;
        if ($group) {
          // Note that username can be included in group results if provided,
          // but will be left blank if not. Also allow negative totals.
          $day = (int)date('z', (int)$payments['timestamp']);
          if ($day !== $prev_day &&
              ($day_total > 0.001 || $day_total < -0.001)) {
            $day_total = number_format($day_total, 2, '.', '');
            $data['search'][] = ['date' => $prev_timestamp, 'name' => $username,
                                 'reference' => '', 'comment' => '',
                                 'amount' => $day_total];
            $day_total = 0;
          }
          $prev_day = $day;
          $prev_user = $payments['user'];
          $prev_timestamp = $timestamp;
          $day_total += $amount;
        }
        else {
          $data['search'][] = ['date' => $timestamp,
                               'name' => $payments['user'],
                               'reference' => $payments['reference'],
                               'comment' => $payments['comment'],
                               'amount' => number_format($amount, 2, '.', '')];
        }
      }
      if ($group && ($day_total > 0.001 || $day_total < -0.001)) {
        $day_total = number_format($day_total, 2, '.', '');
        $data['search'][] = ['date' => $prev_timestamp, 'name' => $username,
                             'reference' => '', 'comment' => '',
                             'amount' => $day_total];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->Search: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($exportData && count($data['search']) > 0) {
      $date = date('Y-m-d');
      $filename = 'payment-' . $date . '.csv';
      $this->CreateCSV($filename, $data['search']);
      $data['filename'] = $filename;
    }
    return $data;
  }

  public function SearchTotal($start, $end, $organisation, $type = '') {
    $total = 0;
    $group_query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $group_query = $organiser->GroupQuery();
    }
    else {
      $group_query = 'users.system_group = "' . $this->user->group . '"';
    }
    $received_query = '';
    if ($type === 'received') {
      $received_query = ' AND amount > 0';
    }
    else if ($type === 'outgoing') {
      $received_query = ' AND amount < 0';
    }

    $mysqli = connect_db();
    $query = 'SELECT SUM(amount) AS total FROM payments LEFT JOIN users ' .
      'ON payments.user = users.user WHERE ' . $group_query . $received_query .
      ' AND timestamp >= ' . $start . ' AND timestamp <= ' . $end;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payments = $mysqli_result->fetch_assoc()) {
        $total = (float)$payments['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->SearchTotal: ' . $mysqli->error);
    }
    $mysqli->close();
    return $total;
  }

  public function Surcharge($purchases) {
    if ($purchases > -0.01 && $purchases < 0.01) return 0;

    // First check for custom surcharge types.
    $surcharge_type = $this->Substitute('payment-surcharge-type');
    if ($surcharge_type === 'sliding-scale-capped') {
      if ($purchases < 5) return 1;
      if ($purchases < 10) return $purchases * 0.2;
      if ($purchases < 30) return (($purchases - 10) * 0.1) + 2;
      return 4;
    }
    else if ($surcharge_type === 'zero-one-two-fifty') {
      if ($purchases < 5) return 0;
      if ($purchases < 15) return 1.00;
      return 2.50;
    }

    $surcharge = 0;
    $min_surcharge = 0;
    $max_surcharge = 0;
    $mysqli = connect_db();
    $query = 'SELECT surcharge, min_surcharge, max_surcharge FROM ' .
      'payment_settings WHERE system_group = "' . $this->user->group . '" OR ' .
      'system_group = "" ORDER BY system_group DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payment_settings = $mysqli_result->fetch_assoc()) {
        $surcharge = (float)$payment_settings['surcharge'];
        $min_surcharge = (float)$payment_settings['min_surcharge'];
        $max_surcharge = (float)$payment_settings['max_surcharge'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->Surcharge: ' . $mysqli->error);
    }
    $mysqli->close();
    
    // If the surcharge is greater than or equal to 0.4 then it's a flat rate.
    if ($surcharge >= 0.4) return $surcharge;
    // Otherwise values between 0 and 0.4 are interpreted as a percentage.
    if ($surcharge > 0) {
      if ($min_surcharge > 0.01 && $purchases * $surcharge < $min_surcharge) {
        return $min_surcharge;
      }
      if ($max_surcharge > 0.01 && $purchases * $surcharge > $max_surcharge) {
        return $max_surcharge;
      }
      return $purchases * $surcharge;
    }
    return 0;
  }

  public function SurchargeDescription() {
    // First check for custom surcharge types.
    $surcharge_type = $this->Substitute('payment-surcharge-type');
    if ($surcharge_type === 'sliding-scale-capped') {
      return '<p>The surcharge is calculated based on the total:<br>' .
        'If less than $5.00 surcharge is $1.00<br>' .
        'If less than $10.00 surcharge is 20% of purchases<br>' .
        'If less than $30.00 surcharge is $2.00 + 10% of (purchases - $10.00)' .
        '<br>For total over $30 surcharge is $4.00</p>';
    }
    else if ($surcharge_type === 'zero-one-two-fifty') {
      return '<p>The surcharge is calculated based on the total:<br>' .
        'If less than $5.00 there is no surcharge<br>' .
        'If less than $15.00 surcharge is $1.00<br>' .
        'For total over $15.00 surcharge is $2.50</p>';
    }

    $mysqli = connect_db();
    $surcharge = '';
    $min_surcharge = '';
    $max_surcharge = '';
    $query = 'SELECT surcharge, min_surcharge, max_surcharge FROM ' .
      'payment_settings WHERE system_group = "' . $this->user->group . '" OR ' .
      'system_group = "" ORDER BY system_group DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payment_settings = $mysqli_result->fetch_assoc()) {
        $surcharge = (float)$payment_settings['surcharge'];
        $min_surcharge = (float)$payment_settings['min_surcharge'];
        $max_surcharge = (float)$payment_settings['max_surcharge'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->Surcharge: ' . $mysqli->error);
    }
    $mysqli->close();
    
    // If the surcharge is greater than or equal to 0.4 then it's a flat rate.
    if ($surcharge >= 0.4) {
      return '<p>The surcharge is fixed at $' .
        number_format($surcharge, 2, '.', '') . '</p>';
    }

    $description = '';
    // Otherwise values between 0 and 0.4 are interpreted as a percentage.
    if ($surcharge > 0) {
      $surcharge *= 100;
      $description = '<p>The surcharge is ' . $surcharge . '% of the total<br>';
      if ($min_surcharge > 0) {
        $description .= 'with a minimum of $' .
          number_format($min_surcharge, 2, '.', '');
        if ($max_surcharge > 0) {
          $description .= ', and a maximum of $' .
            number_format($max_surcharge, 2, '.', '');
        }
      }
      else if ($max_surcharge > 0) {
        $description .= 'with a maximum of $' .
          number_format($max_surcharge, 2, '.', '');
      }
      $description .= '</p>';
    }
    return $description;
  }

  public function Total() {
    $total = 0;
    $mysqli = connect_db();
    $query = 'SELECT amount FROM payment_totals WHERE ' .
      'user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payment_totals = $mysqli_result->fetch_assoc()) {
        $total = (float)$payment_totals['amount'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->Total: ' . $mysqli->error);
    }
    $mysqli->close();
    return $total;
  }

  public function UpdateSetting($name, $value, $group = '') {
    if ($group === '') $group = $this->user->group;
    // Retrieve existing settings (or defaults if not defined for this group).
    $settings = $this->AllSettings($group);
    // Then overwrite the new setting that was provided from Groupwizard module.
    if ($value[0] === '$') $value = substr($value, 1);
    $settings[$name] = htmlspecialchars($value);

    $surcharge = (float)$settings['surcharge'];
    $min_surcharge = (float)$settings['min_surcharge'];
    $max_surcharge = (float)$settings['max_surcharge'];
    $deposit = (float)$settings['deposit'];
    $info = (float)$settings['info'];
    $warning = (float)$settings['warning'];
    $mysqli = connect_db();
    $query = 'INSERT INTO payment_settings VALUES (' .
      '"' . $group . '", ' . $surcharge . ', ' . $min_surcharge . ', ' .
      $max_surcharge . ',' . $deposit . ', ' . $info . ', ' . $warning . ') ' .
      'ON DUPLICATE KEY UPDATE surcharge = ' . $surcharge . ', ' .
      'min_surcharge = ' . $min_surcharge . ', ' .
      'max_surcharge = ' . $max_surcharge . ', deposit = ' . $deposit . ', ' .
      'info = ' . $info . ', warning = ' . $warning;
    if (!$mysqli->query($query)) {
      $this->Log('Payment->UpdateSetting: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  // Private functions below here ////////////////////////////////////////////

  private function AllData() {
    $data = [];
    $organiser = new Organiser($this->user, $this->owner);
    $offset = isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
    if ($offset < 0) $offset = 0;

    $mysqli = connect_db();
    $query = 'SELECT payments.user, timestamp, reference, amount, comment ' .
      'FROM payments LEFT JOIN users ON payments.user = users.user WHERE ' .
      'active = 1 AND ' . $organiser->GroupQuery() .
      ' ORDER BY timestamp DESC LIMIT ' . $offset . ', 50';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($payments = $mysqli_result->fetch_assoc()) {
        $amount = (float)$payments['amount'];
        $amount = number_format($amount, 2, '.', '');
        $timestamp = (int)$payments['timestamp'] * 1000;
        $data[] = ['date' => $timestamp, 'name' => $payments['user'],
                   'reference' => $payments['reference'],
                   'comment' => $payments['comment'], 'amount' => $amount];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Payment->AllData: ' . $mysqli->error);
    }
    $mysqli->close();
    return $data;
  }

  private function EditBanking() {
    $result = [];
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    if ($organiser->MatchUser($username)) {
      $reference =
        $mysqli->escape_string(htmlspecialchars($_POST['reference']));
      // The banking reference can't be empty, or longer than 20 characters.
      if ($reference === '' || strlen($reference) > 20) {
        $result['error'] = 'Bank reference must be 20 characters or less.';
      }
      else {
        $name = $mysqli->escape_string(htmlspecialchars($_POST['name']));
        $number = $mysqli->escape_string(htmlspecialchars($_POST['number']));
        $bsb = $mysqli->escape_string(htmlspecialchars($_POST['bsb']));
        $credit = (int)$_POST['credit'];
        $surcharge = (int)$_POST['surcharge'];
        $deposit = (int)$_POST['deposit'];
        $buyer_group = $mysqli->escape_string($_POST['buyerGroup']);
        $banking = new Banking($this->user, $this->owner);
        $result = $banking->UpdateUser($username, $reference, $name, $number,
                                       $bsb, $credit, $surcharge, $deposit,
                                       $buyer_group);
      }
    }
    else {
      $result['error'] = 'User not found.';
    }
    $mysqli->close();
    return $result;
  }

  private function EditPayment() {
    $edit = [];
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    $timestamp = (int)$_POST['timestamp'] / 1000;
    $reference = $mysqli->escape_string(htmlspecialchars($_POST['reference']));
    $amount = $mysqli->escape_string(htmlspecialchars($_POST['amount']));
    $comment = $mysqli->escape_string(htmlspecialchars($_POST['comment']));
    $new_payment = $_POST['newPayment'] === 'true';
    $import_mode = $_POST['importMode'] === 'true';
    // If the amount has a $ prefix remove it first.
    if (substr($amount, 0, 1) === '$') {
      $amount = substr($amount, 1);
    }

    if ($username === '') {
      $edit['error'] = 'No username given.';
    }
    else if ($timestamp === 0) {
      $edit['error'] = 'No date given.';
    }
    else if ((float)$amount < 0.001 && (float)$amount > -0.001) {
      $edit['error'] = 'No amount given.';
    }
    // The user being updated must match the system_group of the current user.
    else if (!$organiser->MatchUser($username)) {
      $edit['error'] = 'User not found.';
    }
    else {
      if ($new_payment) {
        $exists = false;
        // When entering a new payment make sure it gets a unique timestamp.
        // (Unless in import mode, where a duplicate may have been imported).
        do {
          $query = 'SELECT timestamp, amount FROM payments WHERE ' .
            'user = "' . $username . '" AND timestamp = ' . $timestamp;
          if ($mysqli_result = $mysqli->query($query)) {
            if ($payments = $mysqli_result->fetch_assoc()) {
              $exists = true;
              $timestamp++;
              if ($import_mode && $amount === $payments['amount']) break;
            }
            else {
              $exists = false;
            }
            $mysqli_result->close();
          }
        } while ($exists);
        if ($import_mode && $exists) {
          $mysqli->close();
          return ['exists' => true];
        }
      }
      $query = 'INSERT INTO payments VALUES ("' . $username . '", ' .
        $timestamp . ', "' . $reference . '", ' . $amount . ', ' .
        '"' . $comment . '", "' . $this->user->name . '") ON DUPLICATE KEY ' .
        'UPDATE reference = "' . $reference . '", amount = ' . $amount . ', ' .
        'comment = "' . $comment . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Payment->EditPayment: ' . $mysqli->error);
      }
      $edit['timestamp'] = $timestamp * 1000;
    }
    $mysqli->close();
    return $edit;
  }

  private function RemovePayment() {
    $data = [];
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $timestamp = (int)$_POST['timestamp'] / 1000;

    if ($name === '') {
      $data['error'] = 'No name given.';
    }
    else if ($timestamp === 0) {
      $data['error'] = 'No date given.';
    }
    else if (!$organiser->MatchUser($name)) {
      $data['error'] = 'User not found.';
    }
    else {
      $query = 'DELETE FROM payments WHERE user = "' . $name . '" AND ' .
        'timestamp = ' . $timestamp;
      if (!$mysqli->query($query)) {
        $this->Log('Payment->RemovePayment: ' . $mysqli->error);
      }
      $data = $this->AllData();
    }
    $mysqli->close();
    return $data;
  }

}
