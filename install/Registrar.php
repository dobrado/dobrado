<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Registrar extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    return false;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'RegisterDomain' && $count === 2) {
        $us_domain = $p[0];
        $us_price = $p[1];
        return $this->RegisterDomain($us_domain, $us_price);
      }
      if ($fn === 'SaveDetails' && $count === 9) {
        $us_domain = $p[0];
        $us_name = $p[1];
        $us_address = $p[2];
        $us_city = $p[3];
        $us_state = $p[4];
        $us_country = $p[5];
        $us_postcode = $p[6];
        $us_phone = $p[7];
        $us_email = $p[8];
        return $this->SaveDetails($us_domain, $us_name,  $us_address, $us_city,
                                  $us_state, $us_country, $us_postcode,
                                  $us_phone, $us_email);
      }
      return;
    }

    if ($fn === 'Check') return $this->API('domains:checkAvailability', $p);
    if ($fn === 'Search') return $this->API('domains:search', $p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS registrar (' .
      'domain VARCHAR(100) NOT NULL,' .
      'name VARCHAR(100) NOT NULL,' .
      'address VARCHAR(100) NOT NULL,' .
      'city VARCHAR(50) NOT NULL,' .
      'state VARCHAR(50) NOT NULL,' .
      'country VARCHAR(50) NOT NULL,' .
      'postcode VARCHAR(50) NOT NULL,' .
      'phone VARCHAR(50) NOT NULL,' .
      'email VARCHAR(100) NOT NULL,' .
      'privacy TINYINT(1),' .
      'locked TINYINT(1),' .
      'autorenew TINYINT(1),' .
      'created INT(10) UNSIGNED,' .
      'expires INT(10) UNSIGNED,' .
      'PRIMARY KEY(domain)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Registrar->Install: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function API($command, $post_fields = '', $try_again = true) {
    $username = '';
    $token = '';
    $endpoint = '';

    $mysqli = connect_db();
    $query = 'SELECT name, value FROM settings WHERE ' .
      'user = "' . $this->owner . '" AND label = "registrar"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($settings = $mysqli_result->fetch_assoc()) {
        if ($settings['name'] === 'username') {
          $username = $settings['value'];
        }
        else if ($settings['name'] === 'token') {
          $token = $settings['value'];
        }
        else if ($settings['name'] === 'endpoint') {
          $endpoint = $settings['value'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Registrar->API 1: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($username === '' || $token === '' || $endpoint === '') {
      return ['info' => 'Sorry domain search is not currently available.'];
    }

    set_time_limit(0);
    $domain = '';
    if ($command === 'domains:search') {
      $domain = $post_fields;
      $post_fields = json_encode(['keyword' => $domain]);
    }
    else if ($command === 'domains:checkAvailability') {
      $domain = $post_fields;
      $post_fields = json_encode(['domainNames' => [$domain]]);
    }
    $ch = curl_init($endpoint . $command);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $token);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $this->Log('Registrar->API 2: curl ' . $endpoint . $command);
    $body = curl_exec($ch);
    if (curl_errno($ch) === 0) {
      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if ($http_code !== 200) {
        $this->Log('Registrar->API 3: Error response from ' .
                   $endpoint . $command . "\nHTTP code: " . $http_code .
                   "\nBody: " . $body);
        $body = '';
      }
    }
    else {
      $this->Log('Registrar->API 4: Error connecting to ' . $endpoint .
                 $command . "\nCurl error: " . curl_error($ch));
    }
    curl_close($ch);

    $result_json = json_decode($body, true);
    if ($command === 'domains:search' ||
        $command === 'domains:checkAvailability') {
      $result_list = isset($result_json['results']) ?
        $result_json['results'] : [];
      $purchasable_list = [];
      if (count($result_list) !== 0) {
        foreach ($result_list as $result) {
          if ($result['domainName'] === $domain) {
            if (isset($result['purchasable']) &&
                $result['purchasable'] === true) {
              $purchasable_list = [$result];
            }
            break;
          }

          if (isset($result['purchasable']) &&
              $result['purchasable'] === true) {
            $purchasable_list[] = $result;
          }
        }
      }
      if (count($purchasable_list) > 0) {
        return ['info' => 'Search results', 'domains' => $purchasable_list];
      }

      // The name.com API sometimes returns no results after the first search,
      // but the same search does provide results on a second try.
      if ($try_again) {
        sleep(2);
        return $this->API($command, $domain, false);
      }

      if ($command === 'domains:search') {
        return ['info' => 'Sorry no search results were returned.'];
      }
      if ($command === 'domains:checkAvailability') {
        return ['info' => 'This domain name is registered.<br>It looks like ' .
                  'there\'s no IP address set. If you registered this domain ' .
                  'name and would like it hosted here, please set it to: <b>' .
                  $this->Substitute('domaincheck-ip') .
                  '</b> in your DNS settings.' .
                  '<p>Once the IP address matches, come back to this page ' .
                  'and enter your domain name again for the next step! ' .
                  '(If you visit your domain name and it redirects here, ' .
                  'that\'s great it means you can continue!)</p>'];
      }
    }
    return $result_json;
  }

  private function CountryCode($country) {
    $country = strtolower(trim($country));
    $codes = ['australia' => 'AU', 'brazil' => 'BR', 'canada' => 'CA',
              'switzerland' => 'CH', 'germany' => 'DE', 'spain' => 'ES',
              'france' => 'FR', 'england' => 'GB', 'northern ireland' => 'GB',
              'scotland' => 'GB', 'wales' => 'GB', 'uk' => 'GB',
              'ireland' => 'IE', 'new zealand' => 'NZ'];
    if (isset($codes[$country])) return $codes[$country];
    return 'US';
  }

  private function RegisterDomain($us_domain, $us_price) {
    $post_fields = json_encode(['domain' => ['domainName' => $us_domain],
                                'purchasePrice' => $us_price]);
    $result = $this->API('domains', $post_fields);
    if (!isset($result['domain'])) return false;

    $this->Log('Registrar->RegisterDomain 1: domainName: ' . $us_domain . ', ' .
               'purchasePrice: ' . $us_price . ', order: ' . $result['order'] .
               ', totalPaid: ' . $result['totalPaid']);

    sleep(1);
    // Once the domain name has been registered set the A record.
    $ip = $this->Substitute('domaincheck-ip');
    $post_fields = json_encode(['host' => '', 'type' => 'A',
                                'answer' => $ip, 'ttl' => '3600']);
    $this->API('domains/' . $us_domain . '/records', $post_fields);

    sleep(1);
    // Set an A record for the www subdomain too.
    $ip = $this->Substitute('domaincheck-ip');
    $post_fields = json_encode(['host' => 'www', 'type' => 'A',
                                'answer' => $ip, 'ttl' => '3600']);
    $this->API('domains/' . $us_domain . '/records', $post_fields);

    // And also set contact details.
    $name = '';
    $address = '';
    $city = '';
    $state = '';
    $country = '';
    $postcode = '';
    $phone = '';
    $email = '';

    $mysqli = connect_db();
    $domain = $mysqli->escape_string($us_domain);
    $query = 'SELECT name, address, city, state, country, postcode, phone, ' .
      'email FROM registrar WHERE domain = "' . $domain . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($registrar = $mysqli_result->fetch_assoc()) {
        $name = $registrar['name'];
        $address = $registrar['address'];
        $city = $registrar['city'];
        $state = $registrar['state'];
        $country = $registrar['country'];
        $postcode = $registrar['postcode'];
        $phone = $registrar['phone'];
        $email = $registrar['email'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Registrar->RegisterDomain 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $split = strpos($name, ' ');
    $first = $split ? substr($name, 0, $split) : $name;
    $last = $split ? substr($name, $split + 1) : '';
    // Country needs to be an ISO 3166-1 alpha-2 code.
    $country_code = $this->CountryCode($country);
    $registrant = ['firstName' => $first, 'lastName' => $last,
                   'address1' => $address, 'city' => $city, 'state' => $state,
                   'zip' => $postcode, 'country' => $country_code,
                   'phone' => $phone, 'email' => $email];

    sleep(1);
    $post_fields = json_encode(['contacts' => ['registrant' => $registrant]]);
    $this->API('domains/' . $us_domain . ':setContacts', $post_fields);
    return true;
  }

  private function SaveDetails($us_domain, $us_name,  $us_address,
                               $us_city, $us_state, $us_country,
                               $us_postcode, $us_phone, $us_email) {
    $mysqli = connect_db();
    $domain = $mysqli->escape_string($us_domain);
    $name = $mysqli->escape_string($us_name);
    $address = $mysqli->escape_string($us_address);
    $city = $mysqli->escape_string($us_city);
    $state = $mysqli->escape_string($us_state);
    $country = $mysqli->escape_string($us_country);
    $postcode = $mysqli->escape_string($us_postcode);
    $phone = $mysqli->escape_string($us_phone);
    $email = $mysqli->escape_string($us_email);
    $query = 'INSERT INTO registrar VALUES ("' . $domain . '", ' .
      '"' . $name . '", "' . $address . '", "' . $city . '", ' .
      '"' . $state . '", "' . $country . '", "' . $postcode . '", ' .
      '"' . $phone . '", "' . $email . '", 0, 0, 0, 0, 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Registrar->SaveDetails: ' . $mysqli->error);
    }
    $mysqli->close();
  }

}
