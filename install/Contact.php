<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Contact extends Base {

  public function Add($id) {
    $media = '@media screen and (max-device-width: 480px)';
    // Add some default style rules for each Contact module.
    $selector = '#dobrado-' . $id . ' .contact-form';
    $box_style = ['"","' . $selector . '","background-color","#e9e9e9"',
                  '"","' . $selector . '","border","1px solid #cccccc"',
                  '"","' . $selector . '","border-radius","3px"',
                  '"","' . $selector . '","padding","10px"',
                  '"","' . $selector . ' label","width","8em"',
                  '"' . $media . '","' . $selector . ' label","width",' .
                    '"inherit"',
                  '"","' . $selector . ' .submit","margin-left","8.3em"',
                  '"' . $media . '","' . $selector . ' .submit",' .
                    '"margin-left","0"'];

    $this->AddBoxStyle($box_style);

    $organiser = new Organiser($this->user, $this->owner);
    $parent = $organiser->Parent();
    if ($parent === false) $parent = '';
    $content = '<form class="contact-form">' .
      'When editing this form, make sure each field you add has a name ' .
      'attribute.<br>The field with the name "contact-name" is used on the ' .
      'server to match a recipient, which you can create using the Organiser ' .
      'module. (You can also set the value here and make this field hidden.)' .
      '<br>Don\'t edit the submit button or the hidden input field used to ' .
      'match your organisation.<br>' .
      '<div class="form-spacing">' .
        '<label>Contact name:</label><input name="contact-name" type="text">' .
      '</div>' .
      '<input name="contact-organisation" ' .
        'value="' . $parent . '" type="hidden">' .
      '<button class="submit">submit</button>' .
      '<span class="contact-info" style="display:none;">' .
        'ckeditor removes this span if empty</span>' .
      '</form>';
    $mysqli = connect_db();
    $this->Insert($id, $mysqli->escape_string($content));
    $mysqli->close();
  }

  public function Callback() {
    // mode is used by the Extended module, which calls this function.
    $us_mode = isset($_POST['mode']) ? $_POST['mode'] : '';
    if ($us_mode == 'box') {
      $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
      return ['source' => $this->PlainContent($id), 'editor' => true];
    }

    if ($us_mode == 'contact') {
      $us_content = json_decode($_POST['content'], true);
      $mysqli = connect_db();
      $name = $mysqli->escape_string($us_content['contact-name']);
      $organisation =
        $mysqli->escape_string($us_content['contact-organisation']);
      $mysqli->close();
      if ($name == '') {
        return ['error' => 'contact-name not given'];
      }

      $server = $this->user->config->ServerName();
      $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
        'https://' : 'http://';
      $reply_to = '';
      $subject = 'Message from ' . $server;
      $us_message = '<html><head><title>' . $subject . '</title></head>' .
        '<body><p>A message was received at: ' .
          '<a href="' . $scheme . $server . '">' . $server .
          '</a><br>For: <b>' . $name . '</b><br>Details:</p>';
      foreach ($us_content as $us_key => $us_value) {
        if ($us_key !== 'contact-name' && $us_key !== 'contact-organisation') {
          $us_message .= $us_key . ': ' .
            nl2br(htmlspecialchars($us_value), false) . '<br>';
          // Look for an email field to use as the 'reply-to'.
          if ($us_key === 'email') {
            $reply_to = $us_value;
          }
        }
      }
      $us_message .= '</body></html>';
      $us_message = wordwrap($us_message);
      $user = '';
      if ($this->Substitute('account-single-user') === 'true') {
        // The only email address created in single user mode is the server
        // name, so don't use organiser module in this case.
        $user = $this->user->config->ServerName();
      }
      else {
        $organiser = new Organiser($this->user, $this->owner);
        $user = $organiser->Contact($name, $organisation);
      }
      // This allows for having more than one domain on a server.
      $email_server = $this->Substitute('system-email-server');
      if ($email_server === '') $email_server = $server;
      $email = $name . ' <' . $user . '@' . $email_server . '>';
      $sender = $this->Substitute('system-sender', '/!host/', $server);
      $sender_name = $this->Substitute('system-sender-name');
      if ($sender_name === '') {
        $sender_name = $sender;
      }
      else {
        $sender_name .= ' <' . $sender . '>';
      }
      $cc = $this->Substitute('system-sender-cc');
      $bcc = $this->Substitute('system-sender-bcc');
      $headers = "MIME-Version: 1.0\r\n" .
        "Content-type: text/html; charset=utf-8\r\n" .
        'From: ' . $sender_name . "\r\n";
      if ($cc !== '') {
        $headers .= 'Cc: ' . $cc . "\r\n";
      }
      if ($bcc !== '') {
        $headers .= 'Bcc: ' . $bcc . "\r\n";
      }
      if ($reply_to !== '') {
        $headers .= 'Reply-To: ' . $reply_to . "\r\n";
      }
      if (mail($email, $subject, $us_message, $headers, '-f ' . $sender)) {
        return ['done' => true];
      }
      return ['error' => 'Email to ' . $email . ' not accepted for delivery.'];
    }
  }

  public function CanAdd($page) {
    // Must have admin access to add the contact module.
    return $this->user->canEditSite;
  }

  public function CanEdit($id) {
    // Must have admin access to edit the contact module.
    return $this->user->canEditSite;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<div class="dobrado-editable">' . $this->PlainContent($id) .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $this->Insert($id, $this->PlainContent($old_id, $old_owner, true));
    $this->CopyStyle($id, $old_owner, $old_id);
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS contact (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'content TEXT,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Contact->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS contact_history (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'content TEXT,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'modified_by VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, box_id, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Contact->Install 2: ' . $mysqli->error);
    }

    $mysqli->close();

    // Append dobrado.contact.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.contact.js', true);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM contact WHERE user = "' . $this->owner . '" ' .
        'AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Contact->Remove 1: ' . $mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM contact WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Contact->Remove 2: ' . $mysqli->error);
      }
      $query = 'DELETE FROM contact_history WHERE user = "' . $this->owner .'"';
      if (!$mysqli->query($query)) {
        $this->Log('Contact->Remove 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    if ($us_content['data'] === $this->PlainContent($id)) return;

    $time = time();
    $mysqli = connect_db();
    $data = $mysqli->escape_string($us_content['data']);
    $query= 'UPDATE contact SET content = "' . $data . '", ' .
      'timestamp = ' . $time . ' WHERE user = "' . $this->owner . '" AND ' .
      'box_id = ' . $id;
    if (!$mysqli->query($query)) {
      $this->Log('Contact->SetContent 1: ' . $mysqli->error);
    }

    $query = 'INSERT INTO contact_history VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $data . '", ' . $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Contact->SetContent 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.contact.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.contact.js', true);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Insert($id, $content='') {
    $time = time();
    $mysqli = connect_db();
    $query = 'INSERT INTO contact VALUES ' .
      '("' . $this->owner . '", ' . $id . ', "' . $content . '", ' . $time .')';
    if (!$mysqli->query($query)) {
      $this->Log('Contact->Insert 1: ' . $mysqli->error);
    }

    $query = 'INSERT INTO contact_history VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $content . '", ' . $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Contact->Insert 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function PlainContent($id, $user = '', $escape = false) {
    if ($user === '') {
      $user = $this->owner;
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT content FROM contact WHERE user = "' . $user . '" ' .
      'AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($contact = $mysqli_result->fetch_assoc()) {
        $content = $escape ? $mysqli->escape_string($contact['content']) :
          $contact['content'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Contact->PlainContent: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

}
