// @source: /js/source/dobrado.xero2.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2021 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.xero2){dobrado.xero2={};}
(function(){'use strict';$(function(){if($('.xero2').length===0){return;}
$('#xero2-submit').button().click(submit);});function submit(){$.post('/php/request.php',{enabled:$('#xero2-enabled-input:checked').length,request:'xero2',action:'submit',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'xero2 submit')){return;}
let xero=JSON.parse(response);if(xero.location){location.href=xero.location;}});return false;}})();