<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Commenteditor extends Base {

  public function Add($id) {
    $mysqli = connect_db();
    $query = 'INSERT INTO comment_settings VALUES ' .
      '("' . $this->owner . '", ' . $id . ', 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Add: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Callback() {
    $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
    $us_mode = isset($_POST['mode']) ? $_POST['mode'] : '';
    // box mode is used by the extended editor.
    if ($us_mode === 'box') {
      $description = '(Comments are open) ';
      $checked = '';
      if ($this->Locked($id)) {
        $description = '(Must be logged in to comment) ';
        $checked = ' checked';
      }
      list($allowlist, $blocklist) = $this->Checked();
      $allowlist_checked = $allowlist ? ' checked' : '';
      $blocklist_checked = $blocklist ? ' checked' : '';
      $allowlist_remove = '';
      $blocklist_remove = '';
      $allowlist_options = $this->Options('allowlist');
      if ($allowlist_options !== '') {
        $allowlist_remove = '<div class="form-spacing">' .
          '<label for="comment-allowlist-remove">Remove domain:</label>' .
          '<select id="comment-allowlist-remove" ' .
            'name="comment-allowlist-remove"><option></option>' .
            $allowlist_options .
          '</select></div>';
      }
      $blocklist_options = $this->Options('blocklist');
      if ($blocklist_options !== '') {
        $blocklist_remove = '<div class="form-spacing">' .
          '<label for="comment-blocklist-remove">Remove domain:</label>' .
          '<select id="comment-blocklist-remove" ' .
            'name="comment-blocklist-remove"><option></option>' .
            $blocklist_options .
          '</select></div>';
      }
      // Note: name attribute on inputs is used by extended editor.
      $custom = '<form id="extended-custom-settings">' .
        'Public comments can be locked so that any further comments can only ' .
        'be added to the current page by logged in users.' .
        '<div class="form-spacing">' .
          '<label for="comment-lock-input">Lock:</label>' .
          '<input id="comment-lock-input" type="checkbox" name="comment-lock"' .
            $checked . '> ' . $description .
        '</div>' .
        'Comments and notifications received via webmention will be ' .
        'displayed based on the following rules. These rules apply to all ' .
        'pages.' .
        '<div class="form-spacing">' .
          '<label for="comment-allowlist-check">Allow specified domains only:' .
          '</label>' .
          '<input id="comment-allowlist-check" type="checkbox" ' .
            'name="comment-allowlist-check"' . $allowlist_checked . '>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="comment-allowlist-add">Add domain:</label>' .
          '<input id="comment-allowlist-add" name="comment-allowlist-add" ' .
            'type="text">' .
        '</div>' . $allowlist_remove .
        '<div class="form-spacing">' .
          '<label for="comment-blocklist-check">Block specified domains:' .
          '</label>' .
          '<input id="comment-blocklist-check" type="checkbox" ' .
            'name="comment-blocklist-check"' . $blocklist_checked . '>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="comment-blocklist-add">Add domain:</label>' .
          '<input id="comment-blocklist-add" name="comment-blocklist-add" ' .
            'type="text">' .
        '</div>' . $blocklist_remove .
        '<button type="submit">Submit</button>' .
        '</form>';
      return ['editor' => false, 'source' => '', 'custom' => $custom];
    }

    if ($us_mode === 'submit' &&
        ($this->user->loggedIn || !$this->Locked($id))) {
      if (!isset($_POST['content']) || $_POST['content'] === '') {
        return ['error' => 'Cannot submit empty comment.'];
      }

      include 'library/HTMLPurifier.auto.php';
      $config = HTMLPurifier_Config::createDefault();
      $purifier = new HTMLPurifier($config);
      $comment = new Comment($this->user, $this->owner);
      $id = new_module($this->user, $this->owner, 'comment', $this->user->page,
                       $comment->Group(), $comment->Placement());
      // Want to use the new id in the permalink.
      $us_author = isset($_POST['name']) ? $_POST['name'] : '';
      $us_url = isset($_POST['website']) ? $_POST['website'] : '';
      $us_description = $purifier->purify($_POST['content']);
      $us_content = ['author' => $purifier->purify($us_author),
                     'url' => $purifier->purify($us_url),
                     'description' => nl2br($us_description, false),
                     'permalink' => $this->user->page . '#' . $id];
      $comment->Add($id);
      $comment->SetContent($id, $us_content);
      // The post module is notified about the comment to resend webmentions.
      $post = new Module($this->user, $this->owner, 'post');
      if ($post->IsInstalled()) $post->Factory('NewComment');
      return ['id' => 'dobrado-' . $id, 'content' => $comment->Content($id)];
    }
  }

  public function CanAdd($page) {
    // Can only have one comment editor module on a page.
    return !$this->AlreadyOnPage('commenteditor', $page);
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = false;
    if ($this->user->loggedIn) {
      $author = '';
      $name = $this->user->name;
      if (isset($this->user->settings['account']['username'])) {
        $name = $this->user->settings['account']['username'];
      }
      $url = $this->user->name === 'admin' ? '/' : '/' . $this->user->name;
      $detail = new Module($this->user, $this->owner, 'detail');
      if ($detail->IsInstalled()) {
        $user_detail = $detail->Factory('User');
        if ($user_detail['first'] !== '') {
          $name = $user_detail['first'];
        }
        if ($user_detail['last'] !== '') {
          if ($name !== '') $name .= ' ';
          $name .= $user_detail['last'];
        }
        if ($user_detail['thumbnail'] !== '') {
          $author = '<a href="' . $url . '">' .
            $user_detail['thumbnail'] . '</a> ';
        }
      }
      $author .= '<a href="' . $url . '">' . $name . '</a>';
      if ($this->user->canEditPage) {
        $content = '<button class="comments-editor">edit comment settings' .
          '</button>';
      }
      $content .= '<div class="new-comment-title">Add a comment</div>' .
        '<form id="comment-form">' .
          'Commenting as ' . $author . ':' .
          '<div class="form-spacing">' .
            '<textarea id="comment-content"></textarea>' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>';
    }
    else if (!$this->Locked($id)) {
      $content = '<div class="new-comment-title">Add a comment</div>' .
        '<form id="comment-form">' .
          '<div class="form-spacing">' .
            '<label for="comment-name">Name:</label>' .
            '<input id="comment-name" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="comment-url">Website:</label>' .
            '<input id="comment-url" type="text" maxlength="200">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<textarea id="comment-content"></textarea>' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>';
    }
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $mysqli = connect_db();
    $query = 'INSERT INTO comment_settings (user, box_id, locked) SELECT ' .
      '"' . $this->owner . '", ' . $id . ', locked FROM comment_settings ' .
      'WHERE user = "' . $old_owner . '" AND box_id = ' . $old_id;
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Copy: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'Blocked') {
      return $this->Blocked($p);
    }
  }

  public function Group() {
    return 'post-comment';
  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.commenteditor.js', true);

    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS comment_settings (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'locked TINYINT(1),' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS comment_url_check (' .
      'user VARCHAR(50) NOT NULL,' .
      'allowlist TINYINT(1),' .
      'blocklist TINYINT(1),' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Install 2: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS comment_url_list (' .
      'user VARCHAR(50) NOT NULL,' .
      'url VARCHAR(200) NOT NULL,' .
      'action ENUM("allowlist", "blocklist") NOT NULL,' .
      'PRIMARY KEY(user, url)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Install 3: ' . $mysqli->error);
    }
    $mysqli->close();

    $media = '@media screen and (max-device-width: 480px)';
    // Add style rules to the site_style table.
    $site_style = ['"","#commenteditor-likes-wrapper","padding-top","10px"',
                   '"","#commenteditor-likes-wrapper a","margin-left","10px"',
                   '"","#commenteditor-likes-wrapper img","width","40px"',
                   '"","#commenteditor-likes-label","font-size","1.2em"',
                   '"","#commenteditor-reposts-wrapper","padding-top","10px"',
                   '"","#commenteditor-reposts-wrapper a","margin-left","10px"',
                   '"","#commenteditor-reposts-wrapper img","width","40px"',
                   '"","#commenteditor-reposts-label","font-size","1.2em"',
                   '"","#comment-form","background-color","#eeeeee"',
                   '"","#comment-form","border","1px solid #aaaaaa"',
                   '"","#comment-form","border-radius","2px"',
                   '"","#comment-form","margin","5px"',
                   '"","#comment-form","padding","10px"',
                   '"","#comment-form .thumb","width","20px"',
                   '"","#comment-form .thumb","border-radius","2px"',
                   '"","#comment-form label","width","4em"',
                   '"' . $media . '","#comment-form label","width","inherit"',
                   '"","#comment-content","width","98%"',
                   '"","#comment-content","height","70px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-allowlist-check]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-allowlist-add]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-allowlist-remove]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-blocklist-check]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-blocklist-add]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-blocklist-remove]","width","280px"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['comment']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM comment_settings WHERE ' .
        'user = "' . $this->owner . '" AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->Remove 1: ' . $mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM comment_settings WHERE ' .
        'user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->Remove 2: ' . $mysqli->error);
      }
      $query = 'DELETE FROM comment_url_check WHERE ' .
        'user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->Remove 3: ' . $mysqli->error);
      }
      $query = 'DELETE FROM comment_url_list WHERE ' .
        'user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->Remove 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    $mysqli = connect_db();
    if (isset($us_content['comment-lock'])) {
      $locked = $mysqli->escape_string($us_content['comment-lock']);
      $query = 'UPDATE comment_settings SET locked = "' . $locked . '" ' .
        'WHERE user = "' . $this->owner . '" AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 1: ' . $mysqli->error);
      }
    }
    if (isset($us_content['comment-allowlist-check'])) {
      $check = (int)$us_content['comment-allowlist-check'];
      $query = 'INSERT INTO comment_url_check VALUES ("' . $this->owner . '", '.
        $check . ', 0) ON DUPLICATE KEY UPDATE allowlist = ' . $check;
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 2: ' . $mysqli->error);
      }
    }
    if (isset($us_content['comment-allowlist-add'])) {
      $url = $mysqli->escape_string($us_content['comment-allowlist-add']);
      $query = 'INSERT INTO comment_url_list VALUES ("' . $this->owner . '", ' .
        '"' . $url . '", "allowlist") ON DUPLICATE KEY UPDATE ' .
        'action = "allowlist"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 3: ' . $mysqli->error);
      }
    }
    if (isset($us_content['comment-allowlist-remove'])) {
      $url = $mysqli->escape_string($us_content['comment-allowlist-remove']);
      $query = 'DELETE FROM comment_url_list WHERE ' .
        'user = "' . $this->owner . '" AND url = "' . $url . '" AND ' .
        'action = "allowlist"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 4: ' . $mysqli->error);
      }
    }
    if (isset($us_content['comment-blocklist-check'])) {
      $check = (int)$us_content['comment-blocklist-check'];
      $query = 'INSERT INTO comment_url_check VALUES ("' . $this->owner . '", '.
        '0, ' . $check . ') ON DUPLICATE KEY UPDATE blocklist = ' . $check;
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 5: ' . $mysqli->error);
      }
    }
    if (isset($us_content['comment-blocklist-add'])) {
      $url = $mysqli->escape_string($us_content['comment-blocklist-add']);
      $query = 'INSERT INTO comment_url_list VALUES ("' . $this->owner . '", ' .
        '"' . $url . '", "blocklist") ON DUPLICATE KEY UPDATE ' .
        'action = "blocklist"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 6: ' . $mysqli->error);
      }
    }
    if (isset($us_content['comment-blocklist-remove'])) {
      $url = $mysqli->escape_string($us_content['comment-blocklist-remove']);
      $query = 'DELETE FROM comment_url_list WHERE ' .
        'user = "' . $this->owner . '" AND url = "' . $url . '" AND ' .
        'action = "blocklist"';
      if (!$mysqli->query($query)) {
        $this->Log('Commenteditor->SetContent 7: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function Update() {
    $mysqli = connect_db();
    $query = 'ALTER TABLE comment_url_check CHANGE COLUMN whitelist ' .
      'allowlist TINYINT(1)';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Update 1: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE comment_url_check CHANGE COLUMN blacklist ' .
      'blocklist TINYINT(1)';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Update 2: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE comment_url_list MODIFY COLUMN action ' .
      'ENUM("allowlist", "blocklist") NOT NULL';
    if (!$mysqli->query($query)) {
      $this->Log('Commenteditor->Update 3: ' . $mysqli->error);
    }
    $mysqli->close();

    $site_style = ['"","#extended-custom-settings ' .
                     'label[for=comment-allowlist-check]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-allowlist-add]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-allowlist-remove]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-blocklist-check]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-blocklist-add]","width","280px"',
                   '"","#extended-custom-settings ' .
                     'label[for=comment-blocklist-remove]","width","280px"'];
    $this->AddSiteStyle($site_style);
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.commenteditor.js', true);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Blocked($us_url) {
    list($allowlist, $blocklist) = $this->Checked();
    if (!$allowlist && !$blocklist) return false;

    if (stripos($us_url, 'http') === 0) {
      if (preg_match('/^https?:\/\/([^\/]+)/i', $us_url, $match)) {
        $us_url = $match[1];
      }
    }
    $action = '';
    $mysqli = connect_db();
    $url = $mysqli->escape_string($us_url);
    $query = 'SELECT action FROM comment_url_list WHERE ' .
      'user = "' . $this->owner . '" AND url LIKE "%' . $url . '%"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($comment_url_list = $mysqli_result->fetch_assoc()) {
        $action = $comment_url_list['action'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Commenteditor->Blocked: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($allowlist && $action !== 'allowlist') return true;
    if ($blocklist && $action === 'blocklist') return true;
    return false;
  }

  private function Checked() {
    $allowlist = false;
    $blocklist = false;
    $mysqli = connect_db();
    $query = 'SELECT allowlist, blocklist FROM comment_url_check WHERE ' .
      'user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($comment_url_check = $mysqli_result->fetch_assoc()) {
        $allowlist = $comment_url_check['allowlist'] === '1';
        $blocklist = $comment_url_check['blocklist'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Commenteditor->Checked: ' . $mysqli->error);
    }
    $mysqli->close();
    return [$allowlist, $blocklist];
  }

  private function Locked($id) {
    $locked = true;
    $mysqli = connect_db();
    $query = 'SELECT locked FROM comment_settings WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($comment_settings = $mysqli_result->fetch_assoc()) {
        $locked = (int)$comment_settings['locked'] === 1;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Commenteditor->Locked: ' . $mysqli->error);
    }
    $mysqli->close();
    return $locked;
  }

  private function Options($action) {
    $options = '';
    $mysqli = connect_db();
    $query = 'SELECT url FROM comment_url_list WHERE ' .
      'user = "' . $this->owner . '" AND action = "' . $action . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($comment_url_list = $mysqli_result->fetch_assoc()) {
        $options .= '<option>' . $comment_url_list['url'] . '</option>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Commenteditor->Options: ' . $mysqli->error);
    }
    $mysqli->close();
    return $options;
  }

}
