<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Report extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view reports.'];
    }

    $mysqli = connect_db();
    $action = isset($_POST['action']) ?
      $mysqli->escape_string($_POST['action']) : '';
    $group = isset($_POST['group']) ? $mysqli->escape_string($_POST['group']) :
      $this->user->group;
    $mysqli->close();

    if ($action === 'init') {
      return $this->Init($group);
    }
    if ($action === 'changeGroup') {
      return $this->ChangeGroup($group);
    }
    if ($action === 'collateOrders') {
      return $this->CollateOrders($group);
    }
    if ($action === 'showQuotas') {
      return $this->ShowQuotas($group);
    }
    if ($action === 'showOrder') {
      return $this->ShowOrder($group);
    }
    if ($action === 'purchaseHistory') {
      return $this->PurchaseHistory($group);
    }
    if ($action === 'paymentHistory') {
      return $this->PaymentHistory($group);
    }
    if ($action === 'salesHistory') {
      return $this->SalesHistory($group);
    }
    if ($action === 'attendanceHistory') {
      return $this->AttendanceHistory($group);
    }
    if ($action === 'membershipReminders') {
      return $this->MembershipReminders($group);
    }
    if ($action === 'lastAttendance') {
      return $this->LastAttendance($group);
    }
    if ($action === 'accountsPayable') {
      return $this->AccountsPayable($group);
    }
    if ($action === 'activeMembers') {
      return $this->ActiveMembers($group);
    }
    if ($action === 'paidDeposit') {
      return $this->PaidDeposit($group);
    }
  }

  public function CanAdd($page) {
    if (!$this->user->canEditSite) return false;
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    $invite_group = false;
    $default_group = $this->user->group;
    $purchase_group = isset($_SESSION['purchase-group']) ?
      $_SESSION['purchase-group'] : '';
    // Show a select if the current user has created any invite groups, or if
    // their default group is part of an organisation, in which case add the
    // organisation too.
    $organisation = false;
    $organiser = new Organiser($this->user, $this->owner);
    $siblings = $organiser->Siblings();
    if (count($siblings) > 1) {
      // Default to showing reports for the current value of purchase_group if
      // it's part of the organisation, otherwise show reports for the whole
      // organisation.
      if (in_array($purchase_group, $siblings)) {
        $this->user->group = $purchase_group;
      }
      $parent = $organiser->Parent();
      if ($parent === false) $parent = '';
      // The organisation can use 'group-name' to add a descriptive name.
      $parent_description = $this->Substitute('group-name', '', '', $parent);
      if ($parent_description === '') $parent_description = $parent;
      $content .= '<div class="report-spacing">Showing report for ' .
        '<select id="report-group-select">' .
          '<option value="' . $parent . '">' . $parent_description .'</option>';
      for ($i = 0; $i < count($siblings); $i++) {
        $group = $siblings[$i];
        if ($group === $this->user->group && $purchase_group !== '') {
          $group_name = $this->Substitute('group-name');
          // If a group has the same name as it's parent just use the group.
          if ($group_name === $parent_description) $group_name = $group;
          $content .= '<option selected="selected" value="' . $group . '">' .
            $group_name . '</option>';
        }
        else {
          $group_name = $this->Substitute('group-name', '', '', $group);
          if ($group_name === $parent_description) $group_name = $group;
          $content .= '<option value="' . $group . '">' . $group_name .
            '</option>';
        }
      }
      // If purchase_group isn't set, show report for the organisation.
      if ($purchase_group === '') {
        $organisation = true;
      }
    }
    $invite = new Invite($this->user, $this->owner);
    $created = $invite->Created();
    if (count($created) !== 0) {
      if (in_array($purchase_group, $created)) {
        $this->user->group = $purchase_group;
      }
      if ($content === '') {
        $content .= '<div class="report-spacing">Showing report for ' .
          '<select id="report-group-select">' .
            '<option value="' . $default_group . '">' .
              $this->Substitute('group-name', '', '', $default_group) .
            '</option>';
      }
      for ($i = 0; $i < count($created); $i++) {
        $group = $created[$i];
        if ($group === $this->user->group) {
          $content .= '<option selected="selected" value="' . $group . '">' .
            $this->Substitute('group-name') . '</option>';
          $invite_group = true;
        }
        else {
          $content .= '<option value="' . $group . '">' .
            $this->Substitute('group-name', '', '', $group) . '</option>';
        }
      }
    }
    if ($content !== '') {
      $content .= '</select></div>';
    }
    $report_content = $this->CreateReport($invite_group, $organisation);
    $content .= '<div class="report-content">' . $report_content . '</div>';
    $this->user->group = $default_group;
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.report.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.report.js', false);

    $template = ['"report-taxable","","Tax"'];
    $this->AddTemplate($template);

    $site_style = ['"",".report label","width","5em"',
                   '"","#report-collate-orders","margin-left","4.8em"',
                   '"","#report-collate-orders","margin-bottom","1em"',
                   '"","#report-collate-orders-search","border",' .
                     '"1px solid #aaaaaa"',
                   '"","#report-collate-orders-search","margin-bottom","20px"',
                   '"","#report-collate-orders-search","padding","5px"',
                   '"","#report-order-download","margin-left","10px"',
                   '"",".report-quota-wrapper","border","1px solid #777777"',
                   '"",".report-quota-wrapper","display","inline-block"',
                   '"",".report-quota-wrapper","height","10px"',
                   '"",".report-quota-wrapper","width","100px"',
                   '"",".report-quota-bar","display","block"',
                   '"",".report-quota-bar","height","100%"',
                   '"",".report-order","font-size","1.4em"',
                   '"","#report-quotas-button","margin","5px"',
                   '"",".report-spacing","padding","20px"',
                   '"",".report-purchase","font-size","1.4em"',
                   '"",".report-purchase","margin-top","20px"',
                   '"",".report-purchase-search","border","1px solid #aaaaaa"',
                   '"",".report-purchase-search","margin-bottom","20px"',
                   '"",".report-purchase-search","padding","5px"',
                   '"",".report-received-payment-search","border",' .
                     '"1px solid #aaaaaa"',
                   '"",".report-received-payment-search",' .
                     '"margin-bottom","20px"',
                   '"",".report-received-payment-search","padding","5px"',
                   '"",".report-outgoing-payment-search","border",' .
                     '"1px solid #aaaaaa"',
                   '"",".report-outgoing-payment-search",' .
                     '"margin-bottom","20px"',
                   '"",".report-outgoing-payment-search","padding","5px"',
                   '"",".report-sales-search","border","1px solid #aaaaaa"',
                   '"",".report-sales-search","margin-bottom","20px"',
                   '"",".report-sales-search","padding","5px"',
                   '"",".report-attendance-search","border",' .
                     '"1px solid #aaaaaa"',
                   '"",".report-attendance-search","margin-bottom","20px"',
                   '"",".report-attendance-search","padding","5px"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['invite', 'payment', 'purchase', 'stock']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.report.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function AccountsPayable($group) {
    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show accounts for this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }

    $detail = new Detail($this->user, $this->owner);
    $all_users = $detail->AllUsers($organisation);

    $banking = new Banking($this->user, $this->owner);
    $all_settings = $banking->AllSettings();

    $payment = new Payment($this->user, $this->owner);
    $purchase = new Purchase($this->user, $this->owner);
    // Get a list of payment totals for all users, so that they can be
    // compared to purchase totals to find outstanding debts.
    $payment_totals = $payment->AllTotals($organisation);
    // Look up the configured value for when an account is in balance.
    // Note that this function works with the negative of the balance and then
    // negates it at the end, so do the same with the configured value.
    $invoice_balance = (float)$this->Substitute('invoice-balance') * -1;
    // Note that AllOutstanding returns how much a user owes, so negate
    // it to show a balance.
    $all_outstanding = $purchase->AllOutstanding($payment_totals, time(),
                                                 $organisation);
    $total = 0;
    $data = [];
    foreach ($all_outstanding as $user => $balance) {
      if (isset($all_settings[$user]['credit']) &&
          $all_settings[$user]['credit'] === 1) {
        continue;
      }
      if ($balance > $invoice_balance - 0.1) {
        continue;
      }
      $full_name = '';
      if (isset($all_users[$user]['first'])) {
        $full_name = $all_users[$user]['first'];
      }
      if (isset($all_users[$user]['last'])) {
        if ($full_name !== '') {
          $full_name .= ' ';
        }
        $full_name .= $all_users[$user]['last'];
      }
      $email = '';
      if (isset($all_users[$user]['email'])) {
        $email = $all_users[$user]['email'];
      }
      $payment = $balance - $invoice_balance;
      $total += $payment;
      $data[] = ['user' => $user, 'fullname' => $full_name, 'email' => $email,
                 'data' => price_string($payment * -1)];
    }
    $this->user->group = $default_group;
    $info = '';
    if (count($data) === 1) {
      $info = '<b>1</b> account payable, total: <b>$';
    }
    else {
      $info = '<b>' . count($data) . '</b> accounts payable, total: <b>$';
    }
    $info .= price_string($total * -1) . '</b>';

    if ($_POST['download'] === '1' && count($data) > 0) {
      $date = date('Y-m-d');
      $filename = 'accounts-payable-' . $date . '.csv';
      $this->CreateCSV($filename, $data, false, 'payment');
      return ['info' => $info, 'data' => $data, 'filename' => $filename];
    }
    return ['info' => $info, 'data' => $data];
  }

  private function ActiveMembers($group) {
    if (!$this->GroupMember('admin', 'admin')) {
      return ['error' => 'Permission denied showing active members.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show accounts for this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }

    $detail = new Detail($this->user, $this->owner);
    $all_users = $detail->AllUsers($organisation);
    $group_query = '';
    if ($organisation) {
      $group_query = $organiser->GroupQuery();
    }
    else {
      $group_query = 'users.system_group = "' . $this->user->group . '"';
    }
    $total = 0;
    $data = [];
    $mysqli = connect_db();
    $query = 'SELECT user, active FROM users WHERE user NOT LIKE "buyer\_%" ' .
      'AND ' . $group_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $username = $users['user'];
        $active = (int)$users['active'];
        if ($active === 1) $total++;
        $full_name = '';
        if (isset($all_users[$username]['first'])) {
          $full_name = $all_users[$username]['first'];
        }
        if (isset($all_users[$username]['last'])) {
          if ($full_name !== '') {
            $full_name .= ' ';
          }
          $full_name .= $all_users[$username]['last'];
        }
        $email = '';
        if (isset($all_users[$username]['email'])) {
          $email = $all_users[$username]['email'];
        }
        $data[] = ['user' => $username, 'fullname' => $full_name,
                   'email' => $email, 'data' => $active];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Report->ActiveMembers: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->user->group = $default_group;
    $info = '';
    if ($total === 1) {
      $info = '<b>1</b> active member';
    }
    else {
      $info = '<b>' . $total . '</b> active members';
    }
    $info .= ' (' . (count($data) - $total) . ' inactive).';

    if ($_POST['download'] === '1' && count($data) > 0) {
      $date = date('Y-m-d');
      $filename = 'active-members-' . $date . '.csv';
      $this->CreateCSV($filename, $data, false, 'active');
      return ['info' => $info, 'data' => $data, 'filename' => $filename];
    }
    return ['info' => $info, 'data' => $data];
  }

  private function AttendanceHistory($group) {
    if (!$this->GroupMember('admin', 'admin')) {
      return ['error' => 'Permission denied showing attendance history.'];
    }

    $start = strtotime(date('F j Y 00:00:00', (int)$_POST['start'] / 1000));
    $end = strtotime(date('F j Y 23:59:59', (int)$_POST['end'] / 1000));
    if (!$start || !$end || $start > $end) {
      return ['error' => 'Start and End dates required.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $purchase = new Purchase($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show attendance history for this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }

    $date = date('j M Y', $start);
    $data = [];
    $attendance_total = 0;
    // Want to show up to 10 data points in the graph, so find the number of
    // days being shown and calculate the number of days in each data point.
    $days = (int)ceil(($end - $start) / 86400);
    $segment = $days > 10 ? (int)ceil($days / 10) : 1;
    $next = $start + ($segment * 86400);
    for ($i = 0; $i < 10; $i++) {
      // Make sure $next isn't greater than the requested end date.
      if ($next > $end) $next = $end;
      $segment_total = $purchase->Attendance($start, $next, $organisation);
      $data[] = [$start * 1000, $segment_total];
      $attendance_total += $segment_total;
      if ($days === 1) break;
      $start = $next + 1;
      $next += $segment * 86400;
    }
    $this->user->group = $default_group;
    $result = ['date' => $date, 'total' => $attendance_total];
    // Return data for a single day but don't graph it.
    if ($days === 1) return $result;

    // Add the end date to the displayed date text.
    $result['date'] .= ' - ' . date('j M Y', $end);
    $result['series'] = [['showMarker' => false]];
    // There's only one data series to display, so it can just be wrapped in
    // another array here.
    $result['data'] = ['type' => 'time', 'x_axis' => '%d %b',
                       'y_axis' => '%.0f', 'min' => 0, 'data' => [$data]];
    return $result;
  }

  private function ChangeGroup($group) {
    $result = $this->Init($group);
    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $default_group = $this->user->group;

    if ($group === $organiser->Parent()) {
      $result['content'] = $this->CreateReport(false, true);
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
      $result['content'] = $this->CreateReport(false, false);
    }
    else if (in_array($group, $invite->Created())) {
      $this->user->group = $group;
      $result['content'] = $this->CreateReport(true, false);
    }
    $this->user->group = $default_group;
    return $result;
  }

  private function CollateOrders($group) {
    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $invoice = new Invoice($this->user, $this->owner);
    // Ignore a group in the organisation such as 'non-purchasing'.
    $ignore_group = $this->Substitute('invoice-ignore-group');
    $siblings = $organiser->Siblings($ignore_group);
    $default_group = $this->user->group;
    $start = $_POST['start'] === '0' ? 0 :
      strtotime(date('F j Y 00:00:00', (int)$_POST['start'] / 1000));
    $end = $_POST['end'] === '0' ? 0 :
      strtotime(date('F j Y 23:59:59', (int)$_POST['end'] / 1000));
    $organisation = false;
    $invite_group = false;
    if ($group === $organiser->Parent()) {
      $organisation = true;
      $this->user->group = $group;
      $invoice->NextOrder(true, true, $siblings, false, $start, $end);
    }
    else if (in_array($group, $siblings)) {
      $this->user->group = $group;
      $invoice->NextOrder(true, false, [], false, $start, $end);
    }
    else if (in_array($group, $invite->Created())) {
      $invite_group = true;
      $this->user->group = $group;
      $invoice->NextOrder(true, false, [], false, $start, $end);
    }
    else {
      return ['error' => 'Group not found.'];
    }
    $this->user->group = $default_group;
    return ['content' => $this->FormatSelect($invite_group || $organisation)];
  }

  private function CreateReport($invite_group, $organisation) {
    $content = '';
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day contains commas, assume this is multiple weekdays and use
    // the first day listed.
    if (strpos($co_op_day, ',') !== false) {
      $co_op_day = strstr($co_op_day, ',', true);
    }
    $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                 'Friday', 'Saturday'];
    $weekly = in_array($co_op_day, $weekdays);
    $start = 0;
    $end = 0;
    $purchase = new Purchase($this->user, $this->owner);
    if ($organisation) {
      $content .= '<i>Showing totals for all groups.</i><br>';
    }
    // Only show an order total for the group if ordering is done on a weekly
    // basis, or if the current purchase date is in the future.
    if ($this->Substitute('pre-order') === 'true') {
      $display_total = false;
      $content .= '<div class="report-spacing">';
      if ($this->Substitute('report-change-order-dates') === 'true') {
        $content .= '<div id="report-collate-orders-search">' .
            'Enter dates to collate order:' .
            '<div class="form-spacing">' .
              '<label for="report-collate-start">Start:</label>' .
              '<input id="report-collate-start" maxlength="50" type="text">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="report-collate-end">End:</label>' .
              '<input id="report-collate-end" maxlength="50" type="text">' .
            '</div>' .
            '<button id="report-collate-orders">collate orders</button>' .
            '<span id="report-collate-info"></span>' .
          '</div>';
      }
      else {
        $content .= '<div id="report-collate-orders-search">' .
            'If the order has been modified after the collated order ' .
            'email was sent, you can re-process the order now and then ' .
            'download the new order list when it has finished processing: ' .
            '<button id="report-collate-orders">collate orders</button>' .
            '<span id="report-collate-info"></span>' .
          '</div>';
      }
      $content .= '<div id="report-order-info">Download lists for order: ' .
          '<select id="report-format-select">' .
            $this->FormatSelect($invite_group || $organisation) . '</select>' .
          '<button id="report-order-download">download</button>' .
        '</div><div id="report-order-result"></div>';
      if ($weekly) {
        $display_total = true;
        $content .= '<div class="report-order">Total orders for next week: ';
        $start = (int)strtotime('next ' . $co_op_day);
        $end = $start + 86400;
      }
      else if (strtotime($co_op_day) > time()) {
        $display_total = true;
        $start = (int)strtotime($co_op_day);
        $end = $start + 86400;
        // If the buying date for this order is more than 6 months away, don't
        // show co_op_day as it just means the order will be closed later.
        $six_months = time() + (86400 * 182);
        if ($start > $six_months) {
          $content .= '<div class="report-order">Total orders: ';
        }
        else {
          $content .= '<div class="report-order">Total orders for ' .
            $co_op_day . ': ';
        }
      }
      if ($display_total) {
        $all_totals = $purchase->AllTotals($start, $end, $organisation,
                                           $invite_group);
        $order_total = 0;
        foreach ($all_totals as $user => $total) {
          $order_total += $total;
        }
        $content .= '<b>$' . price_string($order_total) . '</b></div>';
      }
      // Close "report-spacing" div before "report-quotas" div which needs to
      // be the full width of the parent to show the grid properly.
      $content .= '<button id="report-quotas-button">show quotas</button>' .
        '</div><div class="report-quotas"></div>';
    }
    $content .= '<div class="report-spacing">';
    // Show total purchases for the most recent co-op-day.
    $start = 0;
    $end = 0;
    if ($weekly) {
      $start = (int)strtotime($co_op_day);
      if ($start > time()) {
        $start = (int)strtotime('last ' . $co_op_day);
      }
      $end = $start + 7 * 86400;
      $content .= '<div class="report-purchase">Total purchases for ' .
        '<span class="report-purchase-date">this week</span>: ';
    }
    else if (strtotime($co_op_day) < time()) {
      $start = (int)strtotime($co_op_day);
      $end = $start + 86400;
      $content .= '<div class="report-purchase">Total purchases for ' .
        '<span class="report-purchase-date">' . $co_op_day . '</span>: ';
    }
    else {
      $content .= '<div class="report-purchase hidden">Total purchases for ' .
          '<span class="report-purchase-date"></span>: ' .
            '<b>$<span class="report-purchase-total"></span></b>' .
        '</div>' .
        '<div class="report-taxable hidden">' .
          $this->Substitute('report-taxable') . ':' .
          '<b>$<span class="report-taxable-total"></span></b>' .
        '</div>' .
        '<div class="report-surcharge hidden">Total surcharge: ' .
          '<b>$<span class="report-surcharge-total"></span></b>' .
        '</div>';
    }
    if ($start !== 0 && $end !== 0) {
      $all_totals = $purchase->AllTotals($start, $end, $organisation,
                                         $invite_group);
      $purchase_total = 0;
      foreach ($all_totals as $user => $total) {
        $purchase_total += $total;
      }
      $content .= '<b>$<span class="report-purchase-total">' .
        price_string($purchase_total) . '</span></b></div>';
      if ($this->Substitute('stock-taxable') !== '') {
        $all_taxable = $purchase->AllTaxable($start, $end, $organisation,
                                             $invite_group);
        $taxable_total = 0;
        foreach ($all_taxable as $user => $total) {
          $taxable_total += $total;
        }
        $content .= '<div class="report-taxable">' .
          $this->Substitute('report-taxable') . ':' .
          '<b>$<span class="report-taxable-total">' .
          price_string($taxable_total) . '</span></b></div>';
      }
      // Invite groups aren't allowed to generate their own invoices, which
      // means they can't add a surcharge either.
      if (!$invite_group && $this->Substitute('surcharge') === 'true') {
        $all_surcharge = $purchase->AllSurcharge($start, $end);
        $surcharge_total = 0;
        foreach ($all_surcharge as $user => $surcharge) {
          $surcharge_total += $surcharge;
        }
        $content .= '<div class="report-surcharge">Total surcharge: ' .
          '<b>$<span class="report-surcharge-total">' .
            price_string($surcharge_total) . '</span></b></div>';
      }
    }
    $content .= '<div class="report-purchase-graph"></div>' .
      '<div class="report-purchase-search">Enter dates to view purchase ' .
        'history:' .
        '<div class="form-spacing">' .
          '<label for="report-purchase-start">Start:</label>' .
          '<input id="report-purchase-start" maxlength="50" type="text">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="report-purchase-end">End:</label>' .
          '<input id="report-purchase-end" maxlength="50" type="text">' .
        '</div>' .
        '<button id="report-purchase-button">submit</button>' .
      '</div>';
    // Show total payments for the previous week.
    if (!$invite_group) {
      $payment = new Payment($this->user, $this->owner);
      $received_payments = $payment->SearchTotal(strtotime('-1 week'), time(),
                                                 $organisation, 'received');
      $outgoing_payments = $payment->SearchTotal(strtotime('-1 week'), time(),
                                                 $organisation, 'outgoing');
      // Outgoing payment search returns a negative number, display as positive.
      $outgoing_payments *= -1;
      $content .= '<div class="report-received-payments">' .
          'Total received payments ' .
          '<span class="report-received-payment-date">this week</span>: ' .
          '<b>$<span class="report-received-payment-total">' .
            price_string($received_payments) . '</span></b></div>' .
        '<div class="report-received-payment-graph"></div>' .
        '<div class="report-received-payment-search">' .
          'Enter dates to view received payment history:' .
          '<div class="form-spacing">' .
            '<label for="report-received-payment-start">Start:</label>' .
            '<input id="report-received-payment-start" maxlength="50" ' .
              'type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="report-received-payment-end">End:</label>' .
            '<input id="report-received-payment-end" maxlength="50" ' .
              'type="text">' .
          '</div>' .
          '<button id="report-received-payment-button">submit</button>' .
        '</div>' .
        '<div class="report-outgoing-payments">' .
          'Total outgoing payments ' .
          '<span class="report-outgoing-payment-date">this week</span>: ' .
          '<b>$<span class="report-outgoing-payment-total">' .
            price_string($outgoing_payments) . '</span></b></div>' .
        '<div class="report-outgoing-payment-graph"></div>' .
        '<div class="report-outgoing-payment-search">' .
          'Enter dates to view outgoing payment history:' .
          '<div class="form-spacing">' .
            '<label for="report-outgoing-payment-start">Start:</label>' .
            '<input id="report-outgoing-payment-start" maxlength="50" ' .
              'type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="report-outgoing-payment-end">End:</label>' .
            '<input id="report-outgoing-payment-end" maxlength="50" ' .
              'type="text">' .
          '</div>' .
          '<button id="report-outgoing-payment-button">submit</button>' .
        '</div>';
    }
    if ($this->GroupMember('admin', 'admin')) {
      $content .= '<div class="report-sales hidden">Total sales for ' .
          '<span class="report-sales-date"></span>: ' .
            '<b>$<span class="report-sales-total"></span></b>' .
          '<div class="report-sales-graph"></div>' .
        '</div>' .
        '<div class="report-sales-search">Look up sales for a product ' .
          '(displays cost price):' .
          '<div class="form-spacing">' .
            '<label for="report-product">Product:</label>' .
            '<input id="report-product" maxlength="100" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="report-supplier">Supplier:</label>' .
            '<input id="report-supplier" maxlength="50" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="report-sales-start">Start:</label>' .
            '<input id="report-sales-start" maxlength="50" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="report-sales-end">End:</label>' .
            '<input id="report-sales-end" maxlength="50" type="text">' .
          '</div>' .
          '<button id="report-sales-button">submit</button>' .
        '</div>';
    }
    if (!$invite_group && $this->GroupMember('admin', 'admin')) {
      if ($start !== 0 && $end !== 0) {
        $content .= '<div class="report-attendance">';
        if ($weekly) {
          $content .= 'Number of buyers ' .
            '<span class="report-attendance-date">this week</span>: ';
        }
        else {
          $content .= 'Number of buyers ' .
            '<span class="report-attendance-date">on ' . $co_op_day.'</span>: ';
        }
        $content .= '<b><span class="report-attendance-total">' .
          $purchase->Attendance($start, $end, $organisation) . '</span></b>' .
          '</div>';
      }
      else {
        $content .= '<div class="report-attendance hidden">Number of buyers ' .
          '<span class="report-attendance-date"></span>: ' .
          '<b><span class="report-attendance-total"></span></b></div>';
      }
      $content .= '<div class="report-attendance-graph"></div>' .
        '<div class="report-attendance-search">Enter dates to view ' .
          'attendance history:' .
          '<div class="form-spacing">' .
            '<label for="report-attendance-start">Start:</label>' .
            '<input id="report-attendance-start" maxlength="50" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="report-attendance-end">End:</label>' .
            '<input id="report-attendance-end" maxlength="50" type="text">' .
          '</div>' .
          '<button id="report-attendance-button">submit</button>' .
        '</div></div>' .
        '<div class="report-accounts">Account information: ' .
          '<select id="report-accounts-select">' .
            '<option value="">select option...</option>';
      if ($this->Substitute('display-membership-reminder') === 'true') {
        $content .=
          '<option value="membershipReminders">membership reminders</option>';
      }
      $content .= '<option value="lastAttendance">last attendance</option>' .
            '<option value="accountsPayable">accounts payable</option>' .
            '<option value="activeMembers">member status</option>' .
            '<option value="paidDeposit">paid deposit</option>' .
          '</select>' .
          '<div class="form-spacing">' .
            '<label for="report-download-accounts">Download available data:' .
            '</label>' .
            '<input id="report-download-accounts" type="checkbox">' .
          '</div>' .
          '<div class="report-accounts-info"></div>' .
        '</div>';
    }
    else {
      // Close the report-spacing div, which is otherwise closed just before
      // report-accounts above.
      $content .= '</div>';
    }
    return $content;
  }

  private function FormatSelect($organisation) {
    if ($organisation) {
      return '<option value="horizontal">show groups in columns</option>' .
        '<option value="vertical">show groups in rows</option>';
    }
    return '<option value="packing">packing list</option>' .
      '<option value="user">sort by user</option>' .
      '<option value="product">sort by product</option>';
  }

  private function Init($group) {
    $default_group = $this->user->group;
    $organiser = new Organiser($this->user, $this->owner);
    $stock = new Stock($this->user, $this->owner);

    if ($group !== $organiser->Parent()) {
      $this->user->group = $group;
    }
    $result = ['products' => $stock->AllProducts(true, '', false)];
    $this->user->group = $default_group;
    return $result;
  }

  private function LastAttendance($group) {
    if (!$this->GroupMember('admin', 'admin')) {
      return ['error' => 'Permission denied showing last attendance.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show accounts for this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }

    $data = [];
    $detail = new Detail($this->user, $this->owner);
    $all_users = $detail->AllUsers($organisation);
    $purchase = new Purchase($this->user, $this->owner);
    $all_most_recent = $purchase->AllMostRecent(time(), $organisation);
    foreach ($all_most_recent as $username => $timestamp) {
      $full_name = '';
      if (isset($all_users[$username]['first'])) {
        $full_name = $all_users[$username]['first'];
      }
      if (isset($all_users[$username]['last'])) {
        if ($full_name !== '') {
          $full_name .= ' ';
        }
        $full_name .= $all_users[$username]['last'];
      }
      $email = '';
      if (isset($all_users[$username]['email'])) {
        $email = $all_users[$username]['email'];
      }
      $data[] = ['user' => $username, 'fullname' => $full_name,
                 'email' => $email, 'data' => $timestamp * 1000];
    }
    $this->user->group = $default_group;
    $info = 'Most recent attendance for <b>' . count($data) . '</b> members.';

    if ($_POST['download'] === '1' && count($data) > 0) {
      $date = date('Y-m-d');
      $filename = 'last-attendance-' . $date . '.csv';
      $this->CreateCSV($filename, $data, true, 'date');
      return ['info' => $info, 'data' => $data, 'filename' => $filename];
    }
    return ['info' => $info, 'data' => $data];
  }

  private function MembershipReminders($group) {
    if (!$this->GroupMember('admin', 'admin')) {
      return ['error' => 'Permission denied showing membership reminders.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show accounts for this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }

    $data = [];
    $detail = new Detail($this->user, $this->owner);
    foreach ($detail->AllUsers($organisation) as $username => $values) {
      $full_name = '';
      if (isset($values['first'])) {
        $full_name = $values['first'];
      }
      if (isset($values['last'])) {
        if ($full_name !== '') {
          $full_name .= ' ';
        }
        $full_name .= $values['last'];
      }
      $reminder_time = (int)$values['reminder_time'] * 1000;
      if ($reminder_time === 0) $reminder_time = '';
      $data[] = ['user' => $username, 'fullname' => $full_name,
                 'email' => $values['email'], 'data' => $reminder_time];
    }
    $this->user->group = $default_group;
    $info = 'Current reminders for <b>' . count($data) . '</b> members.';

    if ($_POST['download'] === '1' && count($data) > 0) {
      $date = date('Y-m-d');
      $filename = 'membership-reminders-' . $date . '.csv';
      $this->CreateCSV($filename, $data, true, 'date');
      return ['info' => $info, 'data' => $data, 'filename' => $filename];
    }
    return ['info' => $info, 'data' => $data];
  }

  private function PaidDeposit($group) {
    if (!$this->GroupMember('admin', 'admin')) {
      return ['error' => 'Permission denied showing paid deposit.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show accounts for this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }

    $total = 0;
    $data = [];
    $banking = new Banking($this->user, $this->owner);
    $all_settings = $banking->AllSettings($organisation);
    $detail = new Detail($this->user, $this->owner);
    foreach ($detail->AllUsers($organisation) as $username => $values) {
      $full_name = '';
      if (isset($values['first'])) {
        $full_name = $values['first'];
      }
      if (isset($values['last'])) {
        if ($full_name !== '') {
          $full_name .= ' ';
        }
        $full_name .= $values['last'];
      }
      $deposit = 0;
      if (isset($all_settings[$username]['deposit'])) {
        $deposit = (int)$all_settings[$username]['deposit'];
        if ($deposit === 1) {
          $total++;
        }
      }
      $data[] = ['user' => $username, 'fullname' => $full_name,
                 'email' => $values['email'], 'data' => $deposit];
    }
    $this->user->group = $default_group;
    $info = '';
    if ($total === 1) {
      $info = '<b>1</b> member has paid a deposit';
    }
    else {
      $info = '<b>' . $total . '</b> members have paid a deposit';
    }
    $info .= ' (' . (count($data) - $total) . ' have not).';

    if ($_POST['download'] === '1' && count($data) > 0) {
      $date = date('Y-m-d');
      $filename = 'paid-deposit-' . $date . '.csv';
      $this->CreateCSV($filename, $data, false, 'deposit');
      return ['info' => $info, 'data' => $data, 'filename' => $filename];
    }
    return ['info' => $info, 'data' => $data];
  }

  private function PaymentHistory($group) {
    $start = strtotime(date('F j Y 00:00:00', (int)$_POST['start'] / 1000));
    $end = strtotime(date('F j Y 23:59:59', (int)$_POST['end'] / 1000));
    if (!$start || !$end || $start > $end) {
      return ['error' => 'Start and End dates required.'];
    }

    $mysqli = connect_db();
    $type = $mysqli->escape_string($_POST['type']);
    $mysqli->close();

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $payment = new Payment($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      return ['error' => 'Cannot show payment history this group.'];
    }
    else {
      return ['error' => 'Group not found.'];
    }
    $date = date('j M Y', $start);
    $data = [];
    $payment_total = 0;
    // Want to show up to 10 data points in the graph, so find the number of
    // days being shown and calculate the number of days in each data point.
    $days = ceil(($end - $start) / 86400);
    $segment = $days > 10 ? (int)ceil($days / 10) : 1;
    $next = $start + ($segment * 86400);
    for ($i = 0; $i < 10; $i++) {
      // Make sure $next isn't greater than the requested end date. 
      if ($next > $end) $next = $end;
      $segment_total = $payment->SearchTotal($start, $next, $organisation,
                                             $type);
      if ($type === 'outgoing') {
        // Outgoing payments are negative, display them as positive.
        $segment_total *= -1;
      }
      $data[] = [$start * 1000, $segment_total];
      $payment_total += $segment_total;
      if ($days === 1) break;
      $start = $next + 1;
      $next += $segment * 86400;
    }
    $this->user->group = $default_group;
    $result = ['date' => $date, 'total' => price_string($payment_total)];
    // Return data for a single day but don't graph it.
    if ($days === 1) return $result;

    // Add the end date to the displayed date text.
    $result['date'] .= ' - ' . date('j M Y', $end);
    $result['series'] = [['showMarker' => false]];
    // There's only one data series to display, so it can just be wrapped in
    // another array here.
    $result['data'] = ['type' => 'time', 'x_axis' => '%d %b',
                       'y_axis' => '$%.0f', 'min' => 0,
                       'data' => [$data]];
    return $result;
  }

  private function PurchaseHistory($group) {
    $start = strtotime(date('F j Y 00:00:00', (int)$_POST['start'] / 1000));
    $end = strtotime(date('F j Y 23:59:59', (int)$_POST['end'] / 1000));
    if (!$start || !$end || $start > $end) {
      return ['error' => 'Start and End dates required.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $purchase = new Purchase($this->user, $this->owner);
    $default_group = $this->user->group;
    $invite_group = false;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      $this->user->group = $group;
      $invite_group = true;
    }
    else {
      return ['error' => 'Group not found.'];
    }
    $taxable = $this->Substitute('stock-taxable') !== '';
    $surcharge = !$invite_group && $this->Substitute('surcharge') === 'true';
    $date = date('j M Y', $start);
    // Baseline the graph at zero, unless there is a negative segment total.
    $min = 0;
    $purchase_data = [];
    $taxable_data = [];
    $surcharge_data = [];
    $purchase_total = 0;
    $taxable_total = 0;
    $surcharge_total = 0;
    // Want to show up to 10 data points in the graph, so find the number of
    // days being shown and calculate the number of days in each data point.
    $days = (int)ceil(($end - $start) / 86400);
    $segment = $days > 10 ? (int)ceil($days / 10) : 1;
    $next = $start + ($segment * 86400);
    for ($i = 0; $i < 10; $i++) {
      // Make sure $next isn't greater than the requested end date. 
      if ($next > $end) $next = $end;
      $all_totals = $purchase->AllTotals($start, $next, $organisation,
                                         $invite_group);
      $segment_total = 0;
      foreach ($all_totals as $user => $total) {
        $segment_total += $total;
      }
      if ($segment_total < 0) $min = '';
      $purchase_data[] = [$start * 1000, $segment_total];
      $purchase_total += $segment_total;
      if ($taxable) {
        $all_taxable = $purchase->AllTaxable($start, $next, $organisation,
                                             $invite_group);
        $segment_total = 0;
        foreach ($all_taxable as $user => $total) {
          $segment_total += $total;
        }
        $taxable_data[] = [$start * 1000, $segment_total];
        $taxable_total += $segment_total;
      }
      if ($surcharge) {
        $all_surcharge = $purchase->AllSurcharge($start, $next);
        $segment_total = 0;
        foreach ($all_surcharge as $user => $total) {
          $segment_total += $total;
        }
        $surcharge_data[] = [$start * 1000, $segment_total];
        $surcharge_total += $segment_total;
      }
      if ($days === 1) break;
      $start = $next + 1;
      $next += $segment * 86400;
    }
    $this->user->group = $default_group;
    $result = ['date' => $date, 'total' => price_string($purchase_total)];
    $series = [['label' => 'Total', 'showMarker' => false]];
    $data = [$purchase_data];
    if ($taxable) {
      $result['taxable'] = price_string($taxable_total);
      $series[] = ['label' => $this->Substitute('report-taxable'),
                   'showMarker' => false];
      $data[] = $taxable_data;
    }
    if ($surcharge) {
      $result['surcharge'] = price_string($surcharge_total);
      $series[] = ['label' => 'Surcharge', 'showMarker' => false];
      $data[] = $surcharge_data;
    }
    // Return data for a single day but don't graph it.
    if ($days === 1) return $result;

    // Add the end date to the displayed date text.
    $result['date'] .= ' - ' . date('j M Y', $end);
    $result['series'] = $series;
    $result['data'] = ['type' => 'time', 'x_axis' => '%d %b',
                       'y_axis' => '$%.0f', 'min' => $min, 'data' => $data];
    return $result;
  }

  private function SalesHistory($group) {
    $start = strtotime(date('F j Y 00:00:00', (int)$_POST['start'] / 1000));
    $end = strtotime(date('F j Y 23:59:59', (int)$_POST['end'] / 1000));
    if (!$start || !$end || $start > $end) {
      return ['error' => 'Start and End dates required.'];
    }

    $mysqli = connect_db();
    $product = $mysqli->escape_string($_POST['product']);
    $supplier = $mysqli->escape_string($_POST['supplier']);
    $mysqli->close();

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $purchase = new Purchase($this->user, $this->owner);
    $default_group = $this->user->group;
    $organisation = false;

    if ($group === $organiser->Parent()) {
      $organisation = true;
    }
    else if (in_array($group, $organiser->Siblings())) {
      // Note that this returns true when only one group.
      $this->user->group = $group;
    }
    else if (in_array($group, $invite->Created())) {
      $this->user->group = $group;
    }
    else {
      return ['error' => 'Group not found.'];
    }

    if (!$organiser->MatchUser($supplier, $invite->Joined())) {
      $this->user->group = $default_group;
      return ['error' => 'Supplier not found.'];
    }

    $date = date('j M Y', $start);
    // Baseline the graph at zero, unless there is a negative segment total.
    $min = 0;
    $data = [];
    $sales_total = 0;
    // Want to show up to 10 data points in the graph, so find the number of
    // days being shown and calculate the number of days in each data point.
    $days = (int)ceil(($end - $start) / 86400);
    $segment = $days > 10 ? (int)ceil($days / 10) : 1;
    $next = $start + ($segment * 86400);
    for ($i = 0; $i < 10; $i++) {
      // Make sure $next isn't greater than the requested end date.
      if ($next > $end) $next = $end;
      $segment_total = $purchase->SupplyTotal($supplier, $start, $next,
                                              $product, $organisation);
      if ($segment_total < 0) $min = '';
      $data[] = [$start * 1000, $segment_total];
      $sales_total += $segment_total;
      if ($days === 1) break;
      $start = $next + 1;
      $next += $segment * 86400;
    }
    $this->user->group = $default_group;
    $result = ['date' => $date, 'total' => price_string($sales_total)];
    // Return data for a single day but don't graph it.
    if ($days === 1) return $result;

    // Add the end date to the displayed date text.
    $result['date'] .= ' - ' . date('j M Y', $end);
    $result['series'] = [['showMarker' => false]];
    // There's only one data series to display, so it can just be wrapped in
    // another array here.
    $result['data'] = ['type' => 'time', 'x_axis' => '%d %b',
                       'y_axis' => '$%.0f', 'min' => $min, 'data' => [$data]];
    return $result;
  }

  private function ShowOrder($group) {
    $mysqli = connect_db();
    $format = $mysqli->escape_string($_POST['format']);
    $mysqli->close();
    $invite = new Invite($this->user, $this->owner);
    $invoice = new Invoice($this->user, $this->owner);
    $organiser = new Organiser($this->user, $this->owner);
    $default_group = $this->user->group;
    $result = [];

    if ($group === $organiser->Parent()) {
      if (in_array($format, ['horizontal', 'vertical'])) {
        $result = $invoice->ShowOrder($group, $format);
      }
      else {
        $result = ['content' => 'Format: ' . $format .
                                ' not available for organisation order.'];
      }
    }
    else if (in_array($group, $organiser->Siblings())) {
      if (in_array($format, ['user', 'product', 'packing'])) {
        $this->user->group = $group;
        $result = $invoice->ShowOrder($group, $format);
      }
      else {
        $result = ['content' => 'Format: ' . $format .
                                ' not available for group order.'];
      }
    }
    else if (in_array($group, $invite->Created())) {
      // TODO: Needs to also offer to show orders for individual groups, which
      // requires passing group to invite->Joined().
      if (in_array($format, ['horizontal', 'vertical'])) {
        $this->user->group = $group;
        $result = $invoice->ShowOrder($group, $format);
      }
      else {
        $result = ['content' => 'Format: ' . $format .
                                ' not available for invite order.'];
      }
    }
    else {
      $result = ['error' => 'Could not show orders for this group.'];
    }
    $this->user->group = $default_group;
    if (isset($result['files'])) {
      // Add the supplier files to the content as a select.
      $result['content'] .= '<div>You can also download supplier lists: ' .
        '<select id="report-supplier-select">';
      foreach ($result['files'] as $file) {
        $result['content'] .= '<option value="' . $file . '">' . $file .
          '</option>';
      }
      $result['content'] .= '</select> ' .
        '<button id="report-supplier-download">download</button></div>';
    }
    return $result;
  }

  private function ShowQuotas($group) {
    $organiser = new Organiser($this->user, $this->owner);
    $parent = $organiser->Parent();
    $invite = new Invite($this->user, $this->owner);

    if ($group !== $parent &&
        !in_array($group, $organiser->Siblings()) &&
        !in_array($group, $invite->Created())) {
      return ['error' => 'Could not show quotas for this group.'];
    }

    $organisation = false;
    $default_group = $this->user->group;
    if ($group === $parent) {
      $organisation = true;
    }
    else {
      $this->user->group = $group;
    }
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day contains commas, assume this is multiple weekdays and use
    // the first day listed.
    if (strpos($co_op_day, ',') !== false) {
      $co_op_day = strstr($co_op_day, ',', true);
    }
    $start = strtotime('next ' . $co_op_day);
    if (!$start) $start = (int)strtotime($co_op_day);
    if ($start < time()) {
      $this->user->group = $default_group;
      return ['error' => 'Order date must be in the future.'];
    }

    $end = $start + 86400;
    $purchase = new Purchase($this->user, $this->owner);
    $quotas = $purchase->AllQuotas($start, $end, $organisation);
    $this->user->group = $default_group;
    return $quotas;
  }

}
