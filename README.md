# Dobrado

Dobrado is a multi-user content management system written in PHP & Javascript.
It also requires a MySQL database. It is released under the Affero GPL version
3.

Unlike most website building platforms, there are no page templates in Dobrado.
All editing is done through the web pages themselves, via inline editing and
page creation tools. This repository bundles many other dependencies, such as
CKEditor, HTMLPurifier and SimplePie.

Many modules have been written for Dobrado, they are all listed in the install
directory. For help getting started please visit https://dobrado.net.
